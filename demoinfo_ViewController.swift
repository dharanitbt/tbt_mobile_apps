//
//  demoinfo_ViewController.swift
//  BMHS
//
//  Created by TechBT on 22/02/19.
//  Copyright © 2019 TechBT. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import Alamofire
class demoinfo_ViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,CLLocationManagerDelegate {
      var locationManager = CLLocationManager()
    var facility_Array = [String]()
    var contactnumber_Array = [String]()
    var x:CLLocationDegrees = 0.0
    var y:CLLocationDegrees = 0.0
    var latitute = Double()
    var count = 0
    var logtitue = Double()
    var salon_name = ""
    var rating = Double()
  let combination = NSMutableAttributedString()
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        print("section\(section)")
        
        return 5
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0
        {
            return 238
        }else if indexPath.row == 1{
            return 84
        }else if indexPath.row == 2
        {
            return 126
        }else if indexPath.row == 3{
            return 126
        }else if indexPath.row == 4{
            return 197
        }
        return 100
    }

    func getDetails()
    {
        facility_Array.removeAll()
        contactnumber_Array.removeAll()
        CardName.sharedInstance.DateArray.removeAll()
        CardName.sharedInstance.TimeArray.removeAll()
        let clicked_id = UserDefaults.standard.string(forKey: "clicked_id")
        print("clicked_id\(clicked_id)")
        let parameters: [String: Any] = [
            "saloonID" : clicked_id
        ]
        let url = CardName.sharedInstance.infoDetails_url
        print("info_url\(url)")
        let type = UserDefaults.standard.string(forKey: "token")!
        let token = UserDefaults.standard.string(forKey: "Authorizent_token")!
        
        
        let header = [
            "Authorization": "\(type) \(token)",
        ]
        Alamofire.request(url, method: .post,parameters: parameters, encoding: JSONEncoding.default,headers : header)
            .responseJSON { response in
                print("response\(response)")
                let response_value = response.result.value as! NSDictionary
                self.latitute = response_value.value(forKey: "Latitude") as! Double
                self.logtitue = response_value.value(forKey: "Longitude") as! Double
                self.x = self.latitute as! CLLocationDegrees
                self.y = self.logtitue as! CLLocationDegrees
                print("x====\(self.x),\(self.y)")
                let address = response_value.value(forKey: "Address") as! String
                let salon_name = response_value.value(forKey: "Name") as! String
                print(self.latitute)
              
                //  self.mapview.animate(to: position)
                var buttonText: NSString = "Address \n \(address) " as NSString
                
                //getting the range to separate the button title strings
                var newlineRange: NSRange = buttonText.range(of: "\n")
                
                //getting both substrings
                var substring1: NSString = ""
                var substring2: NSString = ""
                
                if(newlineRange.location != NSNotFound) {
                    substring1 = buttonText.substring(to: newlineRange.location) as NSString
                    substring2 = buttonText.substring(from: newlineRange.location) as NSString
                }
                
                //assigning diffrent fonts to both substrings
                // let font:UIFont? = UIFont(name: "Arial", size: 8.0)
                let myString = substring1
                let myAttribute = [ NSAttributedStringKey.foregroundColor: UIColor.black ,  NSAttributedStringKey.font: UIFont(name: "Arial", size: 17.0)! ]
                let myAttrString = NSAttributedString(string: myString as String, attributes: myAttribute)
                
                let font1:UIFont? = UIFont(name: "Arial", size: 15.0)
                let myString1 = substring2
                let myAttribute1 = [ NSAttributedStringKey.foregroundColor: UIColor.black,  NSAttributedStringKey.font: UIFont(name: "Arial", size: 15.0)!]
                
                let myAttrString1 = NSAttributedString(string: myString1 as String, attributes: myAttribute1)
                
                //appending both attributed strings
                
              
                
                self.combination.append(myAttrString)
                self.combination.append(myAttrString1)
                //combination.append(image1String)
               // self.address_text.attributedText = combination
                self.rating = response_value.value(forKey: "Rating") as! Double
             //   self.ratingbar.rating = rating
                let timings = response_value.value(forKey: "Timing") as! NSArray
                print("timings\(timings)")
                for i in 0..<timings.count
                {
                    let timing_value = timings[i] as! NSDictionary
                    let date = timing_value.value(forKey: "DayName") as! String
                    let timings = timing_value.value(forKey: "TimeDesc") as! String
                    CardName.sharedInstance.DateArray.append(date)
                    CardName.sharedInstance.TimeArray.append(timings)
                }
             //   self.timings_tableview.reloadData()
                if response_value.value(forKey: "ContactNumber") is NSNull
                {
                    
                }else{
                    let contact_number = response_value.value(forKey: "ContactNumber") as! String
                    self.contactnumber_Array.append(contact_number)
                }
                if response_value.value(forKey: "ContactNumber2") is NSNull
                {
                    
                }else{
                    let additional_number = response_value.value(forKey: "ContactNumber2") as! String
                    self.contactnumber_Array.append(additional_number)
                }
                
                let facility = response_value.value(forKey: "Facility") as! NSArray
                for i in 0..<facility.count
                {
                    self.facility_Array.append(facility[i] as! String)
                }
                self.tableview.reloadData()
               // self.facilities_tableview.reloadData()
               // self.contact_tableview.reloadData()
        }
        
        
        
    }
  
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       // var cell : UITableViewCell
        print("here")
        if indexPath.row == 0
        {
         let cell = tableView.dequeueReusableCell(withIdentifier: "one") as! Mapview_TableViewCell
            cell.mapview.isMyLocationEnabled = true
            cell.mapview.isUserInteractionEnabled = true
            print("lat\(latitute)")
            print("lat\(logtitue)")
            var position = CLLocationCoordinate2DMake(latitute,logtitue)
            var gmsposititon = GMSCameraPosition(target: position, zoom: 15.0, bearing: 100, viewingAngle: 360)
            var marker = GMSMarker(position: position)
            marker.title = salon_name
            marker.map = cell.mapview
            cell.mapview.camera =  gmsposititon
            cell.addresstext.attributedText = combination
            //set the data here
            return cell
        }else if indexPath.row == 1
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "two") as! number_TableViewCell
            //set the data here
           // tableView.numberOfRows(inSection: 2)
            for i in 0..<contactnumber_Array.count
            {
                cell.contactnumber.text = contactnumber_Array[i]
            }
            
            return cell
        }else if indexPath.row == 2
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "three") as! rating_TableViewCell
            //set the data here
            cell.ratingbar.rating = self.rating
            return cell
        }
        else if indexPath.row == 3{
            let cell = tableView.dequeueReusableCell(withIdentifier: "four") as! info_facility_TableViewCell
            //set the data here
            for i in 0..<facility_Array.count
            {
                 cell.facility_label.text = facility_Array[i]
            }
        //
            return cell
        }
        else if indexPath.row == 4
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "five") as! timings_TableViewCell
            //set the data here
            return cell
        }
        return UITableViewCell()
    }
  
    override func viewDidLoad() {
        super.viewDidLoad()
      //    self.locationManager.delegate = self
        getDetails()
        self.tableview.delegate = self
        self.tableview.dataSource = self
//tableview.reloadData()
        // Do any additional setup after loading the view.
    }
    

    @IBOutlet weak var tableview: UITableView!
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
