//
//  Notification_Viewcontroller.swift
//  BMHS
//
//  Created by TBT Office on 8/5/19.
//  Copyright © 2019 TechBT. All rights reserved.
//

import UIKit
import Alamofire

class Notification_Viewcontroller: UIViewController, UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("Notification Count : \(notification_array.count)")
        return notification_array.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Notification_TableViewCell", for: indexPath) as! Notification_TableViewCell
        cell.message_lbl.text = notification_array[indexPath.row]
        //cell.close_button .tag = indexPath.row
        //cell.close_button.addTarget(self, action: #selector(self.removebutton(_:)), for: .touchUpInside);
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
    }
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            table_view.beginUpdates()
            notification_array.remove(at: indexPath.row)
            self.table_view.deleteRows(at: [indexPath], with: .fade )
            table_view.endUpdates()
            table_view.reloadData()
            
            // handle delete (by removing the data from your array and updating the tableview)
        }
    }
    @objc func removebutton(_ sender : UIButton)
    {
        print("Remove button called")
        notification_array.remove(at: sender.tag) //Remove your element from array
        self.table_view.deleteRows(at: [IndexPath(row: sender.tag, section: 0)], with: .automatic)
        table_view.reloadData()
    }
    @IBOutlet weak var no_notification_view: UIView!
    @IBOutlet weak var table_view: UITableView!
    @IBOutlet weak var back_img: UIImageView!
    var notification_array = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        getdetails()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(Notification_Viewcontroller.tapFunction))
        back_img.isUserInteractionEnabled = true
        
        back_img.addGestureRecognizer(tap)
        table_view.register(UINib(nibName: "Notification_TableViewCell", bundle: nil), forCellReuseIdentifier: "Notification_TableViewCell")
        //Do any additional setup after loading the view.
    }
    @objc func tapFunction(sender:UITapGestureRecognizer) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let subclass = storyboard.instantiateViewController(withIdentifier: "DashboardViewController") as! UITabBarController
        subclass.selectedIndex = 0
        self.present(subclass,animated: true,completion: nil)
        
        
    }
func getdetails()
    
    {
        self.notification_array.removeAll()
        let url = CardName.sharedInstance.notification_url
        var token = ""
        var type = ""
        
        type = UserDefaults.standard.string(forKey: "token")!
        token = UserDefaults.standard.string(forKey: "Authorizent_token")!
        
        
        let header = [
            "Authorization": "\(type) \(token)",
        ]
        print("headers1212\(header)")
        Alamofire.request(url, method: .post, encoding: JSONEncoding.default,headers : header)
            .responseJSON { response in
                print("URL \(url)")
                print("Notification response\(response)")
                print("token_ex\(token)")
                
                if response.error == nil{
                  let responce_array = response.result.value as! NSArray
//
                    if responce_array.count == 0
                    {
                        self.table_view.isHidden = true
                        self.no_notification_view.isHidden = false

                    }else
                    {

                        for i in 0..<responce_array.count
                        {
                            let responce_value = responce_array[i] as! NSDictionary
                            let message = responce_value.value(forKey: "NotificationMessage") as! String
                            self.notification_array.append(message)
                        }
                        self.table_view.reloadData()
                        self.table_view.delegate = self
                        self.table_view.dataSource = self
                        
                    }

                }else
                {
                    let alert = UIAlertController(title: "Alert", message: "Network Error, Try Again!", preferredStyle: .alert)
                    _ = alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
                    self.present(alert,animated: true,completion: nil)
                }



      }
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
