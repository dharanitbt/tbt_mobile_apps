//
//  Dashboard_ableViewController.swift
//  BMHS
//
//  Created by TechBT on 22/02/19.
//  Copyright © 2019 TechBT. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import AARatingBar
import Alamofire
import Cosmos
import MapKit

class Dashboard_ableViewController: UITableViewController,CLLocationManagerDelegate,UIGestureRecognizerDelegate {
    @IBOutlet weak var address_text: UITextView!
    var facility_Array = [String]()
    var contactnumber_Array = [String]()
    var x:CLLocationDegrees = 0.0
    var y:CLLocationDegrees = 0.0
    var latitute = Double()
    var count = 0
    var logtitue = Double()
    var salon_name = ""
    var rating = Double()
    //   var Date_Array = [String]()
    let combination = NSMutableAttributedString()
    //  var timings_Array = [String]()
    @IBOutlet weak var scrollview: UIScrollView!
    
    @IBOutlet var tableview: UITableView!
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        var count = 0
//        if section == 0
//        {
//            return 1
//        }
//        else if section == 1{
//            return 3
//        }
//        else if section == 2
//        {
//            return 1
//        }
//        else if section == 3
//        {
//            return 3
//        }
//        else if section == 4
//        {
//            return 3
//        }
//
//        if tableView == contact_tableview
//        {
//            count = self.contactnumber_Array.count
//
//        }
//        else if tableView == facilities_tableview
//        {
//
//            count = self.facility_Array.count
//        }
//        else if tableView == timings_tableview
//        {
//            count = CardName.sharedInstance.TimeArray.count
//        }
        return 1
    }
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 5
    }
    @IBOutlet weak var timings_tableview: UITableView!
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let url = URL(string: "tel://\(contactnumber_Array[indexPath.row])"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "salon"
    }
   override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // var cell : UITableViewCell
        print("here")
    print("indexpath\(indexPath.section)")
        if indexPath.section == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "section1") as! Mapview_TableViewCell
            
            self.mapview.isMyLocationEnabled = true
            self.mapview.isUserInteractionEnabled = true
            print("lat\(latitute)")
            print("lat\(logtitue)")
            var position = CLLocationCoordinate2DMake(latitute,logtitue)
            var gmsposititon = GMSCameraPosition(target: position, zoom: 15.0, bearing: 100, viewingAngle: 360)
            var marker = GMSMarker(position: position)
            marker.title = salon_name
            marker.map = self.mapview
            self.mapview.camera =  gmsposititon
            self.address_text.attributedText = combination
            //set the data here
            return cell
    }
//        }else if indexPath.section == 1
//        {
//            let cell = tableView.dequeueReusableCell(withIdentifier: "two") as! number_TableViewCell
//            //set the data here
//            // tableView.numberOfRows(inSection: 2)
//            for i in 0..<contactnumber_Array.count
//            {
//                cell.contactnumber.text = contactnumber_Array[i]
//            }
//
//            return cell
//        }else if indexPath.section == 2
//        {
//            let cell = tableView.dequeueReusableCell(withIdentifier: "three") as! rating_TableViewCell
//            //set the data here
//            cell.ratingbar.rating = self.rating
//            return cell
//        }
//        else if indexPath.section == 3{
//            let cell = tableView.dequeueReusableCell(withIdentifier: "four") as! info_facility_TableViewCell
//            //set the data here
//            for i in 0..<facility_Array.count
//            {
//                cell.facility_label.text = facility_Array[i]
//            }
//            //
//            return cell
//        }
//        else if indexPath.section == 4
//        {
//            let cell = tableView.dequeueReusableCell(withIdentifier: "five") as! timings_TableViewCell
//            //set the data here
//            return cell
//        }
        return UITableViewCell()
    }
   
  
    @IBOutlet weak var facilities_tableview: UITableView!
    
    var locationManager = CLLocationManager()
    override func viewDidLoad() {
        super.viewDidLoad()
        //mapview.isMyLocationEnabled = true
        // self.scrollview.contentSize = self.scrollview.superview!.bounds.size
        
        self.tableview.delegate = self
        self.tableview.dataSource = self
        
       // tableview.register(UINib(nibName: "Mapview_TableViewCell", bundle: nil), forCellReuseIdentifier: "one")
        
        //self.view.addSubview(mapview)
        
        self.tableview.register(Mapview_TableViewCell.self, forCellReuseIdentifier: "section1")
//        self.tableview.register(number_TableViewCell.self, forCellReuseIdentifier: "two")
//        self.tableview.register(rating_TableViewCell.self, forCellReuseIdentifier: "three")
//        self.tableview.register(info_facility_TableViewCell.self, forCellReuseIdentifier: "four")
//        self.tableview.register(t.self, forCellReuseIdentifier: "five")
     //   self.scrollview.contentInsetAdjustmentBehavior = .automatic
//        contact_tableview.register(UINib(nibName: "Contact_TableViewCell", bundle: nil), forCellReuseIdentifier: "CustomCellOne")
//        facilities_tableview.register(UINib(nibName: "Facilities_TableViewCell", bundle: nil), forCellReuseIdentifier: "CustomCellTwo")
    //    tableView.register(UINib(nibName: "Mapview_TableViewCell", bundle: nil), forCellReuseIdentifier: "section1_0")
       // tableView.register(UINib(nibName: "Mapview_TableViewCell", bundle: nil), forCellReuseIdentifier: "section1_0")
      
     //   self.locationManager.delegate = self
        getDetails()
      
        //        rating_bar.ratingDidChange = { ratingValue in
        //            // get current selected rating
        //        }
        // Do any additional setup after loading the view.
    }
    
    
    @objc func handleTap(_ sender: UITapGestureRecognizer)
    {
        
        
        //let coordinate = CLLocationCoordinate2DMake(x,y)
        
    }
    
    
    @IBAction func btn_mapview(_ sender: Any) {
        print("Here came")
        if let url = URL(string: ("https://maps.apple.com/&daddr=\(x),\(y)")), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let salonname = UserDefaults.standard.string(forKey: "Salon_Name")
   //     self.navigationBar.topItem?.title = salonname
    }
    
    @IBOutlet weak var contact_tableview: UITableView!
  
    @IBOutlet weak var ratingbar: CosmosView!
    func getDetails()
    {
        facility_Array.removeAll()
        contactnumber_Array.removeAll()
        CardName.sharedInstance.DateArray.removeAll()
        CardName.sharedInstance.TimeArray.removeAll()
        let clicked_id = UserDefaults.standard.string(forKey: "clicked_id")
        print("clicked_id\(clicked_id)")
        let parameters: [String: Any] = [
            "saloonID" : clicked_id
        ]
        let url = CardName.sharedInstance.infoDetails_url
        print("info_url\(url)")
        let type = UserDefaults.standard.string(forKey: "token")!
        let token = UserDefaults.standard.string(forKey: "Authorizent_token")!
        
        
        let header = [
            "Authorization": "\(type) \(token)",
        ]
        Alamofire.request(url, method: .post,parameters: parameters, encoding: JSONEncoding.default,headers : header)
            .responseJSON { response in
                print("response\(response)")
                let response_value = response.result.value as! NSDictionary
                self.latitute = response_value.value(forKey: "Latitude") as! Double
                self.logtitue = response_value.value(forKey: "Longitude") as! Double
                self.x = self.latitute as! CLLocationDegrees
                self.y = self.logtitue as! CLLocationDegrees
                print("x====\(self.x),\(self.y)")
                let address = response_value.value(forKey: "Address") as! String
                let salon_name = response_value.value(forKey: "Name") as! String
                print(self.latitute)
                
                //  self.mapview.animate(to: position)
                var buttonText: NSString = "Address \n \(address) " as NSString
                
                //getting the range to separate the button title strings
                var newlineRange: NSRange = buttonText.range(of: "\n")
                
                //getting both substrings
                var substring1: NSString = ""
                var substring2: NSString = ""
                
                if(newlineRange.location != NSNotFound) {
                    substring1 = buttonText.substring(to: newlineRange.location) as NSString
                    substring2 = buttonText.substring(from: newlineRange.location) as NSString
                }
                
                //assigning diffrent fonts to both substrings
                // let font:UIFont? = UIFont(name: "Arial", size: 8.0)
                let myString = substring1
                let myAttribute = [ NSAttributedStringKey.foregroundColor: UIColor.black ,  NSAttributedStringKey.font: UIFont(name: "Arial", size: 17.0)! ]
                let myAttrString = NSAttributedString(string: myString as String, attributes: myAttribute)
                
                let font1:UIFont? = UIFont(name: "Arial", size: 15.0)
                let myString1 = substring2
                let myAttribute1 = [ NSAttributedStringKey.foregroundColor: UIColor.black,  NSAttributedStringKey.font: UIFont(name: "Arial", size: 15.0)!]
                
                let myAttrString1 = NSAttributedString(string: myString1 as String, attributes: myAttribute1)
                
                //appending both attributed strings
                
                
                
                self.combination.append(myAttrString)
                self.combination.append(myAttrString1)
                //combination.append(image1String)
                // self.address_text.attributedText = combination
                self.rating = response_value.value(forKey: "Rating") as! Double
                //   self.ratingbar.rating = rating
                let timings = response_value.value(forKey: "Timing") as! NSArray
                print("timings\(timings)")
                for i in 0..<timings.count
                {
                    let timing_value = timings[i] as! NSDictionary
                    let date = timing_value.value(forKey: "DayName") as! String
                    let timings = timing_value.value(forKey: "TimeDesc") as! String
                    CardName.sharedInstance.DateArray.append(date)
                    CardName.sharedInstance.TimeArray.append(timings)
                }
                //   self.timings_tableview.reloadData()
                if response_value.value(forKey: "ContactNumber") is NSNull
                {
                    
                }else{
                    let contact_number = response_value.value(forKey: "ContactNumber") as! String
                    self.contactnumber_Array.append(contact_number)
                }
                if response_value.value(forKey: "ContactNumber2") is NSNull
                {
                    
                }else{
                    let additional_number = response_value.value(forKey: "ContactNumber2") as! String
                    self.contactnumber_Array.append(additional_number)
                }
                
                let facility = response_value.value(forKey: "Facility") as! NSArray
                for i in 0..<facility.count
                {
                    self.facility_Array.append(facility[i] as! String)
                }
                self.tableview.reloadData()
                // self.facilities_tableview.reloadData()
                // self.contact_tableview.reloadData()
        }
        
        
        
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let location = locations.last
        print("here")
        let camera = GMSCameraPosition.camera(withLatitude: (location?.coordinate.latitude)!, longitude: (location?.coordinate.longitude)!, zoom: 25.0)
        
        self.mapview?.animate(to: camera)
        
        //Finally stop updating location otherwise it will come again and again in this delegate
        self.locationManager.stopUpdatingLocation()
        
    }
    @IBOutlet weak var navigationBar: UINavigationBar!
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBOutlet weak var mapview: GMSMapView!
    
    @IBAction func btn_back(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let subclass = storyboard.instantiateViewController(withIdentifier: "DashboardViewController") as! UITabBarController
        subclass.selectedIndex = 1
        self.present(subclass,animated: true,completion: nil)
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

