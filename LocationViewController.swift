//
//  LocationViewController.swift
//  
//
//  Created by TechBT on 27/08/18.
//

import UIKit
import Alamofire

class LocationViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return SaloonCount_Array.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableview.dequeueReusableCell(withIdentifier: "Cell") as! LocationTableViewCell
        cell.Location_Name.text = ("\(cityArray[indexPath.row] as! String) (\(SaloonCount_Array[indexPath.row] as! Int)) ")
        return cell
    }
    
    @IBAction func btn_back(_ sender: Any)
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let subclass = storyboard.instantiateViewController(withIdentifier: "DashboardViewController") as! UITabBarController
        self.present(subclass,animated: true,completion: nil)
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         UserDefaults.standard.set(cityArray[indexPath.row], forKey: "clicked_city")
        print("city123\(cityArray[indexPath.row])")
        UserDefaults.standard.set(cityArray[indexPath.row], forKey: "city_name")
         UserDefaults.standard.set(cityId_Array[indexPath.row], forKey: "city_id")
        UserDefaults.standard.set("0", forKey: "isFirst")
      savepreferencelocation(cityname: cityArray[indexPath.row])
        saveusercity(cityid: cityId_Array[indexPath.row] as! Int)
        print("City123\(UserDefaults.standard.string(forKey: "city_id"))")
        UserDefaults.standard.set(true, forKey: "city_selected")
        
    }
    var cityArray: [String] = [""]
    var cityId_Array = NSMutableArray()
    var Categorization_Array = NSMutableArray()
    var SaloonCount_Array = NSMutableArray()
    
    
    
    @IBOutlet weak var navigationitem: UINavigationBar!
    func savepreferencelocation(cityname : String)
    {
        let url = CardName.sharedInstance.savepreferenceLocation
        var type = UserDefaults.standard.string(forKey: "token")!
        var token = UserDefaults.standard.string(forKey: "Authorizent_token")!
        let header = [
            "Authorization": "\(type) \(token)",
        ]
        print("headers\(header)")
        let parameters: [String: Any] = [
           "AddressCity":cityname,"AddressPlace": cityname
        ]
        Alamofire.request(url, method: .post,parameters: parameters, encoding: JSONEncoding.default,headers : header)
            .responseJSON { response in
                print("sace preference responce\(response)")
                if response.error == nil
                {
                 let result_value = response.result.value as! NSDictionary
                    let status = result_value.value(forKey: "Status") as! Bool
                    if status {
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let subclass = storyboard.instantiateViewController(withIdentifier: "DashboardViewController") as! UITabBarController
                        self.present(subclass,animated: true,completion: nil)
                    }else{
                        let alert = UIAlertController(title: "Alert", message: " Network Error! ,Try Again?", preferredStyle: .alert)
                        let subclass = alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
                        self.present(alert,animated: true,completion: nil)
                    }
                }else{
                    let alert = UIAlertController(title: "Alert", message: " Network Error! ,Try Again?", preferredStyle: .alert)
                    let subclass = alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
                    self.present(alert,animated: true,completion: nil)
                }
        }
    }
    func getLocationCity()
   {
    cityArray.removeAll()
    cityId_Array.removeAllObjects()
    Categorization_Array.removeAllObjects()
    SaloonCount_Array.removeAllObjects()
      let url = CardName.sharedInstance.LocationListing_Url
    let type = UserDefaults.standard.string(forKey: "token")!
    let token = UserDefaults.standard.string(forKey: "Authorizent_token")!
    
    
    let header = [
        "Authorization": "\(type) \(token)",
    ]
    Alamofire.request(url, method: .get,headers : header)
        .responseJSON { response in
          //  print(response.result.value)
            print("get location city responce\(response)")
            let responseArray = response.result.value as! NSArray
            print(responseArray.count)
            for i in 0..<responseArray.count
            {
             let responseValue = responseArray[i] as! NSDictionary
                print(responseValue.value(forKey: "City")!)
                let cityname = responseValue.value(forKey: "City") as! String
                print("place2\(cityname)")
                self.cityArray.append(cityname)
                let cityid = responseValue.value(forKey: "CityID") as! Int
                self.cityId_Array.add(cityid)
               
                let category_str = responseValue.value(forKey: "Categorization") as! String
                self.Categorization_Array.add(category_str)
                let salon_count = responseValue.value(forKey: "SaloonCount") as! Int
                self.SaloonCount_Array.add(salon_count)
            }
          self.tableview.reloadData()
    }
    }
    func saveusercity(cityid : Int)
   {
    let url = CardName.sharedInstance.saveusercity + "?masterCityID=\(cityid)"
    let type = UserDefaults.standard.string(forKey: "token")!
    let token = UserDefaults.standard.string(forKey: "Authorizent_token")!
    print("url\(url)")
    
    let header = [
        "Authorization": "\(type) \(token)",
    ]
    Alamofire.request(url, method: .get,headers : header)
        .responseJSON { response in
            print("saveusercity response\(response)")
            if response.result.value != nil{
                let status = response.result.value as! Bool
                if status {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let subclass = storyboard.instantiateViewController(withIdentifier: "DashboardViewController") as! UITabBarController
                self.present(subclass,animated: true,completion: nil)
                }else
                {
                let alert = UIAlertController(title: "Alert", message: " Network Error! ,Try Again?", preferredStyle: .alert)
                let subclass = alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
                self.present(alert,animated: true,completion: nil)
                        }
                }else
                {
                
                let alert = UIAlertController(title: "Alert", message: " Network Error! ,Try Again?", preferredStyle: .alert)
                let subclass = alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
                self.present(alert,animated: true,completion: nil)
                }
    }
    }
  
    @IBOutlet weak var btn_back: UIBarButtonItem!
    @IBOutlet weak var tableview: UITableView!
    
   
    override func viewDidLoad() {
        super.viewDidLoad()

   tableview.register(UINib(nibName: "LocationTableViewCell", bundle: nil), forCellReuseIdentifier: "Cell")
        if UserDefaults.standard.bool(forKey: "city_selected")
        {
            self.navigationItem.leftBarButtonItem?.isEnabled = true
            btn_back.isEnabled = true
            
        }else{
             btn_back.isEnabled = false
             self.navigationItem.leftBarButtonItem?.isEnabled = false
             btn_back.tintColor = UIColor.clear
            //self.navigationItem.leftBarButtonItem = nil
            
        }
     getLocationCity()
        // Do any additional setup after loading the view.
    }
    
    
   
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
