//
//  Notification_TableViewCell.swift
//  BMHS
//
//  Created by TBT Office on 8/7/19.
//  Copyright © 2019 TechBT. All rights reserved.
//

import UIKit

class Notification_TableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBOutlet weak var close_button: UIButton!
    @IBOutlet weak var items_lbl: UILabel!
    @IBOutlet weak var message_lbl: UILabel!
    override func setSelected(_ selected: Bool, animated: Bool) {
       
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
