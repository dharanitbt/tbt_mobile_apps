//
//  infoService_ViewController.swift
//  BMHS
//
//  Created by TechBT on 05/09/18.
//  Copyright © 2018 TechBT. All rights reserved.
//

import UIKit
import Alamofire
class infoService_ViewController:UIViewController,UITableViewDataSource,UITableViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource {
    @IBOutlet weak var confirm_view: UIView!
    @IBOutlet weak var back_button: UIBarButtonItem!
    var DateArray = [String]()
    var weekdayArray = [Int]()
    var service_time_Array = [String]()
    var service_salon_amount = 0
    var service_id_Array = [Int]()
    var service_genderArray = [Int]()
    var maleservice_nameArray = [String]()
    var maleservice_idArray = [Int]()
    var maleservice_amountArray = [Double]()
    var maleservice_timeArray = [Int]()
    var clicked_service = [String]()
    var clicked_id = [Int]()
    var clicked_time = [String]()
    var clicked_amount = [String]()
    var clicked_stepper_array = [Int]()
    var clicked_row = -1
    var startservice_id = 0
    var endservice_id = 0
    var gender_type = ""
    var clicked_salon_endtime = 0.0
    var service_clicked_time = ""
    var indexpath = 0
    var time_indexpath = 0
    var clicked_salon_date = ""
    var type = ""
    var isscrolling = false
    var Bookingdate = ""
    var bookingtime_from = Int()
    var total_time = 0
    var clicked_indexpath = -1
    var timeclicked = Bool()
    var service_stepper_array = [Int]()
    var equipments = [[String:Any]]()
    var clicked_index = 0
   @IBOutlet weak var tabbar: UITabBarItem!
    @IBOutlet weak var activityindicator: UIActivityIndicatorView!
    @IBOutlet weak var navItem: UINavigationBar!
    @IBOutlet weak var gendertaggle_out: UISegmentedControl!
    @IBOutlet weak var gender_taggle: UISegmentedControl!
    var stepper_value = -1
    var time_Array = ["6:00 AM","6:30 AM","7:00 AM","7:30 AM","8:00 AM","8:30 AM","9:00 AM","9:30 AM","10:00 AM","10:30 AM","11:00 AM","11:30 AM","12:00 PM","12:30 PM","1:00 PM","1:30 PM","2:00 PM","2:30 PM","3:00 PM","3:30 PM","4:00 PM","4:30 PM","5:00 PM","5:30 PM","6:00 PM","6:30 PM","7:00 PM","7:30 PM","8:00 PM","8:30 PM","9:00 PM","9:30 PM","10:00 PM","10:30 PM","11:00 PM","11:30 PM"]
    var daywiseid = [13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48]
    var days_Array = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"]
    @IBOutlet weak var timeslotview: UIView!
    var clicked_date = ""
    @IBOutlet weak var btn_payoutlet: UIButton!
    var today_day = ""
    var today_date = ""
    var indexpath_array = [Int]()
    var service_count = [Int]()
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collectionview
        {
        return self.DateArray.count
        }
        else if collectionView == timecollectionview
        {
            return self.service_time_Array.count
        }
        return 1
    }
    @IBOutlet weak var timecollectionview: UICollectionView!
    @IBOutlet weak var line_view: UIView!
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == collectionview
        {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DateCollectionViewCell", for: indexPath) as! DateCollectionViewCell
            if indexPath.row == self.indexpath
            {
                cell.datemonth_lbl.textColor = UIColor(red: 198/255.0, green: 40/255.0, blue: 40/255.0, alpha: 1.0)
                cell.Date_lbl.textColor = UIColor(red: 198/255.0, green: 40/255.0, blue: 40/255.0, alpha: 1.0)
            }else{
                cell.datemonth_lbl.textColor = UIColor.black
                cell.Date_lbl.textColor = UIColor.black
            }
        if indexPath.row == 0
        {
            cell.Date_lbl.text = "Today"
        }else if indexPath.row == 1
        {
            cell.Date_lbl.text = "Tomorrow"
        }
        else{
        let weekday = weekdayArray[indexPath.row] as! Int
            if weekday == 1
            {
              cell.Date_lbl.text = "Sunday"
                
            }else if weekday == 2
            {
                cell.Date_lbl.text = "Monday"
            }
            else if weekday == 3
            {
                cell.Date_lbl.text = "Tuesday"
                
            }else if weekday == 4
            {
                cell.Date_lbl.text = "Wednesday"
                
            }
            else if weekday == 5
            {
                cell.Date_lbl.text = "Thursday"
               
            }else if weekday == 6
            {
                cell.Date_lbl.text = "Friday"
              
            }
            else if weekday == 7
            {
                cell.Date_lbl.text = "Saturday"
               
            }
            
        }
        cell.datemonth_lbl.text = DateArray[indexPath.row] as! String
        return cell
        }
        else if collectionView == timecollectionview
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TimeCollectionViewCell", for: indexPath) as! TimeCollectionViewCell
            print("path\(time_indexpath)")
         
                if indexPath.row == self.time_indexpath
                {
                    cell.timecell.layer.backgroundColor = UIColor(red: 198/255.0, green: 40/255.0, blue: 40/255.0, alpha: 1.0).cgColor
                    cell.timecell.textColor = UIColor.white
                }else{
                    cell.timecell.layer.backgroundColor = UIColor(red: 239/255.0, green: 239/255.0, blue: 244/255.0, alpha: 1.0).cgColor
                    cell.timecell.textColor = UIColor.black
                }
            
           
              cell.timecell.text = self.service_time_Array[indexPath.row] as! String
            return cell
        }
        return UICollectionViewCell()
    }
    
    @IBAction func btn_genderselection(_ sender: Any) {
        switch gender_taggle.selectedSegmentIndex
        {
        case 0:
            gender_type = "male"
            getDetails()
            //self.showToast(message: "Showing Male services")
            CardName.sharedInstance.isgenderselected = true
            CardName.sharedInstance.ismaleselected = true
            clicked_row = 0
            
       case 1:
        print("clicked Female")
        gender_type = "female"
        clicked_row = 0
        CardName.sharedInstance.isgenderselected = true
        CardName.sharedInstance.isfemaleselected = true
        getDetails()
        //self.showToast(message: "Showing Female services")
        default:
        break
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        
        self.activityindicator.isHidden = false
        self.activityindicator.startAnimating()
        print("Gender checking \(CardName.sharedInstance.gender_type)")
        if CardName.sharedInstance.gender_type == "Unisex"
        {
            self.gendertaggle_out.isHidden = false
            
            gender_type = "female"
            
            self.getDetails()
//            ToastPresenter.shared.show(in: self.view, message: "Showing Female services", backgroundColor: .gray, textColor: .black, timeOut: 1, roundness: .low)

           // self.showToast(message: "Showing Female services")

            
            
        }
        else if CardName.sharedInstance.gender_type == "Male"
        {
            self.gendertaggle_out.isHidden = true
            
            gender_type = "male"
            self.getDetails()
        }
        else if CardName.sharedInstance.gender_type == "Female"
        {
            self.gendertaggle_out.isHidden = true
            
            gender_type = "female"
            self.getDetails()
        }
        
        
        
      
        
    }
    @IBAction func btn_back(_ sender: Any) {
        CardName.sharedInstance.isaddservice_clicked = false
         service_stepper_array.removeAll()
        clicked_stepper_array.removeAll()
        CardName.sharedInstance.selected_indexpath.removeAll()
        
        if CardName.sharedInstance.camefrom == "Dashboard"
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let subclass = storyboard.instantiateViewController(withIdentifier: "DashboardViewController") as! UITabBarController
            subclass.selectedIndex = 0
            self.present(subclass,animated: true,completion: nil)
        }
        else if CardName.sharedInstance.camefrom == "Salonpage"
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let subclass = storyboard.instantiateViewController(withIdentifier: "DashboardViewController") as! UITabBarController
            subclass.selectedIndex = 1
            self.present(subclass,animated: true,completion: nil)
        }
        else
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let subclass = storyboard.instantiateViewController(withIdentifier: "DashboardViewController") as! UITabBarController
            subclass.selectedIndex = 0
            self.present(subclass,animated: true,completion: nil)
        }
        
    }
    
//    func collectionView(_ collectionView: UICollectionView, didHighlightItemAt indexPath: IndexPath) {
////        if collectionView == collectionview
////        {
////        let cell = collectionview.cellForItem(at: indexPath)
////        print("clicked_higlight")
////        print("clicked_position\(indexPath.row)")
////            if cell?.layer.backgroundColor == UIColor(red: 198/255.0, green: 40/255.0, blue: 40/255.0, alpha: 1.0).cgColor
////            {
////                cell?.layer.backgroundColor = UIColor.white.cgColor
////            }else
////            {
////                cell?.layer.backgroundColor = UIColor(red: 198/255.0, green: 40/255.0, blue: 40/255.0, alpha: 1.0).cgColor
////            }
////
////        }else if collectionView == timecollectionview
////        {
////         let cell = timecollectionview.cellForItem(at: indexPath)
////            cell?.layer.backgroundColor = UIColor(red: 198/255.0, green: 40/255.0, blue: 40/255.0, alpha: 1.0).cgColor
////        }
//        if collectionView == collectionview
//        {
//            let cell = collectionview.cellForItem(at: indexPath)
//            if cell?.layer.backgroundColor == UIColor(red: 198/255.0, green: 40/255.0, blue: 40/255.0, alpha: 1.0).cgColor
//            {
//                cell?.layer.backgroundColor = UIColor.white.cgColor
//
//
//            }else{
//                cell?.layer.backgroundColor = UIColor(red: 198/255.0, green: 40/255.0, blue: 40/255.0, alpha: 1.0).cgColor
//
//            }
//        }
//        else if collectionview == timecollectionview
//        {
//            let cell = timecollectionview.cellForItem(at: indexPath)
//           if cell?.layer.backgroundColor == UIColor(red: 198/255.0, green: 40/255.0, blue: 40/255.0, alpha: 1.0).cgColor
//           {
//            cell?.layer.backgroundColor = UIColor.white.cgColor
//            }else
//           {
//            cell?.layer.backgroundColor = UIColor(red: 198/255.0, green: 40/255.0, blue: 40/255.0, alpha: 1.0).cgColor
//            }
//        }
//
//    }
//    func tableView(_ tableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
//        <#code#>
//    }
    // change background color back when user releases touch

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        if CardName.sharedInstance.isaddservice_clicked
//        {
//
//            for i in 0..<service_nameArray.count
//            {
//                for j in 0..<CardName.sharedInstance.selected_indexpath.count
//                {
//
//                    if i == CardName.sharedInstance.selected_indexpath[j]
//                    {
//                        print("indecpath_count123\(CardName.sharedInstance.selected_service_count[j])")
//                        clicked_stepper_array.append(1)
//                        service_stepper_array[i] = CardName.sharedInstance.selected_service_count[j]
//                        print("stepper_value\(service_stepper_array[i])")
//
//                        // cell.stepper.value = 1
//                    }else{
//
//                    }
//                }
//            }
//            print("service_Array\(service_stepper_array[indexPath.row])")
//        }else{
//            for i in 0..<service_nameArray.count{
//                service_stepper_array.append(0)
//            }
//        }
     
    }
    func showToast(message : String) {
        
        let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 70, y: self.view.frame.size.height-150, width: 220, height: 30))
        let mycolor = #colorLiteral(red: 0.2980392157, green: 0.6862745098, blue: 0.3137254902, alpha: 1)
        toastLabel.backgroundColor = mycolor
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.font = UIFont(name: "Montserrat-Light", size: 8.0)
        toastLabel.text = message
        //toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 15;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 2.0, delay: 2.0, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == collectionview
        {
             let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DateCollectionViewCell", for: indexPath) as! DateCollectionViewCell
            print("clicked_didselect")
            cell.Date_lbl.textColor = UIColor(red: 198/255.0, green: 40/255.0, blue: 40/255.0, alpha: 1.0)
            print("time slot checking")
            clicked_index = indexPath.row
            clicked_date = DateArray[indexPath.row] as! String
            clicked_salon_date = DateArray[indexPath.row] as! String
            print("clicked\(clicked_date)")
            print("clicked\(weekdayArray[indexPath.row])")
            let inputFormatter = DateFormatter()
            inputFormatter.dateFormat = "MMM dd"
            UserDefaults.standard.set(clicked_date, forKey: "date")
            UserDefaults.standard.set(weekdayArray[indexPath.row], forKey: "day")
            CardName.sharedInstance.today_day_int = weekdayArray[indexPath.row]
            let showDate = inputFormatter.date(from: clicked_date)
            inputFormatter.dateFormat = "dd-MM"
            let resultString = inputFormatter.string(from: showDate!)
            print(resultString)
            self.Bookingdate = resultString + "-2019"
            print("Bookoing\(self.Bookingdate)")
           self.indexpath = indexPath.row
//            if clicked_id.count == 0
//            {
//                let alert = UIAlertController(title: "Alert", message: "Please select atleast one service", preferredStyle: .alert)
//                let okaction = alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
//                self.present(alert,animated: true,completion: nil)
//            }else{
                getTimeslot(clicked_day: clicked_date, clicked_servicetime: total_time)
                collectionview.reloadData()
               //timecollectionview.reloadData()
//            }
            
        }else if collectionView == timecollectionview
        {
             let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TimeCollectionViewCell", for: indexPath) as! TimeCollectionViewCell
            print("clicked_salon_time\(self.clicked_salon_endtime)")
            UserDefaults.standard.set(service_time_Array[indexPath.row], forKey: "time")
            let time = self.clicked_salon_endtime / 30.0
            print("time\(time.rounded(.up))")
            let endtime = Double(self.timeslot_Array[indexPath.row] as! Int) + time.rounded(.up)
            print("endtime\(endtime)")
            self.endservice_id = Int(endtime)
            self.timeclicked = true
            self.startservice_id = self.timeslot_Array[indexPath.row] as! Int
            self.bookingtime_from = self.startservice_id
            self.time_indexpath = indexPath.row
            self.timecollectionview.reloadData()
        }
    }
    
    var service_nameArray = [String]()
    var service_amountArray = [Double]()
    var service_Array = [Int]()
    var timeslot_Array = [Int]()
   // var total_amount = 0.0
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.service_nameArray.count
        
    }
    func getTimeslot(clicked_day:String,clicked_servicetime:Int)
    {
        
        timeslot_Array.removeAll()
        self.service_time_Array.removeAll()
        let clicked_id = UserDefaults.standard.string(forKey: "clicked_id")
        print("clicked_id\(clicked_id)")
        let inputFormatter = DateFormatter()
        inputFormatter.dateFormat = "MMM dd"
        let showDate = inputFormatter.date(from: clicked_day)
        inputFormatter.dateFormat = "dd-MM"
        let resultString = inputFormatter.string(from: showDate!)
        print(resultString)
        let date = Date()
        let calendar = Calendar.current
        
        let year = calendar.component(.year, from: date)
        print("clicked_date\(clicked_day)")
       
        let url = "\(CardName.sharedInstance.service_timeslots)?date=\(resultString)-\(year)&totalServiceTime=\(clicked_servicetime)&saloonDetailsID=\(clicked_id!)"
        print("info_url\(url)")
        let type = UserDefaults.standard.string(forKey: "token")!
        let token = UserDefaults.standard.string(forKey: "Authorizent_token")!
        
        
        let header = [
            "Authorization": "\(type) \(token)",
        ]
        Alamofire.request(url, method: .get,encoding: JSONEncoding.default,headers : header)
            .responseJSON { response in
                print("Time slot Response : \(response)")
                if response.error == nil
                {
                    print("service_responce\(response.result.value)")
                    let response_Array = response.result.value as! NSArray
                    
                   
                    for i in 0..<response_Array.count
                    {
                        let responce_value = response_Array[i] as! NSDictionary
                        let timeslot = responce_value.value(forKey: "TimeSlotID") as! Int
                        self.timeslot_Array.append(timeslot)
                        for i in 0..<self.daywiseid.count
                        {
                            if timeslot == self.daywiseid[i]
                            {
                                self.service_time_Array.append(self.time_Array[i])
                            }
                        }
                    }
                    print("service_time array\(self.service_time_Array)")
                   // self.service_time_Array.removeAll()
                    if self.service_time_Array.count != 0 && self.service_time_Array.count != nil
                    {
                    self.timeslotview.isHidden = false
                    self.timecollectionview.reloadData()
                    }
                    else
                    {
                        let alert = UIAlertController(title: "Alert", message: "Timeslot not available, Try another date!", preferredStyle: .alert)
                        let okaction = alert.addAction(UIAlertAction(title: "Ok", style: .default)
                        { (action) in
                            
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let subclass = storyboard.instantiateViewController(withIdentifier: "SalonDetails_ViewController") as! UITabBarController
                            subclass.selectedIndex = 1
                            self.present(subclass,animated: false,completion: nil)
                        
                        })
                        self.present(alert,animated: true,completion: nil)
                        
                    }
                 }else{
                    let alert = UIAlertController(title: "Alert", message: "Network Error, Try Again!", preferredStyle: .alert)
                    let okaction = alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
                    self.present(alert,animated: true,completion: nil)
                }
                }
                
        
       
    }
    @IBOutlet weak var collectionview: UICollectionView!
    
    @IBAction func btn_payment(_ sender: Any) {
        CardName.sharedInstance.isaddservice_clicked = false
        if clicked_id.count == 0
        {
            let alert = UIAlertController(title: "Alert", message: "Select a service", preferredStyle: .alert)
            let okaction = alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alert,animated: true,completion: nil)
        }else{
          print("value\(UserDefaults.standard.string(forKey: "time"))")
            if clicked_date == ""
            {
                  UserDefaults.standard.set(self.DateArray[0], forKey: "date")
                
            }
           if timeclicked
           {
            CardName.sharedInstance.selected_servicename = clicked_service
            CardName.sharedInstance.selected_serviceid = clicked_id
            let id = UserDefaults.standard.string(forKey: "clicked_id")
            print("Clicked_id\(id)")
            CardName.sharedInstance.selected_serviceamount = clicked_amount
            CardName.sharedInstance.service_selected_time = clicked_time
            print("clicked\(clicked_service)")
            getpricedetails()
            }
            else{
            let alert = UIAlertController(title: "Alert", message: "Select time slot", preferredStyle: .alert)
            let okaction = alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (alert) in
                print("clicked_salon_time\(self.clicked_salon_endtime)")
                
                UserDefaults.standard.set(self.service_time_Array[0], forKey: "time")
                let time = self.clicked_salon_endtime / 30.0
                print("time\(time.rounded(.up))")
                if self.timeslot_Array.count != nil && self.timeslot_Array.count != 0
                {
                let endtime = Double(self.timeslot_Array[0] as! Int) + time.rounded(.up)
                
                print("endtime\(endtime)")
                self.endservice_id = Int(endtime)
                self.timeclicked = true
                self.startservice_id = self.timeslot_Array[0] as! Int
                self.bookingtime_from = self.startservice_id
                self.time_indexpath = 0
                self.timecollectionview.reloadData()
                CardName.sharedInstance.selected_servicename = self.clicked_service
                CardName.sharedInstance.selected_serviceid = self.clicked_id
                let id = UserDefaults.standard.string(forKey: "clicked_id")
                print("Clicked_id\(id)")
                CardName.sharedInstance.selected_serviceamount = self.clicked_amount
                print("clicked\(self.clicked_service)")
                }
                //self.getpricedetails()
            }))
            self.present(alert,animated: true,completion: nil)
            }
              
            
           
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableview.dequeueReusableCell(withIdentifier: "Cell") as! Service_TableViewCell
        //let cell1 = tableview.scrollToNearestSelectedRow(at: indexpath, animated: true)
        print("Service name checking: \(service_nameArray)")
        if CardName.sharedInstance.isaddservice_clicked
        {
            if CardName.sharedInstance.isgenderselected
            {
                cell.service_name.text = self.service_nameArray[indexPath.row]
                cell.service_amt.text = "₹\(self.service_amountArray[indexPath.row])"
                cell.stepper.value = Double(service_stepper_array[indexPath.row])
            }else{
                for i in 0..<service_nameArray.count
                {
                    for j in 0..<CardName.sharedInstance.selected_indexpath.count
                    {
                        
                        if i == CardName.sharedInstance.selected_indexpath[j]
                        {
                            print("indecpath_count123\(CardName.sharedInstance.selected_service_count[j])")
                            clicked_stepper_array.append(1)
                            service_stepper_array[i] = CardName.sharedInstance.selected_service_count[j]
                            print("stepper_value\(service_stepper_array[i])")
                            
                            // cell.stepper.value = 1
                        }else{
                            
                        }
                    }
                }
                print("indecpath_count\(CardName.sharedInstance.selected_indexpath)")
                print("indecpath_count(selected)\(CardName.sharedInstance.selected_service_count)")
                cell.service_name.text = self.service_nameArray[indexPath.row]
                cell.service_amt.text = "₹\(self.service_amountArray[indexPath.row] )"
               
                cell.stepper.value = Double(service_stepper_array[indexPath.row])
                print("array_service\(service_stepper_array[indexPath.row])")
//                for _ in 0..<service_nameArray.count
//                {
//                    for j in 0..<CardName.sharedInstance.selected_indexpath.count
//                    {
//
//                        if indexPath.row == CardName.sharedInstance.selected_indexpath[j]
//                        {
//                             print("indecpath_count\(CardName.sharedInstance.selected_indexpath[j])")
//
//                            cell.service_name.text = self.service_nameArray[indexPath.row]
//                            cell.service_amt.text = "₹\(self.service_amountArray[indexPath.row] )"
//
//                            cell.stepper.value = Double(CardName.sharedInstance.selected_service_count[j])
//                            cell.stepper.minimumValue = 0
//                           // cell.stepper.value = 1
//
//
//
//                        }else{
//                            cell.service_name.text = self.service_nameArray[indexPath.row]
//                            cell.service_amt.text = "₹\(self.service_amountArray[indexPath.row] )"
//                            cell.stepper.minimumValue = 0
//                           cell.stepper.value = Double(service_stepper_array[indexPath.row])
//
//                        }
//                    }
//                }
            }
            }else{
            if service_nameArray.count != nil && service_nameArray.count != 0
            {
            
            if CardName.sharedInstance.isgenderselected
            {
                cell.service_name.text = self.service_nameArray[indexPath.row]
                cell.service_amt.text = "₹\(self.service_amountArray[indexPath.row] )"
                cell.stepper.value = Double(service_stepper_array[indexPath.row])
            }else{
                if service_stepper_array.count != nil && service_stepper_array.count != 0
                {
                
                cell.service_name.text = self.service_nameArray[indexPath.row]
                cell.service_amt.text = "₹\(self.service_amountArray[indexPath.row] )"
                cell.stepper.value = Double(service_stepper_array[indexPath.row])
                }
            }
            // cell.stepper.buttonsBackgroundColor = UIColor.lightGray
            }  // cell.stepper.labelBackgroundColor = UIColor.lightGray
        }
        
        
        cell.stepper.tag = indexPath.row
        cell.stepper.addTarget(self, action: #selector(stepperValueChanged(stepper:)), for: .valueChanged)
        cell.stepper.labelCornerRadius = 5
        CardName.sharedInstance.isgenderselected = false
        CardName.sharedInstance.ismaleselected = false
        CardName.sharedInstance.isfemaleselected = false
        return cell
        
        
        
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
       if scrollView == tableview
       {
        print("isscrolling")
        self.isscrolling = true
        }
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        isscrolling = false
        
    }
    func steppervalue(stepper : Service_TableViewCell)
    {
        
    }
    @objc func stepperValueChanged(stepper: UIStepper) {
       
        let indexpath =  IndexPath(row: stepper.tag, section: 0)
        
        self.timeslotview.isHidden = false
         self.line_view.isHidden = false
        let screenSize = UIScreen.main.bounds
        let screenWidth = screenSize.width
        let screenheight = screenSize.height
    
        print("stepper value clicked table height::screenwidth\(screenWidth)")
        
        if screenWidth == 320.0
        {
            //   videoPlayer.view.frame = CGRect(x: 15.0, y: 76.0, width: 289.0, height: 182.0)
            self.tableview.frame = CGRect(x: 0, y: 92.0, width: 320.0, height: 335)
        }
        else if screenWidth == 375.0
        {
            print("iphone 6")
            self.tableview.frame = CGRect(x: 0, y: 120, width: 375.0, height: 390)
            //  videoPlayer.view.frame = CGRect(x: 15.0, y: 100, width: 345.0, height: 200.0)
            
        }
        else if screenWidth == 414.0
        {
            print("iphone 6s plus")
            self.tableview.frame = CGRect(x: 0, y: timeslotview.frame.origin.y + timeslotview.frame.height + 5 , width: 414.0, height:  530)
            //  videoPlayer.view.frame = CGRect(x: 15.0, y: 100.0, width: 384, height: 200.0)
            
        }
        else if screenWidth > 415
        {
            print("unexpested screensize")
            
        }
        if isscrolling
        {
            return
        }
          var stepper_id = Int(stepper.value)
           if clicked_row != indexpath.row
           {
            if type == "add"
            {
                stepper_value = 0
                clicked_row = indexpath.row
            }else if type == "sub"
            {
                  stepper_value = stepper_id
               // clicked_row = indexpath.row
            }else{
                stepper_value = 0
                 clicked_row = indexpath.row
            }
            
           }
        print("stepper_id\(stepper_id)")
        print("stepper value\(stepper_value)")
            if stepper_id > stepper_value
            {
                print("added")
                clicked_service.append(service_nameArray[indexpath.row])
                let click_amount = String(service_amountArray[indexpath.row])
                clicked_amount.append(click_amount)
                let clicked_service_id = service_id_Array[indexpath.row]
                clicked_id.append(clicked_service_id)
                let clicked_servicetime = String(service_Array[indexpath.row])
                clicked_time.append(clicked_servicetime)
                //indexpath_array.append(indexpath.row)
                print("clicked_path\(clicked_indexpath)")
                if clicked_indexpath == indexpath.row
                {
                    print("service_count\(service_count.count)")
                    for i in 0..<CardName.sharedInstance.selected_indexpath.count
                    {
                        if CardName.sharedInstance.selected_indexpath[i] == clicked_indexpath{
                          //[i] += 1
                            service_stepper_array[indexpath.row] = Int(stepper.value)
                            clicked_stepper_array.append(service_stepper_array[indexpath.row])
                            service_count.append(1)
                            CardName.sharedInstance.selected_service_count[i] = Int(stepper.value)
                            print("selected service count\(CardName.sharedInstance.selected_service_count)")
                        }
                    }
                }else{
                    CardName.sharedInstance.selected_indexpath.append(indexpath.row)
                    CardName.sharedInstance.selected_service_count.append(Int(stepper.value))
                    service_stepper_array[indexpath.row] = Int(stepper.value)
                    clicked_stepper_array.append(service_stepper_array[indexpath.row])
                    service_count.append(1)
                }
                clicked_indexpath = indexpath.row
               
                print("clicked_path::\(indexpath.row)")
                equipments.append(["SaloonServiceID" : clicked_service_id,"ServiceSubCategoryPrice" : click_amount])
                // equipments.append(["SaloonServiceID" : clicked_id[i],"ServiceSubCategoryPrice" : clicked_amount[i]])
                CardName.sharedInstance.salon_service["result"] = equipments
                //
                print("clicked::\(clicked_id)")
                stepper_value = stepper_id
                type = "add"
                
            }
           
            else {
                print("else part")
                var romove_position = 0
                var clicked_service_id = service_id_Array[indexpath.row]
                for i in 0..<clicked_id.count
                {
                    if clicked_service_id == clicked_id[i]
                    {
                        romove_position = i
                        // return
                    }
                }
                clicked_id.remove(at: romove_position)
                clicked_amount.remove(at: romove_position)
                clicked_time.remove(at: romove_position)
                clicked_service.remove(at: romove_position)
                clicked_stepper_array.remove(at: romove_position)
                equipments.remove(at: romove_position)
                //indexpath_array.remove(at: romove_position)
                //service_count.remove(at: romove_position)
                CardName.sharedInstance.salon_service["result"] = equipments
                clicked_row = indexpath.row
                 type = "sub"
                 print("clicked_id\(clicked_amount)")
                 if clicked_id.count == 0
                 {
                    stepper_value = 0
                    self.line_view.isHidden = true
                    type = "add"
                    self.btn_payoutlet.setTitle("Confirm", for: .normal)
                    let screenSize = UIScreen.main.bounds
                    let screenWidth = screenSize.width
                    let screenheight = screenSize.height
                    self.timeslotview.isHidden = true
                    print("screenwidth\(screenWidth)")
                    print("Clicked count table height = 0")
                    if screenWidth == 320.0
                    {
                        //videoPlayer.view.frame = CGRect(x: 15.0, y: 76.0, width: 289.0, height: 182.0)
                        self.tableview.frame = CGRect(x: 0, y: 52.0, width: 320.0, height: screenheight - 200)
                    }
                    else if screenWidth == 375.0
                    {
                        print("iphone 6")
                        self.tableview.frame = CGRect(x: 0, y: 60.0, width: 375.0, height: screenheight - 220)
                        //videoPlayer.view.frame = CGRect(x: 15.0, y: 100, width: 345.0, height: 200.0)
                        
                    }
                    else if screenWidth == 414.0
                    {
                        print("iphone 6s plus")
                        self.tableview.frame = CGRect(x: 0, y: 69.0, width: 414.0, height: screenheight - 280)
                        //videoPlayer.view.frame = CGRect(x: 15.0, y: 100.0, width: 384, height: 200.0)
                        
                    }
                    else if screenWidth > 415
                    {
                        print("unexpested screensize")
                        
                    }
                }
             //   stepper_value = stepper_id
               
               //  stepper_value = stepper_id
                }
        
        
                let clicked_day =
                    DateArray[clicked_index] as! String
        

                let clicked_servicetime = service_Array[indexpath.row] as! Int
        print("Stepper Changed:clicked_day\(clicked_day)")
        print("Stepper Changedclicked_time\(clicked_servicetime)")
                getTimeslot(clicked_day : clicked_day,clicked_servicetime : clicked_servicetime)
       //self.collectionview.reloadData()
        self.timecollectionview.reloadData()
        self.caluculate_amount()
        self.calculate_time()
    }

    func getpricedetails()
    {
        CardName.sharedInstance.type_array.removeAll()
        CardName.sharedInstance.price_Array.removeAll()
        CardName.sharedInstance.name_array.removeAll()
        let url = CardName.sharedInstance.getpricedetails
        print("info_url\(url)")
        let type = UserDefaults.standard.string(forKey: "token")!
        let token = UserDefaults.standard.string(forKey: "Authorizent_token")!
        
        
        let header = [
            "Authorization": "\(type) \(token)",
        ]
        let service_id = UserDefaults.standard.string(forKey: "service_id")
        let id = UserDefaults.standard.string(forKey: "clicked_id")
        CardName.sharedInstance.endslot_id = String(endservice_id)
        CardName.sharedInstance.startslot_id = String(self.bookingtime_from)
        if self.Bookingdate == ""
        {
            let inputFormatter = DateFormatter()
            inputFormatter.dateFormat = "MMM dd"
            let showDate = inputFormatter.date(from: self.DateArray[0])
            inputFormatter.dateFormat = "dd-MM"
            let resultString = inputFormatter.string(from: showDate!)
            print(resultString)
            self.Bookingdate = resultString + "-2018"
            print("service_bookingtime\(self.Bookingdate)")
            UserDefaults.standard.set(self.Bookingdate, forKey: "service_bookingdate")
        }
        let parameters: [String: Any] = [
            "SaloonDetailsID" : id!,
            "totalEstimatedPrice" : service_salon_amount,
            "StartTimeID" : startservice_id,
            "EndTimeID" : endservice_id,
            "BMHSOfferDetailsID" : 0,
            "BookingDate" : self.Bookingdate,
            "ServiceIDs" : clicked_id,
            "IsGuest": false,
            //"ServiceIDs": service_id!
        ]
        
        print("Total Amount Parms\(parameters)")
        Alamofire.request(url, method: .post,parameters: parameters, encoding: JSONEncoding.default,headers : header)
            .responseJSON { response in
                if response.error == nil
                {
                    print("response\(response)")
                    let responce_value = response.result.value as! NSDictionary
                    CardName.sharedInstance.price_dictionary = response.result.value as! NSDictionary
                    let payment_type = responce_value.value(forKey: "PaymentType") as! Int
                    CardName.sharedInstance.payment_mode = payment_type
                    let pricesplitup = responce_value.value(forKey: "PriceSplitUpDetails") as! NSArray
                    let total_amount = responce_value.value(forKey: "TotalAmount") as! String
                    CardName.sharedInstance.service_total_Amount = total_amount
                    print("amount\(CardName.sharedInstance.service_total_Amount)")
                    for i in 0..<pricesplitup.count
                    {
                        let result_value = pricesplitup[i] as! NSDictionary
                        let name = result_value.value(forKey: "Name") as! String
                        let price = result_value.value(forKey: "Price") as! String
                        let type = result_value.value(forKey: "Type") as! String
                        CardName.sharedInstance.name_array.append(name)
                        CardName.sharedInstance.price_Array.append(price)
                        CardName.sharedInstance.type_array.append(type)
                    }
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let subclass = storyboard.instantiateViewController(withIdentifier: "Checkout_ViewController") as! Checkout_ViewController
                    self.present(subclass,animated: true,completion: nil)
                }else{
                    let alert = UIAlertController(title: "Alert", message: "Network Error, Try Again!", preferredStyle: .alert)
                    let okaction = alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
                    self.present(alert,animated: true,completion: nil)
                }
              
        }
    }
    func calculate_time()
    {
       
       if clicked_time.count != 0
       {
        for i in 0..<clicked_time.count
        {
            let time = Int(clicked_time[i])
            total_time += time ?? 0
        }
        print("total_time\(total_time)")
        let sec = total_time * 60
        let (h, m) = secondsToHoursMinutesSeconds(seconds: sec)
        print("total_time\(h) \(m)")
        if h == 0
        {
            self.clicked_salon_endtime = Double(m)
            CardName.sharedInstance.total_time = "\(m)Min"
        }else
        {
            let time = h * 60
            self.clicked_salon_endtime = Double(time) + Double(m)
            CardName.sharedInstance.total_time = "\(h)hr \(m)Min"
        }
       }
    }
   func caluculate_amount()
    {
  
        print("dic\(CardName.sharedInstance.salon_service)")
        var total_amount = 0.0
        print("clickeD_amount\(clicked_amount)")
      if clicked_amount.count != 0
      {
     for i in 0..<clicked_amount.count
     {
        total_amount += Double(clicked_amount[i]) ?? 0.0
     }
        print("total_amount\(total_amount)")
        CardName.sharedInstance.total_amount = "\(total_amount)"
        self.service_salon_amount = Int(total_amount)
        print("service_amount\(service_salon_amount)")
        self.btn_payoutlet.setTitle("Pay ₹\(total_amount)", for: .normal)
        
        }
    }
    func secondsToHoursMinutesSeconds (seconds : Int) -> (Int, Int) {
        return (seconds / 3600, (seconds % 3600) / 60)
    }
    @IBOutlet weak var tableview: UITableView!
   
//    @IBAction func didtouchstepper(_ sender:UIStepper)
//    {
//          let indexpath =  IndexPath(row: sender.tag, section: 0)
//            let cell = tableview.cellForRow(at: indexpath) as! Service_TableViewCell
//        cell.service_countLbl.isHidden = false
//        if sender.value == 0
//        {
//            cell.service_countLbl.isHidden = true
//        }
//        else{
//             cell.service_countLbl.text = "\(Int(sender.value))"
//        }
//
//
//    }

    override func viewDidLoad()
    {
        super.viewDidLoad()
        let button = UIButton(type: .custom)
        //set image for button
        button.setImage(UIImage(named: "back"), for: .normal)
        //add function for button
        //button.addTarget(self, action: #selector(fbButtonPressed), for: .touchUpInside)
        //set frame
           button.translatesAutoresizingMaskIntoConstraints = false
        
        button.widthAnchor.constraint(equalToConstant: 10).isActive = true
        
        button.heightAnchor.constraint(equalToConstant: 10).isActive = true
        button.frame = CGRect(x: 0, y: 0, width: 10, height: 10)
        
        let barButton = UIBarButtonItem(customView: button)
        //assign button to navigationbar
        self.navigationItem.rightBarButtonItem = barButton
if CardName.sharedInstance.isaddservice_clicked
{
    timeclicked = true
    
}
      datecalculation()
      tableview.register(UINib(nibName: "Service_TableViewCell", bundle: nil), forCellReuseIdentifier: "Cell")
        
        if CardName.sharedInstance.isaddservice_clicked
        {
            clicked_service = CardName.sharedInstance.selected_servicename
            clicked_id = CardName.sharedInstance.selected_serviceid
            
            clicked_amount = CardName.sharedInstance.selected_serviceamount
            clicked_time = CardName.sharedInstance.service_selected_time
            service_time_Array = CardName.sharedInstance.service_selected_time
           //clicked_stepper_array = CardName.sharedInstance.selected_service_count
            equipments = CardName.sharedInstance.salon_service["result"] as! [[String : Any]]
            
            caluculate_amount()
            calculate_time()
            
        }else{
            
        }
       
        
        
        //Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        
       
        let salonname = UserDefaults.standard.string(forKey: "Salon_Name")
        self.navItem.topItem?.title = salonname
       
        let screenSize = UIScreen.main.bounds
        let screenWidth = screenSize.width
        let screenheight = screenSize.height
        
        print("screenwidth\(screenWidth)")
        print("Did appear table height")
        if screenWidth == 320.0
        {
            //   videoPlayer.view.frame = CGRect(x: 15.0, y: 76.0, width: 289.0, height: 182.0)
            self.tableview.frame = CGRect(x: 0, y: 52.0, width: screenWidth, height: screenheight - 200)
        }
        else if screenWidth == 375.0
        {
            print("iphone 6")
            self.tableview.frame = CGRect(x: 0, y: 60.0, width: 375.0, height: screenheight - 220)
            //  videoPlayer.view.frame = CGRect(x: 15.0, y: 100, width: 345.0, height: 200.0)

        }
        else if screenWidth == 414.0
        {
            print("iphone 6s plus")
            self.tableview.frame = CGRect(x: 0, y: 69.0, width: 414.0, height: screenheight - 280)
            //  videoPlayer.view.frame = CGRect(x: 15.0, y: 100.0, width: 384, height: 200.0)

        }
        else if screenWidth > 415
        {
            print("unexpected screensize")

        }
        
    }
    func datecalculation()
    {
        DateArray.removeAll()
        weekdayArray.removeAll()
        let today = Date()
        var tomorrow1 = Date()
        var nextday1 = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM d, yyyy"
        var todaystring = dateFormatter.string(from: today)
        print("today_string\(todaystring)")
      
        if let weekday = getDayOfWeek(todaystring) {
            print("weekday\(weekday)")
            CardName.sharedInstance.today_day_int = weekday
            print("day\(CardName.sharedInstance.today_day_int)")
            weekdayArray.append(weekday)
        }
        todaystring = todaystring.replacingOccurrences(of: ", 2019", with: "")
        DateArray.append(todaystring)
        if var tomorrow = today.tomorrow{
            tomorrow1 = today.tomorrow!
            var tomorrowString = dateFormatter.string(from: tomorrow)
          
            if let weekday = getDayOfWeek(tomorrowString) {
                print("weekday\(weekday)")
                weekdayArray.append(weekday)
            }
            tomorrowString = tomorrowString.replacingOccurrences(of: ", 2019", with: "")
            print("today_string\(tomorrowString)")
            DateArray.append(tomorrowString)
            print("tomorrow\(tomorrowString)")
        // "Mar 23, 2017, 00:14 AM\n"
        }
        for i in 0..<14
        {
            if i == 0
            {
            if var nextday = tomorrow1.tomorrow{
                var nextdaystring = dateFormatter.string(from: nextday)
             print("today_string\(nextdaystring)")
               
                if let weekday = getDayOfWeek(nextdaystring) {
                    print("weekday\(weekday)")
                    weekdayArray.append(weekday)
                }
                nextdaystring = nextdaystring.replacingOccurrences(of: ", 2019", with: "")
                DateArray.append(nextdaystring)
            nextday1 = tomorrow1.tomorrow!
              }
            }else{
                if let nextday = nextday1.tomorrow{
                    var nextdaystring = dateFormatter.string(from: nextday)
                  
                    nextday1 = nextday
                    if let weekday = getDayOfWeek(nextdaystring) {
                        print("weekday\(weekday)")
                        weekdayArray.append(weekday)
                    }
                    nextdaystring = nextdaystring.replacingOccurrences(of: ", 2019", with: "")
                    print("today_string\(nextdaystring)")
                    DateArray.append(nextdaystring)
                }
            }
        }
        print("date_count\(DateArray.count)")
    }
    func getDayOfWeek(_ today:String) -> Int? {
        let formatter  = DateFormatter()
        formatter.dateFormat = "MMM d, yyyy"
        // formatter.dateStyle = .long
       //formatter.timeStyle = .long
      //formatter.timeZone = TimeZone(secondsFromGMT: Int(+5.30))
        print("today+date\(today)")
        let formatter1 = DateFormatter()
        formatter1.dateFormat = "yyyy-MM-dd"
     // formatter.timeZone = NSTimeZone(abbreviation: "GMT+5:30") as! TimeZone
       
        guard let todayDate = formatter.date(from: today) else {
            print("error")
            return nil
            
        }
        print("date\(formatter.string(from: todayDate))")
        let myCalendar = Calendar(identifier: .gregorian)
        let weekDay = myCalendar.component(.weekday, from: todayDate)
        print("today_date\(weekDay)")
        return weekDay
    }
    
    func getDetails()
    {
        
        service_amountArray.removeAll()
        service_nameArray.removeAll()
        service_Array.removeAll()
       service_stepper_array.removeAll()
        service_id_Array.removeAll()
        let clicked_id = UserDefaults.standard.string(forKey: "clicked_id")
        print("clicked_id\(clicked_id)")
       
        let url = "\(CardName.sharedInstance.info_serviceUrl)?saloonDetailsID=\(clicked_id!)"
        print("info_url\(url)")
        let type = UserDefaults.standard.string(forKey: "token")!
        let token = UserDefaults.standard.string(forKey: "Authorizent_token")!
        let header = [
            "Authorization": "\(type) \(token)",
        ]
        Alamofire.request(url, method: .get,encoding: JSONEncoding.default,headers : header)
            .responseJSON { response in
                print("Details_responce\(response)")
                if response.error == nil
                {
                    let responce_Array = response.result.value as! NSArray
                    print(responce_Array)
                    
                    for i in 0..<responce_Array.count
                    {
                        let responce_value = responce_Array[i] as! NSDictionary
                        if self.gender_type == ""                         {
                            
                            let service_name = responce_value.value(forKey: "Name") as! String
                            self.service_nameArray.append(service_name)
                            let service_amout = responce_value.value(forKey: "Price") as! Double
                            self.service_amountArray.append(service_amout)
                            let service_time = responce_value.value(forKey: "Time") as! Int
                            self.service_Array.append(service_time)
                            let service_id = responce_value.value(forKey: "Id") as! Int
                            self.service_id_Array.append(service_id)
                            let service_gender = responce_value.value(forKey: "Gender") as! Int
                            self.service_genderArray.append(service_gender)
                            self.service_stepper_array.append(0)
                        }
                        else if self.gender_type == "male"
                        {
                            let service_gender = responce_value.value(forKey: "Gender") as! Int
                            if service_gender == 1
                            {
                                let responce_value = responce_Array[i] as! NSDictionary
                                let service_name = responce_value.value(forKey: "Name") as! String
                                self.service_nameArray.append(service_name)
                                let service_amout = responce_value.value(forKey: "Price") as! Double
                                self.service_amountArray.append(service_amout)
                                let service_time = responce_value.value(forKey: "Time") as! Int
                                self.service_Array.append(service_time)
                                let service_id = responce_value.value(forKey: "Id") as! Int
                                self.service_id_Array.append(service_id)
                                print("Male service id appended")
                                let service_gender = responce_value.value(forKey: "Gender") as! Int
                                self.service_genderArray.append(service_gender)
                                self.service_stepper_array.append(0)
                            }
                            
                            
                        }else if self.gender_type == "female"
                        {
                        let service_gender = responce_value.value(forKey: "Gender") as! Int
                            if service_gender == 2
                            {
                                let responce_value = responce_Array[i] as! NSDictionary
                                let service_name = responce_value.value(forKey: "Name") as! String
                                self.service_nameArray.append(service_name)
                                let service_amout = responce_value.value(forKey: "Price") as! Double
                                self.service_amountArray.append(service_amout)
                                let service_time = responce_value.value(forKey: "Time") as! Int
                                self.service_Array.append(service_time)
                                let service_id = responce_value.value(forKey: "Id") as! Int
                                self.service_id_Array.append(service_id)
                                print("feMale service id appended")
                                let service_gender = responce_value.value(forKey: "Gender") as! Int
                                self.service_genderArray.append(service_gender)
                                self.service_stepper_array.append(0)
                            }
                        }
                        
                    }
                    self.activityindicator.stopAnimating()
                    self.activityindicator.isHidden = true
                    self.tableview.reloadData()
                }else{
                    let alert = UIAlertController(title: "Alert", message: "Network Error, Try Again!", preferredStyle: .alert)
                    let okaction = alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
                    self.present(alert,animated: true,completion: nil)
                }
                
                
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
   
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension Date {
    
    var tomorrow: Date? {
        return Calendar.current.date(byAdding: .day, value: 1, to: self)
    }
}
