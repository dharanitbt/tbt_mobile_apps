//
//  CollectionViewCell.swift
//  BMHS
//
//  Created by TechBT on 27/08/18.
//  Copyright © 2018 TechBT. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var CollectionView: UIView!
    @IBOutlet weak var collection_img: UIImageView!
    @IBOutlet weak var collection_Name: UILabel!
    
}
