//  Infosalon_ViewController.swift
//  BMHS
//
//  Created by TechBT on 30/08/18.
//  Copyright © 2018 TechBT. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import AARatingBar
import Alamofire
import Cosmos
import MapKit

class Infosalon_ViewController: UIViewController,CLLocationManagerDelegate,UITableViewDelegate,UITableViewDataSource,UIGestureRecognizerDelegate {
    @IBOutlet weak var address_text: UITextView!
   
    var facility_Array = [String]()
    var contactnumber_Array = [String]()
    var height1: CGFloat = 0
    var facility_count: CGFloat = 0
    var x:CLLocationDegrees = 0.0
    var y:CLLocationDegrees = 0.0
    var weekday = NSData()
    
    //   var Date_Array = [String]()
   //  var timings_Array = [String]()
  //
    @IBOutlet weak var facility_view: UIView!
    @IBOutlet weak var timing_view: UIView!
    @IBOutlet weak var scrollview: UIScrollView!
    
    @IBOutlet weak var facility_lbl: UILabel!
    @IBOutlet weak var rating_view: UIView!
    @IBOutlet weak var contact_label: UILabel!
    @IBOutlet weak var contact_view: UIView!
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var count = 0
        if tableView == contact_tableview
        {
            count = self.contactnumber_Array.count
            
        }
        else if tableView == facilities_tableview
        {
            
            count = self.facility_Array.count
            
        }
        else if tableView == timings_tableview
        {
            count = CardName.sharedInstance.TimeArray.count
        }
        return count
    }
    
    @IBOutlet weak var timings_tableview: UITableView!
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == contact_tableview
        {
            tableView.deselectRow(at: indexPath, animated: false)
        }
        if tableView == timings_tableview
        {
            tableView.deselectRow(at: indexPath, animated: false)
        }
    }
   
//
    
    
    
//     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        if tableView == contact_tableview
//        {
//            return 44
//        }
//        if tableView == facilities_tableview
//        {
//            return 44
//        }
//        return 49
//    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == contact_tableview
        {
            let cell = contact_tableview.dequeueReusableCell(withIdentifier: "CustomCellOne", for: indexPath) as! Contact_TableViewCell
            cell.btn_call.tag = indexPath.row
            cell.btn_call.addTarget(self, action: #selector(btn_call(cell:)) , for: .touchUpInside )
            
            cell.contact_number.text = self.contactnumber_Array[indexPath.row] as! String
//            CardName.sharedInstance.contactarray_count = contactnumber_Array.count
//            CardName.sharedInstance.contact_Array = contactnumber_Array
            
            return cell
        }
        else if tableView == facilities_tableview
        {
            let cell = facilities_tableview.dequeueReusableCell(withIdentifier: "CustomCellTwo", for: indexPath) as! Facilities_TableViewCell
            height1 = cell.bounds.height
            print("cell height :\(height1)")
            
            cell.facilities.text = facility_Array[indexPath.row] as! String
            facility_count = CGFloat(facility_Array.count)
            
            return cell
        }
        else if tableView == timings_tableview
        {
            let cell = timings_tableview.dequeueReusableCell(withIdentifier: "CustomCellThree", for: indexPath) as! timings_TableViewCell
            cell.date_lbl.text = CardName.sharedInstance.DateArray[indexPath.row] as! String
            cell.timing_lbl.text = CardName.sharedInstance.TimeArray[indexPath.row] as! String
          var weekday = Date().dayOfWeek()
            print("Weekday : \(weekday)")
            if weekday == CardName.sharedInstance.DateArray[indexPath.row]
            {
                cell.date_lbl.text = ("\(CardName.sharedInstance.DateArray[indexPath.row] as! String) - TODAY")
 
                cell.date_lbl.font = UIFont(name: "ProximaNova-Bold", size: 16.0)
                cell.timing_lbl.font = UIFont(name: "ProximaNova-Bold", size: 16.0)
                cell.date_lbl.textColor = UIColor.darkText
                cell.timing_lbl.textColor = UIColor.darkText
              
            }
            return cell
        }
        
        
        return UITableViewCell()
    }
    @objc func btn_call(cell: Contact_TableViewCell) {
        //Get the indexpath of cell where button was tapped
        let indexpath =  IndexPath(row: cell.tag, section: 0 )
        
        if let url = URL(string: "tel://\(contactnumber_Array[(indexpath.row)])"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
        print(indexpath.row)
    }
//
    
  
    
        
    override func viewDidLayoutSubviews() {
//        contact_tableview.frame = CGRect(x:   contact_tableview.frame.origin.x, y:   contact_tableview.frame.origin.y , width:   contact_tableview.frame.size.width, height:   contact_tableview.contentSize.height )
//              contact_view.frame = CGRect(x:   contact_view.frame.origin.x, y:   contact_view.frame.origin.y , width:  contact_view.frame.size.width, height:   contact_tableview.frame.height + contact_label.frame.height + 5 )
//        contact_tableview.reloadData()
        
    }
    
       
        
    
        
        
    
    @IBOutlet weak var facilities_tableview: UITableView!
    
    var locationManager = CLLocationManager()
    override func viewDidLoad() {
        super.viewDidLoad()
        //mapview.isMyLocationEnabled = true
        // self.scrollview.contentSize = self.scrollview.superview!.bounds.size
        
        
        

        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        mapview.isUserInteractionEnabled = true
        mapview.addGestureRecognizer(tap)
        
        
        
        //self.view.addSubview(mapview)
        
        
      //  self.scrollview.contentInsetAdjustmentBehavior = .automatic
        contact_tableview.register(UINib(nibName: "Contact_TableViewCell", bundle: nil), forCellReuseIdentifier: "CustomCellOne")
        facilities_tableview.register(UINib(nibName: "Facilities_TableViewCell", bundle: nil), forCellReuseIdentifier: "CustomCellTwo")
        timings_tableview.register(UINib(nibName: "timings_TableViewCell", bundle: nil), forCellReuseIdentifier: "CustomCellThree")
        self.locationManager.delegate = self
      
       
        //        rating_bar.ratingDidChange = { ratingValue in
        //            // get current selected rating
        //        }
        // Do any additional setup after loading the view.
    }
    
    
    @objc func handleTap(_ sender: UITapGestureRecognizer)
    {
        
        
        //let coordinate = CLLocationCoordinate2DMake(x,y)
        
    }
    
    
    @IBAction func btn_mapview(_ sender: Any) {
        print("Here came")
        let source = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: x, longitude: y)))
        source.name = "Source"
        
        
        MKMapItem.openMaps(with: [source], launchOptions: [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving])
//        if let url = URL(string: ("https://maps.apple.com/&daddr=\(x),\(y)")), UIApplication.shared.canOpenURL(url) {
//            if #available(iOS 10, *) {
//                UIApplication.shared.open(url)
//            } else {
//                UIApplication.shared.openURL(url)
//            }
//        }
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
    
        
        
        let salonname = UserDefaults.standard.string(forKey: "Salon_Name")
        self.navigationBar.topItem?.title = salonname
//        let rowheight = CGFloat(6 * 44) + 10
//        print("Row Height:\(rowheight)")
//        facility_view.frame = CGRect(x: self.facility_view.frame.origin.x , y: facility_view.frame.origin.y, width: facility_view.frame.width
//            , height: rowheight + 20)
//           facilities_tableview.frame = CGRect(x: facilities_tableview.frame.origin.x, y: facilities_tableview.frame.origin.y, width: facilities_tableview.frame.width, height: facilities_tableview.frame.height + 300)
//        print("real height:\(height1)")
//        print("count facility:\(facility_count)")
//        print("Tableview height : \(facilities_tableview.frame.height)")
//        timing_view.frame = CGRect(x: self.timing_view.frame.origin.x, y: self.facility_view.frame.origin.y + facility_view.frame.height +  10, width: self.timing_view.frame.width, height: self.timing_view.frame.height )
//        timings_tableview.frame = CGRect(x: timings_tableview.frame.origin.x, y: 40, width: timings_tableview.frame.width, height: timings_tableview.frame.height)
//     contact_tableview.frame = CGRect(x:   contact_tableview.frame.origin.x, y:   contact_tableview.frame.origin.y , width:   contact_tableview.frame.size.width, height:   contact_tableview.contentSize.height )
//        contact_view.frame = CGRect(x:   contact_view.frame.origin.x, y:   contact_view.frame.origin.y , width:  contact_view.frame.size.width, height:   contact_tableview.frame.height + contact_label.frame.height + 5 )
    }
    
    @IBOutlet weak var contact_tableview: UITableView!
    override func viewWillAppear(_ animated: Bool) {
        
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                print("No access")
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
                // self.locationManager.delegate = self
                // self.locationManager.requestAlwaysAuthorization()
                self.locationManager.requestWhenInUseAuthorization()
                self.locationManager.startUpdatingLocation()
                self.mapview?.isMyLocationEnabled = true
                
            }
        } else {
            print("Location services are not enabled")
        }
         getDetails()
    }
    @IBOutlet weak var ratingbar: CosmosView!
    func getDetails()
    {
        facility_Array.removeAll()
        contactnumber_Array.removeAll()
        CardName.sharedInstance.DateArray.removeAll()
        CardName.sharedInstance.TimeArray.removeAll()
        let clicked_id = UserDefaults.standard.string(forKey: "clicked_id")
        print("clicked_id\(clicked_id)")
        let parameters: [String: Any] = [
            "saloonID" : clicked_id
        ]
        let url = CardName.sharedInstance.infoDetails_url
        print("info_url\(url)")
        let type = UserDefaults.standard.string(forKey: "token")!
        let token = UserDefaults.standard.string(forKey: "Authorizent_token")!
        
        
        let header = [
            "Authorization": "\(type) \(token)",
        ]
        Alamofire.request(url, method: .post,parameters: parameters, encoding: JSONEncoding.default,headers : header)
            .responseJSON { response in
                if response.error == nil  {
                print("infosalon_response\(response)")
                let response_value = response.result.value as! NSDictionary
                
                let latitute = response_value.value(forKey: "Latitude") as! Double
                let longititute = response_value.value(forKey: "Longitude") as! Double
                self.x = latitute as! CLLocationDegrees
                self.y = longititute as! CLLocationDegrees
                print("x====\(self.x),y=====\(self.y)")
                let address = response_value.value(forKey: "Address") as! String
                let salon_name = response_value.value(forKey: "Name") as! String
                print(latitute)
                var position = CLLocationCoordinate2DMake(latitute,longititute)
                var gmsposititon = GMSCameraPosition(target: position, zoom: 15.0, bearing: 100, viewingAngle: 360)
                var marker = GMSMarker(position: position)
                marker.title = salon_name
                marker.map = self.mapview
                self.mapview.camera = gmsposititon
                self.mapview.selectedMarker = marker
                //self.mapview.animate(to: position)
                var buttonText: NSString = "Address \n \(address) " as NSString
                
                //getting the range to separate the button title strings
                var newlineRange: NSRange = buttonText.range(of: "\n")
                
                //getting both substrings
                var substring1: NSString = ""
                var substring2: NSString = ""
                if(newlineRange.location != NSNotFound) {
                    substring1 = buttonText.substring(to: newlineRange.location) as NSString
                    substring2 = buttonText.substring(from: newlineRange.location) as NSString
                }
                
                //assigning diffrent fonts to both substrings
                // let font:UIFont? = UIFont(name: "Arial", size: 8.0)
                let myString = substring1
                let myAttribute = [ NSAttributedStringKey.foregroundColor: UIColor.black ,  NSAttributedStringKey.font: UIFont(name: "Arial", size: 17.0)! ]
                let myAttrString = NSAttributedString(string: myString as String, attributes: myAttribute)
                
                let font1:UIFont? = UIFont(name: "Arial", size: 15.0)
                let myString1 = substring2
                let myAttribute1 = [ NSAttributedStringKey.foregroundColor: UIColor.black,  NSAttributedStringKey.font: UIFont(name: "Arial", size: 15.0)!]
                
                let myAttrString1 = NSAttributedString(string: myString1 as String, attributes: myAttribute1)
                
                //appending both attributed strings
                
                let combination = NSMutableAttributedString()
                
                combination.append(myAttrString)
                combination.append(myAttrString1)
                //combination.append(image1String)
                self.address_text.attributedText = combination
                let rating = response_value.value(forKey: "Rating") as! Double
                print("rating\(rating)")
            
                self.ratingbar.rating = rating
                let timings = response_value.value(forKey: "Timing") as! NSArray
                print("timings\(timings)")
                for i in 0..<timings.count
                {
                    let timing_value = timings[i] as! NSDictionary
                    let date = timing_value.value(forKey: "DayName") as! String
                    let timings = timing_value.value(forKey: "TimeDesc") as! String
                    CardName.sharedInstance.DateArray.append(date)
                    CardName.sharedInstance.TimeArray.append(timings)
                }
                self.timings_tableview.reloadData()
                if response_value.value(forKey: "ContactNumber") is NSNull
                {
                    
                }else{
                    let contact_number = response_value.value(forKey: "ContactNumber") as! String
                    self.contactnumber_Array.append(contact_number)
                }
                if response_value.value(forKey: "ContactNumber2") is NSNull
                {
                    
                }else{
                    let additional_number = response_value.value(forKey: "ContactNumber2") as! String
                    self.contactnumber_Array.append(additional_number)
                }
                
                let facility = response_value.value(forKey: "Facility") as! NSArray
                for i in 0..<facility.count
                {
                    self.facility_Array.append(facility[i] as! String)
                }
                self.facilities_tableview.reloadData()
                self.contact_tableview.reloadData()
                
        }
                else
                {
                    let alert = UIAlertController(title: "Message", message: "Something went wrong! Please Try Again", preferredStyle: .alert)
                    let okaction = alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
                    self.present(alert,animated: true,completion: nil)
                }
        }
        
    }
    
   
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let location = locations.last
        print("here")
        let camera = GMSCameraPosition.camera(withLatitude: (location?.coordinate.latitude)!, longitude: (location?.coordinate.longitude)!, zoom: 25.0)
        
        self.mapview?.animate(to: camera)
        
        //Finally stop updating location otherwise it will come again and again in this delegate
        self.locationManager.stopUpdatingLocation()
        
    }
    @IBOutlet weak var navigationBar: UINavigationBar!
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBOutlet weak var mapview: GMSMapView!
    
    @IBAction func btn_back(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let subclass = storyboard.instantiateViewController(withIdentifier: "DashboardViewController") as! UITabBarController
        subclass.selectedIndex = 1
        self.present(subclass,animated: true,completion: nil)
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension Date {
    func dayOfWeek() -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE"
        return dateFormatter.string(from: self).capitalized
        // or use capitalized(with: locale) if you want
    }
}

//print(Date().dayOfWeek()!) // Wednesday
