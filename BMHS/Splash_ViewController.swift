//
//  Splash_ViewController.swift
//  BMHS
//
//  Created by TechBT on 02/11/18.
//  Copyright © 2018 TechBT. All rights reserved.
//

import UIKit
import Alamofire

class Splash_ViewController: UIViewController {
var user_id = ""
    var password = ""
    var user_token = ""
    var token_type = ""
    
    @IBOutlet weak var activityindicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.activityindicator.startAnimating()
        if UserDefaults.standard.string(forKey: "Authorizent_token") != nil
        {
            let authorizent = UserDefaults.standard.string(forKey: "Authorizent_token")
            print("splash auth\(authorizent)")
            let token = UserDefaults.standard.string(forKey: "token")
            print("splash token\(token)")
            if token == ""
            {
                registerdevice()
            }else
            {
                self.token_type = token ?? "bearer"
                let authorization_code = UserDefaults.standard.string(forKey: "Authorizent_token")
                self.user_token = authorization_code ?? ""
                validatetoken()
            }
        }else
        {
            registerdevice()
            
        }
  
        // Do any additional setup after loading the view.
    }
    func registerdevice()
    {
        let url = CardName.sharedInstance.device_register_url
        print("info_url\(url)")
        let uuid = UIDevice.current.identifierForVendor?.uuidString
        print("device_id\(uuid)")
   
       
        var systemVersion = UIDevice.current.systemVersion
       
        print("system\(systemVersion)")
        let parameters: [String: Any] = [
            "DeviceUniqueID" : "\(uuid!)VER\(systemVersion)",
        ]
        print("param\(parameters)")
        Alamofire.request(url, method: .post,parameters: parameters, encoding: JSONEncoding.default)
            .responseJSON { response in
                print("responce\(response)")
                if response.error == nil
                {
                    let responce_value = response.result.value as! NSDictionary
                    
                    let status = responce_value.value(forKey: "Status") as! Bool
                    if status
                    {
                        let id = responce_value.value(forKey: "ID") as! String
                        
                        self.user_id = id
                        UserDefaults.standard.set(self.user_id, forKey: "uuid")
                        self.password = id
                        self.guesttoken()
                    }else
                    {
                        let alert = UIAlertController(title: "Alert", message: "Plaease Try Again!", preferredStyle: .alert)
                        let okaction = alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
                        self.present(alert,animated: true,completion: nil)
                    }
                }else{
                    let alert = UIAlertController(title: "Alert", message: "Network Error, Try Again!", preferredStyle: .alert)
                    let okaction = alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
                    self.present(alert,animated: true,completion: nil)
                    print("error\(response.error)")
                }
              
        }
    }
    func guesttoken()
    {
        let url = CardName.sharedInstance.guest_token_url
        print("url\(url)")
        let headers = [
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        print("header\(headers)")
        let parameters: [String: Any] = [
            "username" : self.user_id,
            "password" : self.password,
            "grant_type" : "password"
        ]
        print("parameter\(parameters)")
        Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding()).responseJSON { (response:DataResponse<Any>) in
            print(response)
            print(response.error)
            if response.error == nil
            {
                let responce_value = response.result.value as! NSDictionary
                let token = responce_value.value(forKey: "access_token") as! String
                let token_sy = responce_value.value(forKey: "token_type") as! String
                self.user_token = token
                self.token_type = token_sy
                self.validatetoken()
            }
            else{
                let alert = UIAlertController(title: "Alert", message: "Network Error, Try Again!", preferredStyle: .alert)
                let okaction = alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
                self.present(alert,animated: true,completion: nil)
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    func validatetoken()
    {
        let url = CardName.sharedInstance.validate_token_url
        print("url\(url)")
        let headers = [
            "Authorization": "\(self.token_type) \(self.user_token)"
        ]
        print("header12\(headers)")
    
        Alamofire.request(url, method: .post, encoding: JSONEncoding.default,headers : headers)
            .responseJSON { response in
            print(response)
            print(response.error)
                if response.error == nil
                {
                   let responce_value = response.result.value as! NSDictionary
                    let status = responce_value.value(forKey: "Status") as! Bool
                    if status
                    {
                        UserDefaults.standard.set(self.token_type, forKey: "token")
                        UserDefaults.standard.set(self.user_token, forKey: "Authorizent_token")
                        self.activityindicator.stopAnimating()
                        print("standard\(UserDefaults.standard.string(forKey: "isFirst"))")
                        if UserDefaults.standard.string(forKey: "isFirst") == "1" || UserDefaults.standard.string(forKey: "isFirst") == nil
                        {
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let subclass = storyboard.instantiateViewController(withIdentifier: "LocationViewController") as! LocationViewController
                            self.present(subclass,animated: true,completion: nil)
                        }else{
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let subclass = storyboard.instantiateViewController(withIdentifier: "DashboardViewController") as! UITabBarController
                            self.present(subclass,animated: true,completion: nil)
                        }
                        
                    }else
                    {
                        self.registerdevice()
                    }
                }else
                {
                    let alert = UIAlertController(title: "Alert", message: "Network Error, Try Again!", preferredStyle: .alert)
                    let okaction = alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
                    self.present(alert,animated: true,completion: nil)
                    print("error\(response.error)")
                }
        }
    }

}
