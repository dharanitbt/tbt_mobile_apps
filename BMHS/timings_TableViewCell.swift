//
//  timings_TableViewCell.swift
//  BMHS
//
//  Created by TechBT on 04/09/18.
//  Copyright © 2018 TechBT. All rights reserved.
//

import UIKit

class timings_TableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBOutlet weak var timing_lbl: UILabel!
    @IBOutlet weak var date_lbl: UILabel!
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
