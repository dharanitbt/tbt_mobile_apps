//
//  Checkout_ViewController.swift
//  BMHS
//
//  Created by TechBT on 22/10/18.
//  Copyright © 2018 TechBT. All rights reserved.
//

import UIKit
import Alamofire
//import PaymentSDK
//import Instamojo
import SafariServices
class Checkout_ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource{
    @IBOutlet weak var paystore_button: UIButton!
    
    func callInstamojo(transactionID: Int , accesstoken: String)
    {
        let id = String(transactionID)
        
        
        
        let url = "https://test.instamojo.com/api/1.1/payment-requests/"
        print("url_online:::\(url)")
        let header: [String: String] = [
            "x-api-key" : "5b93672cd798d7d7eb89380bdd032dad",
            "x-auth-token" : "0b98e603a06efa0695ed524a7bd33cc1",
            "Content-type" : "application/json"
        ]
        let parameters: [String: Any] =
            [
                "phone" : UserDefaults.standard.string(forKey: "username")!,
                "amount" : CardName.sharedInstance.total_amount,
                "purpose" : UserDefaults.standard.string(forKey: "Salon_Name")!,
                "id"      : id,
                "redirect_url" :  "https://www.instamojo.com/integrations/android/redirect/"
                //"redirect_url" : "www.images.google.com"
        ]
        
        
        
        
        print("parameter result:::::\(parameters)")
        print(" header result::::::\(header)")
        Alamofire.request(url, method: .post,parameters: parameters,encoding: JSONEncoding.default,headers : header)
            .responseJSON { response in
                print("instamojo response\(response)")
                let responce_value = response.result.value as! NSDictionary
                let order_response = responce_value.value(forKey: "payment_request") as! NSDictionary
                let order_url = order_response.value(forKey: "longurl") as! String
                let svc = SFSafariViewController(url: URL(string: order_url)!)
                self.present(svc, animated: true, completion: nil)
                let sucess =  responce_value.value(forKey: "success") as! Int
        }
        
        
    }
    var salonDetailsId = ""
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return CardName.sharedInstance.selected_serviceamount.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 27
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = service_tableview.dequeueReusableCell(withIdentifier: "checkoutservice_TableViewCell", for: indexPath) as! checkoutservice_TableViewCell
        cell.service_name.text = CardName.sharedInstance.selected_servicename[indexPath.row] as! String
        cell.service_amount.text = "₹ \(CardName.sharedInstance.selected_serviceamount[indexPath.row])" as! String
        return cell
    }
    @IBOutlet weak var sub_total_view: UIView!
    @IBOutlet weak var gst_splitup_view: UIView!
    @IBOutlet weak var offer_view: UIView!

    @IBOutlet weak var cgst_lbl: UILabel!
    @IBOutlet weak var sgst_lbl: UILabel!
    @IBAction func btn_close(_ sender: Any) {
        self.background_view.isHidden = true
        // self.gst_splitup_view.isHidden = true
        print("Close Button Clicked")
        setView(view: gst_splitup_view, hidden: true)
    }
    
    @IBAction func btn_gst_splitup(_ sender: Any) {
        self.background_view.isHidden = false
        setView(view: gst_splitup_view, hidden: false)
    }
    
    @IBAction func addService(_ sender: Any)
    {
        
        
        CardName.sharedInstance.isaddservice_clicked = true
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let subclass = storyboard.instantiateViewController(withIdentifier: "SalonDetails_ViewController") as! UITabBarController
        subclass.selectedIndex = 1
        self.present(subclass,animated: true,completion: nil)
    }
    @IBOutlet weak var loyality_lbl: UILabel!
    
    @IBOutlet weak var totalamount_view: UIView!
    @IBOutlet weak var line_view: UIView!
    @IBOutlet weak var sub_view: UIView!
    @IBOutlet weak var background_view: UIView!
    @IBOutlet weak var gst_lbl: UILabel!
    var equipments = [[String:Any]]()
    @IBOutlet weak var gst_splitup: UIButton!
    @IBOutlet weak var service_subtotal: UILabel!
    @IBOutlet weak var service_time: UILabel!
    @IBOutlet weak var service_bookingtime: UILabel!
    @IBOutlet weak var salon_addr: UILabel!
    @IBOutlet weak var salon_Name: UILabel?
    @IBOutlet weak var salon_icon: UIImageView!
    @IBOutlet weak var total_view: UIView!
    @IBOutlet weak var btn_payout: UIButton!
    @IBOutlet weak var offer_amount: UILabel!

    @IBOutlet weak var back_image: UIImageView!
    
    @IBOutlet weak var get_view: UIView!
    @IBOutlet weak var gst_amount: UILabel!
    @IBOutlet weak var total_amount: UILabel!
    var day = 0
    let date = UserDefaults.standard.string(forKey: "date")
    let time = UserDefaults.standard.string(forKey: "time")
    
    @IBOutlet weak var loyality_view: UIView!
    
    
    override func viewDidAppear(_ animated: Bool) {
        
        var dayofweek = ""
        //  day = UserDefaults.standard.integer(forKey: "day")
        print("day\(CardName.sharedInstance.today_day_int)")
        let weekday = CardName.sharedInstance.today_day_int
        if weekday == 1
        {
            dayofweek = "Sunday"
            
        }else if weekday == 2
        {
            dayofweek = "Monday"
            
        }
        else if weekday == 3
        {
            dayofweek = "Tuesday"
            
        }else if weekday == 4
        {
            dayofweek = "Wednesday"
            
        }
        else if weekday == 5
        {
            dayofweek = "Thursday"
            
        }else if weekday == 6
        {
            dayofweek = "Friday"
            
        }
        else if weekday == 7
        {
            dayofweek = "Saturday"
            
        }
        total_amount.text = "₹ \(CardName.sharedInstance.service_total_Amount)"
        self.service_bookingtime?.text = "\(dayofweek),\(date!),\(time!)"
    }
    
    @IBAction func btn_back(_ sender: Any)
    {
        
    }
   
    override func viewDidLoad() {
        //btn_payout.isEnabled = false
        super.viewDidLoad()
        addNotificationToRecievePaymentCompletion()
        //self.view.addSubview(spinner)
        let tap = UITapGestureRecognizer(target: self, action: #selector(Checkout_ViewController.tapFunction))
        back_image.isUserInteractionEnabled = true

        back_image.addGestureRecognizer(tap)

        let mycolor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        self.paystore_button.layer.borderColor = mycolor.cgColor
        self.paystore_button.layer.borderWidth = 1
        let layer = self.salon_view.layer
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        layer.shadowRadius = 6.0
        layer.shadowOpacity = 0.1
        layer.masksToBounds = false
        self.salon_view.layer.cornerRadius = 5
        let layer1 = self.service_view.layer
        layer1.shadowColor = UIColor.black.cgColor
        layer1.shadowOffset = CGSize(width: 0.0, height: 2.0)
        layer1.shadowRadius = 6.0
        layer1.shadowOpacity = 0.1
        layer1.masksToBounds = false
        self.service_view.layer.cornerRadius = 5
        let totalview_layer = self.total_view.layer
        totalview_layer.shadowColor = UIColor.black.cgColor
        totalview_layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        totalview_layer.shadowRadius = 6.0
        totalview_layer.shadowOpacity = 0.1
        totalview_layer.masksToBounds = false
        self.total_view.layer.cornerRadius = 5
        let paystore = self.btn_storeout.layer
        paystore.shadowColor = UIColor.black.cgColor
        paystore.shadowOffset = CGSize(width: 0.0, height: 2.0)
        paystore.shadowRadius = 6.0
        paystore.shadowOpacity = 0.4
        paystore.masksToBounds = false
        let gstsplitup = self.gst_splitup_view.layer
        gstsplitup.shadowColor = UIColor.black.cgColor
        gstsplitup.shadowOffset = CGSize(width: 0.0, height: 2.0)
        gstsplitup.shadowRadius = 6.0
        gstsplitup.shadowOpacity = 0.1
        gstsplitup.masksToBounds = false
        self.gst_splitup_view.layer.cornerRadius = 5
        self.btn_storeout.layer.cornerRadius = 5
        service_tableview.register(UINib(nibName: "checkoutservice_TableViewCell", bundle: nil), forCellReuseIdentifier: "checkoutservice_TableViewCell")
        self.get_view.isHidden = true
        self.gst_splitup.isHidden = true
        self.offer_view.isHidden =  true
        self.salon_Name?.text = UserDefaults.standard.string(forKey: "Salon_Name")
        self.salon_addr.text = UserDefaults.standard.string(forKey: "Salon_Location")
        var salon_img = UserDefaults.standard.string(forKey: "Salon_Logo")
        let url = URL(string: salon_img ?? "")
        if url != nil
        {
            let data = try? Data(contentsOf: url!)
            
            if let imageData = data {
                let image = UIImage(data: imageData)
                salon_icon.image = image
            }
        }
        print("amount\(CardName.sharedInstance.service_total_Amount)")
        total_amount.text = CardName.sharedInstance.service_total_Amount
        self.service_time.text = CardName.sharedInstance.total_time
        self.service_subtotal.text = CardName.sharedInstance.total_amount
        switch CardName.sharedInstance.payment_mode {
        case 1:
            self.btn_payout.isHidden = true
            self.btn_storeout.isHidden = true
        case 2:
            self.btn_storeout.isHidden = false
            self.btn_payout.isHidden = true
        case 3:
            self.btn_storeout.isHidden = true
            self.btn_payout.isHidden = false
        case 4:
            self.btn_storeout.isHidden = false
            self.btn_payout.isHidden = false
            
        default:
            break
        }
        //type switch
        for i in 0..<CardName.sharedInstance.type_array.count
        {
            switch CardName.sharedInstance.type_array[i] {
            case "SubTotal":
                self.sub_total_view.isHidden = false
                self.service_subtotal.text = "₹ \(CardName.sharedInstance.price_Array[i])"
                //                self.sub_view.frame = CGRect(x: sub_view.frame.origin.x, y: sub_view.frame.origin.y, width: sub_view.frame.width, height: sub_total_view.frame.height + 50)
                //                self.line_view.frame = CGRect(x: line_view.frame.origin.x, y: sub_view.frame.origin.y + sub_view.frame.height + 10, width: line_view.frame.width, height: line_view.frame.height)
            //                self.totalamount_view.frame = CGRect(x: totalamount_view.frame.origin.x, y: line_view.frame.origin.y + 30, width: totalamount_view.frame.width, height: totalamount_view.frame.height)
            case "GST":
                if CardName.sharedInstance.price_Array[i] == "0.00"
                {
                    self.get_view.isHidden = true
                }else
                {
                    self.get_view.isHidden = false
                    self.gst_amount.text = "₹ \(CardName.sharedInstance.price_Array[i])"
                    self.gst_lbl.text = "₹ \(CardName.sharedInstance.price_Array[i])"
                    self.gst_splitup.isHidden = false
                    
                }
            case "SGST":
                print("name\(CardName.sharedInstance.name_array[i])")
                print("price\(CardName.sharedInstance.price_Array[i])")
                print("type\(CardName.sharedInstance.type_array[i])")
                self.sgst_lbl.text = "₹ \(CardName.sharedInstance.price_Array[i])"
                
            case "CGST":
                print("name1\(CardName.sharedInstance.name_array[i])")
                print("price2\(CardName.sharedInstance.price_Array[i])")
                print("type3\(CardName.sharedInstance.type_array[i])")
                self.cgst_lbl.text = "₹ \(CardName.sharedInstance.price_Array[i])"
                
            case "Offer":
                if CardName.sharedInstance.price_Array[i] == "- 0.00"
                {
                    self.offer_view.isHidden = true
                }else
                {
                    self.offer_view.isHidden = false
                print("name1\(CardName.sharedInstance.name_array[i])")
                print("price2\(CardName.sharedInstance.price_Array[i])")
                print("type3\(CardName.sharedInstance.type_array[i])")
                self.offer_amount.text = "₹ \(CardName.sharedInstance.price_Array[i])"
                }
                
            case "Loyalty":
                print("name1\(CardName.sharedInstance.name_array[i])")
                print("price2\(CardName.sharedInstance.price_Array[i])")
                print("type3\(CardName.sharedInstance.type_array[i])")
                self.loyality_lbl.text = "₹ \(CardName.sharedInstance.price_Array[i])"
            default:
                
                break
            }
        }
        
        
        
        // Do any additional setup after loading the view.
    }
    //    func beginPayment(){
    //         serv = serv.createProductionEnvironment()
    //         let type :ServerType = .eServerTypeProduction
    //        let order = PGOrder(orderID: "", customerID: "", amount: "", eMail: "", mobile: "")
    //        order.params = ["MID": "rxazcv89315285244163",
    //                        "ORDER_ID": "order1",
    //                        "CUST_ID": "cust123",
    //                        "MOBILE_NO": "7777777777",
    //                        "EMAIL": "username@emailprovider.com",
    //                        "CHANNEL_ID": "WAP",
    //                        "WEBSITE": "WEBSTAGING",
    //                        "TXN_AMOUNT": "100.12",
    //                        "INDUSTRY_TYPE_ID": "Retail",
    //                        "CHECKSUMHASH": "oCDBVF+hvVb68JvzbKI40TOtcxlNjMdixi9FnRSh80Ub7XfjvgNr9NrfrOCPLmt65UhStCkrDnlYkclz1qE0uBMOrmuKLGlybuErulbLYSQ=",
    //                        "CALLBACK_URL": "https://securegw-stage.paytm.in/theia/paytmCallback?ORDER_ID=order1"]
    //        self.txnController =  self.txnController.initTransaction(for: order) as?PGTransactionViewController
    //        self.txnController.title = "Paytm Payments"
    //        self.txnController.setLoggingEnabled(true)
    //        if(type != ServerType.eServerTypeNone) {
    //            self.txnController.serverType = type;
    //        } else {
    //            return
    //        }
    //        self.txnController.merchant = PGMerchantConfiguration.defaultConfiguration()
    //        self.tabBarController.delegate = self
    //        self.navigationController?.pushViewController(self.txnController, animated: true)
    //    }
    override func viewDidLayoutSubviews() {
        print("type array\(CardName.sharedInstance.type_array)")
        
        var row_height = CGFloat()
        if CardName.sharedInstance.selected_serviceamount.count >= 3
        {
            let screensize = UIScreen.main.bounds
            let width = screensize.width
            if width == 320
            {
                row_height = 79
            }else
            {
                row_height = 108
            }
            
        }else{
            row_height = CGFloat(CardName.sharedInstance.selected_serviceamount.count * 27) + 15
        }
        service_tableview.frame = CGRect(x: service_tableview.frame.origin.x, y: service_tableview.frame.origin.y, width: service_tableview.frame.size.width, height: row_height )
        service_tableview.reloadData()
        service_view.frame = CGRect(x: service_view.frame.origin.x, y: service_view.frame.origin.y, width: service_view.frame.width, height: row_height + 50)
        if CardName.sharedInstance.selected_serviceamount.count >= 3
        {
            total_view.frame.origin.y = total_view.frame.origin.y + 50
        }
        for i in 0..<CardName.sharedInstance.type_array.count
        {
            switch CardName.sharedInstance.type_array[i] {
            case "SubTotal":
                print("sub total came")
                self.sub_view.frame = CGRect(x: sub_view.frame.origin.x, y: sub_view.frame.origin.y, width: sub_view.frame.width, height: sub_total_view.frame.height + 60)
                self.line_view.frame = CGRect(x: line_view.frame.origin.x, y: sub_view.frame.origin.y + sub_view.frame.height + 10, width: line_view.frame.width, height: line_view.frame.height)
                self.totalamount_view.frame = CGRect(x: totalamount_view.frame.origin.x, y: line_view.frame.origin.y + 10, width: totalamount_view.frame.width, height: totalamount_view.frame.height)
                self.total_view.frame = CGRect(x: self.total_view.frame.origin.x, y: self.total_view.frame.origin.y, width: self.total_view.frame.width, height: 100)
            case "GST" :
                
                if CardName.sharedInstance.price_Array[i] != "0.00"
                {
                    print("GST came")
                    
                    
                    self.total_view.frame = CGRect(x: self.total_view.frame.origin.x, y: self.total_view.frame.origin.y, width: self.total_view.frame.width, height: 150)
                }
                else
                {
                    print("GST else part came")
                    self.sub_view.frame = CGRect(x: sub_view.frame.origin.x, y: sub_view.frame.origin.y, width: sub_view.frame.width, height: sub_total_view.frame.height + 90)
                    self.line_view.frame = CGRect(x: line_view.frame.origin.x  , y: sub_view.frame.origin.y + sub_view.frame.height - 40  , width: line_view.frame.width, height: line_view.frame.height)
                    self.totalamount_view.frame = CGRect(x: totalamount_view.frame.origin.x, y: line_view.frame.origin.y + 10, width: totalamount_view.frame.width, height: totalamount_view.frame.height + 20)
                    self.total_view.frame = CGRect(x: self.total_view.frame.origin.x, y: self.total_view.frame.origin.y, width: self.total_view.frame.width, height: 100)
                }
                break
            case "Loyalty":
                print("Loyalty came")
                self.loyality_view.isHidden = false
                self.sub_view.frame = CGRect(x: sub_view.frame.origin.x, y: sub_view.frame.origin.y, width: sub_view.frame.width, height: sub_total_view.frame.height + 60)
                self.loyality_view.frame = CGRect(x: loyality_view.frame.origin.x, y:sub_view.frame.origin.y +  18, width: loyality_view.frame.width, height: loyality_view.frame.height)
                self.line_view.frame = CGRect(x: line_view.frame.origin.x, y: loyality_view.frame.origin.y + loyality_view.frame.height + 10, width: line_view.frame.width, height: line_view.frame.height)
                self.totalamount_view.frame = CGRect(x: totalamount_view.frame.origin.x, y: line_view.frame.origin.y + 10, width: totalamount_view.frame.width, height: totalamount_view.frame.height)
                self.total_view.frame = CGRect(x: self.total_view.frame.origin.x, y: self.total_view.frame.origin.y, width: self.total_view.frame.width, height: 100)
                
                //self.total_view.frame = CGRect(x: self.total_view.frame.origin.x, y: self.total_view.frame.origin.y, width: self.total_view.frame.width, height: 300)
                
                break
            case "Offer":
                //self.offer_view.isHidden = false
                print("Offer Part Came")
                self.sub_view.frame = CGRect(x: sub_view.frame.origin.x, y: sub_view.frame.origin.y, width: sub_view.frame.width, height: sub_total_view.frame.height + 60)
                self.offer_view.frame = CGRect(x: offer_view.frame.origin.x, y: sub_total_view.frame.origin.y + sub_total_view.frame.height + 5, width: offer_view.frame.width, height: offer_view.frame.height)
                self.get_view.frame = CGRect(x: get_view.frame.origin.x, y: offer_view.frame.origin.y + offer_view.frame.height + 5, width: get_view.frame.width, height: get_view.frame.height)
                self.line_view.frame = CGRect(x: line_view.frame.origin.x, y: sub_view.frame.origin.y + sub_view.frame.height + 10, width: line_view.frame.width, height: line_view.frame.height)
                self.totalamount_view.frame = CGRect(x: totalamount_view.frame.origin.x, y: line_view.frame.origin.y + 10, width: totalamount_view.frame.width, height: totalamount_view.frame.height)
                self.total_view.frame = CGRect(x: self.total_view.frame.origin.x, y: self.total_view.frame.origin.y, width: self.total_view.frame.width, height: 100)
                
                
                
            default:
                break
            }
        }
        
    }
    
    
    @IBOutlet weak var service_view: UIView!
    @IBOutlet weak var service_tableview: UITableView!
    @IBOutlet weak var salon_view: UIView!
    var salondetails_id = ""
    @objc func tapFunction(sender:UITapGestureRecognizer) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let subclass = storyboard.instantiateViewController(withIdentifier: "SalonDetails_ViewController") as! UITabBarController
        subclass.selectedIndex = 1
        self.present(subclass,animated: true,completion: nil)
    }
    @IBOutlet weak var btn_storeout: UIButton!
    func paystore()
    {
        let id = UserDefaults.standard.string(forKey: "clicked_id")
        let url = "\(CardName.sharedInstance.payment_url)?saloonDetailsID=\(id!)&totalPrice=450&isPayOnline=\(false)"
        print("info_url\(url)")
        let type = UserDefaults.standard.string(forKey: "token")!
        let token = UserDefaults.standard.string(forKey: "Authorizent_token")!
        
        
        let header = [
            "Authorization": "\(type) \(token)",
            
        ]
        print("header\(header)")
        Alamofire.request(url, method: .get,encoding: JSONEncoding.default,headers : header)
            .responseJSON { response in
                print("Details_responce\(response)")
                if response.result.value != nil{
                    
                    let responce_value = response.result.value as! NSDictionary
                    let id = responce_value.value(forKey: "ID") as! Int
                    CardName.sharedInstance.Paymentreference_id = String(id)
                    self.salonDetailsId = String(id)
                    let status = responce_value.value(forKey: "Status") as! Bool
                    if status{
                        self.ordersave()
                    }
                    else
                    {
                        let alert = UIAlertController(title: "Message", message: "Network error! Try again later", preferredStyle: .alert)
                        let okaction = alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                        self.present(alert,animated: true,completion: nil)
                    }
                    
                }
        }
    }
    @IBAction func btn_paystore(_ sender: Any) {
        if UserDefaults.standard.bool(forKey: "isLogged") != nil
        {
            var usertype = UserDefaults.standard.bool(forKey: "isLogged")
            if usertype
            {
                
                paystore()
            }
            else
            {
                CardName.sharedInstance.comming_from = "Checkout"
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let subclass = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! UITabBarController
                self.present(subclass,animated: true,completion: nil)
            }
            
        }
    }
    @IBAction func paynow(_ sendere: Any)
    {
        let alert = UIAlertController(title: "Coming Soon", message: "", preferredStyle: .alert)
        let okaction = alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
        self.present(alert,animated: true,completion: nil)
        
        //        if UserDefaults.standard.bool(forKey: "isLogged") != nil
        //        {
        //            var usertype = UserDefaults.standard.bool(forKey: "isLogged")
        //            if usertype
        //            {
        //                CardName.sharedInstance.isonlinepayment = true
        //                CardName.sharedInstance.payment_type = 1
        //                print("registered user")
        //                beginOnlinePayment()
        //
        //            }else{
        //                let storyboard = UIStoryboard(name: "Main", bundle: nil)
        //                let subclass = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! UITabBarController
        //                self.present(subclass,animated: true,completion: nil)
        //            }
        //        }
    }
    func setView(view: UIView, hidden: Bool) {
        UIView.transition(with: view, duration: 0.5, options: .transitionFlipFromTop, animations: {
            view.isHidden = hidden
        })
    }
    
    
    func beginOnlinePayment()
    {
        let id = UserDefaults.standard.string(forKey: "clicked_id")!
        let url = "\(CardName.sharedInstance.onlinepayment_url)?saloonDetailsID=\(id)&totalPrice=\(CardName.sharedInstance.total_amount)&isPayOnline=\(CardName.sharedInstance.isonlinepayment)&paymentProviderID=\(CardName.sharedInstance.payment_type)"
        print("info_url\(url)")
        let type = UserDefaults.standard.string(forKey: "token")!
        let token = UserDefaults.standard.string(forKey: "Authorizent_token")!
        //let salon_id = Int(id ?? "0")
        print("salon_id: \(id)")
        
        let header = [
            "Authorization": "\(type) \(token)",
        ]
        print("header\(header)")
        
        Alamofire.request(url, method: .get,encoding: JSONEncoding.default,headers : header)
            .responseJSON { response in
                print("Details_responce online payment\(response)")
                
                let responce_value = response.result.value as! NSDictionary
                let transactionId = responce_value.value(forKey: "ID")
                // self.salonDetailsId = String(id)
                let status = responce_value.value(forKey: "Status") as! Bool
                if status{
                    print("beginonlinepayment id : \(transactionId)")
                    self.callInstamojo(transactionID: transactionId as! Int , accesstoken: token)
                    
                    
                }
                //
        }
    }
    
    
    func addNotificationToRecievePaymentCompletion(){
        NotificationCenter.default.addObserver(self, selector: #selector(self.paymentCompletionCallBack), name: NSNotification.Name("INSTAMOJO"), object: nil)
    }
    
    @objc func paymentCompletionCallBack() {
        
        if UserDefaults.standard.value(forKey: "USER-CANCELLED") != nil {
            print("Back Button Pressed")
            //Transaction cancelled by user, back button was pressed
        }
        if UserDefaults.standard.value(forKey: "ON-REDIRECT-URL") != nil
        {
            
            print("on redirect")
            
            // Check the payment status with the transaction_id
        }
        if UserDefaults.standard.value(forKey: "USER-CANCELLED-ON-VERIFY") != nil
        {
            //Transaction cancelled by user when trying to verify UPI payment
        }
    }
    
    
    func showAlert(errorMessage: String) {
        let alert = UIAlertController(title: "", message: errorMessage, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    //    func fetchOrderFromInstamojo(transactionID: String , accesstoken: String){
    //        //spinner.show()
    //        let id = String(transactionID)
    //        let request = Request.init(orderID: id, accessToken: accesstoken, orderRequestCallBack: self)
    //        request.execute()
    //    }
    
    func Savepayment()
    {
        let url = CardName.sharedInstance.savepayment_Url
        print("info_url\(url)")
        var type = UserDefaults.standard.string(forKey: "token")!
        var token = UserDefaults.standard.string(forKey: "Authorizent_token")!
        let header = [
            "Authorization": "\(type) \(token)",
        ]
        print("token\(header)")
        
        let id = UserDefaults.standard.string(forKey: "clicked_id")
        let parameters: [String: Any] = [
            "IsOnlinePaymentRequest" : false,
            "SaloonTransactionDetailsId" : CardName.sharedInstance.SalonTransaction_id ,
            "PaymentReferenceId" : CardName.sharedInstance.Paymentreference_id,
            "BookingStatusID" : "5001",
            "SaloonDetailsID" : id!,
            "BMHSOfferDetailsID" : "0",
            "SaloonServiceOfferID" : "0",
            "PriceDetails" : CardName.sharedInstance.price_dictionary ,
            "OffersUniqueIDs" : "OFFER14164636511118484277143",
            "PaymentGateway" : ""
            
            
        ]
        print("payment params:\(parameters)")
        Alamofire.request(url, method: .post,parameters: parameters,encoding: JSONEncoding.default,headers : header)
            .responseJSON { response in
                print(" savepayment response:::\(response)")
                
                
        }
        
        
    }
    func ordersave()
    {
        
        if UserDefaults.standard.bool(forKey: "isLogged") != nil
        {
            var usertype = UserDefaults.standard.bool(forKey: "isLogged")
            if usertype
            {
                
                let url = CardName.sharedInstance.order_save_url
                print("info_url\(url)")
                var type = UserDefaults.standard.string(forKey: "token")!
                var token = UserDefaults.standard.string(forKey: "Authorizent_token")!
                let header = [
                    "Authorization": "\(type) \(token)",
                ]
                print("token\(header)")
                let responce_value = CardName.sharedInstance.salon_service as! NSDictionary
                let result_array = responce_value.value(forKey: "result") as! NSArray
                for i in 0..<result_array.count
                {
                    let responce = result_array[i] as! NSDictionary
                    let clicked_id = responce.value(forKey: "SaloonServiceID") as! Int
                    let cliked_amount = responce.value(forKey: "ServiceSubCategoryPrice") as! String
                    self.equipments.append(["SaloonServiceID" : clicked_id,"ServiceSubCategoryPrice" : cliked_amount])
                    print("responce_value\(self.equipments)")
                }
                let id = UserDefaults.standard.string(forKey: "clicked_id")
                let booking_date = UserDefaults.standard.string(forKey: "service_bookingdate")
                let parameters: [String: Any] = [
                    "BookingDate" : booking_date!,
                    "BookingStatus" : "3",
                    "OrderEndTime" : CardName.sharedInstance.endslot_id,
                    "OrderStartTime" : CardName.sharedInstance.startslot_id,
                    "SaloonDetailsID" : id ,
                    "SaloonUserServiceTransactions" : self.equipments
                ]
                print("result\(parameters)")
                Alamofire.request(url, method: .post,parameters: parameters,encoding: JSONEncoding.default,headers : header)
                    .responseJSON { response in
                        print("Order save Details_response\(response)")
                        
                        let responce_value = response.result.value as! NSDictionary
                        if responce_value.value(forKey: "OrderRefNumber") != nil
                        {
                            if responce_value.value(forKey: "OrderRefNumber") is NSNull
                            {
                                let alert = UIAlertController(title: "Message", message: "Transaction failed, Try Again", preferredStyle: .alert)
                                let okaction = alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                                self.present(alert,animated: true,completion: nil)
                                
                                //CardName.sharedInstance.order_ref = ""
                            }
                            else{
                                CardName.sharedInstance.order_num = responce_value.value(forKey: "OrderRefNumber") as! String
                                let ord_ref = responce_value.value(forKey: "OrderRefNumber") as! String
                                CardName.sharedInstance.order_ref = ord_ref
                                let salontransaction_id = responce_value.value(forKey: "SaloonTransactionID") as! Int
                                CardName.sharedInstance.SalonTransaction_id = String(salontransaction_id)
                                self.Savepayment()
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                let subclass = storyboard.instantiateViewController(withIdentifier: "Success_ViewController") as! Success_ViewController
                                self.present(subclass,animated: true,completion: nil)
                                
                                
                            }
                            
                        }
                        
                        
                        }
            }
            else
            {
                CardName.sharedInstance.comming_from = "Checkout"
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let subclass = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! UITabBarController
                self.present(subclass,animated: true,completion: nil)
            }
        }
        /*
         // MARK: - Navigation
         
         // In a storyboard-based application, you will often want to do a little preparation before navigation
         override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
         // Get the new view controller using segue.destination.
         // Pass the selected object to the new view controller.
         }         */
        
    }
}

