//
//  Facility_CollectionViewCell.swift
//  BMHS
//
//  Created by TechBT on 25/10/18.
//  Copyright © 2018 TechBT. All rights reserved.
//

import UIKit

class Facility_CollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var facilty_lbl: UILabel!
    override func awakeFromNib() {
        
        self.facilty_lbl.layer.borderWidth = 0.5
        self.facilty_lbl.layer.borderColor = UIColor.lightGray.cgColor
        self.facilty_lbl.layer.shadowColor = UIColor.black.cgColor
        self.facilty_lbl.layer.shadowOpacity = 0.1
    }
 
   
}
