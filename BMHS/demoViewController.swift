//
//  demoViewController.swift
//  BMHS
//
//  Created by TechBT on 26/10/18.
//  Copyright © 2018 TechBT. All rights reserved.
//

import UIKit
import Alamofire
import GoogleMaps
import GooglePlaces

class demoViewController: UIViewController,CLLocationManagerDelegate {
 let dataArray = ["AAA", "BBB", "CCC", "DDD", "EEE", "FFF", "GGG"]
    var locationManager = CLLocationManager()
    lazy var geocoder = CLGeocoder()
    func determineMyCurrentLocation()
    {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
            //locationManager.startUpdatingHeading()
        }
    }
    
    
    var clicked_type = "Area"
    var estimateWidth = 0.0
    var cellMarginSize = 6.0
    var cityname_array = [String]()
    var areaName_Dic = [NSArray]()
    var city_idArray = [Int]()
    var clicked_cityname = [String]()
    var clicked_cityid = [Int]()
    var clicked_citysaloncount = [Int]()
    var maincity_array = [String]()
    var maincity_idArray = [String]()
    var Categorization_Array = [String]()
    var SaloonCount_Array = [Int]()
    @IBOutlet weak var btn_cityout: UIButton!
    @IBOutlet weak var back_img: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        let tap = UITapGestureRecognizer(target: self, action: #selector(demoViewController.tapfunction))
        back_img.isUserInteractionEnabled = true
        back_img.addGestureRecognizer(tap)
        getlocation()
        let screenSize = UIScreen.main.bounds
        let screenWidth = screenSize.width
        estimateWidth = Double(screenWidth / 2)
        self.collectionview.delegate = self
        self.collectionview.dataSource = self
        self.setupGridView()
      //  PlaygroundPage.current.needsIndefiniteExecution = true
       
               btn_cityout.setTitle(UserDefaults.standard.string(forKey: "city_name"), for: .normal)
        
     
        
        // Do any additional setup after loading the view.
    }
  
    
    @IBAction func btn_currentlocation(_ sender: Any) {
       // CardName.sharedInstance.iscurrentLocation = true
        
        UserDefaults.standard.set(true, forKey: "iscurrentlocation")
     determineMyCurrentLocation()
        
//        if CLLocationManager.locationServicesEnabled() {
//            switch CLLocationManager.authorizationStatus() {
//            case .notDetermined, .restricted, .denied:
//                print("No access")
//                locationManager.requestWhenInUseAuthorization()
//            case .authorizedAlways, .authorizedWhenInUse:
//                print("Access")
//                self.locationManager.delegate = self
//              //   self.locationManager.requestAlwaysAuthorization()
//               // self.locationManager.requestWhenInUseAuthorization()
//                self.locationManager.startUpdatingLocation()
//
//              //  self.mapview?.isMyLocationEnabled = true
//
//            }
//        } else {
//            print("Location services are not enabled")
//        }
    }
    
    @objc func  tapfunction (sender:UITapGestureRecognizer) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let subclass = storyboard.instantiateViewController(withIdentifier: "DashboardViewController") as! UITabBarController
        subclass.selectedIndex = 1
        self.present(subclass,animated: true,completion: nil)
    }
    
    @IBAction func btn_backtocity(_ sender: Any)
    {
        
        self.clicked_type = "City"
        getcityname()
    }
    
    @IBAction func btn_selectlocation(_ sender: Any)
    {
        self.clicked_type = "City"
        getcityname()
    }

    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation:CLLocation = locations[0] as CLLocation
        
        // Call stopUpdatingLocation() to stop listening for location updates,
        // other wise this function will be called every time when user location changes.
        
        // manager.stopUpdatingLocation()
        
        print("user latitude = \(userLocation.coordinate.latitude)")
        print("user longitude = \(userLocation.coordinate.longitude)")
        UserDefaults.standard.set(userLocation.coordinate.longitude, forKey: "current_longitute")
        UserDefaults.standard.set(userLocation.coordinate.latitude, forKey: "current_latitute")
       getAddressFromLatLon(pdblLatitude: UserDefaults.standard.string(forKey: "current_latitute")!, withLongitude: UserDefaults.standard.string(forKey: "current_longitute")!)
     
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let subclass = storyboard.instantiateViewController(withIdentifier: "DashboardViewController") as! UITabBarController
        subclass.selectedIndex = 1
        
        self.present(subclass,animated: true,completion: nil)
        
    }

  
  
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
    {
        print("Error \(error)")
    }
    override func viewDidLayoutSubviews() {
        self.setupGridView()
        DispatchQueue.main.async {
         //   self.collectionview.reloadData()
        }
    }
    func getAddressFromLatLon(pdblLatitude: String, withLongitude pdblLongitude: String) {
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let lat: Double = Double("\(pdblLatitude)")!
        //21.228124
        let lon: Double = Double("\(pdblLongitude)")!
        //72.833770
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon
        
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        
        
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                let pm = placemarks! as [CLPlacemark]
                
                if pm.count > 0 {
                    let pm = placemarks![0]
                    print(pm.country)
                    print(pm.locality)
                    print(pm.subLocality)
                    print(pm.thoroughfare)
                    print(pm.postalCode)
                    print(pm.subThoroughfare)
                    var addressString : String = ""
                    if pm.subLocality != nil {
                        UserDefaults.standard.set(pm.subLocality, forKey: "clicked_city")
                        addressString = addressString + pm.subLocality! + ", "
                    }
                    if pm.thoroughfare != nil {
                        addressString = addressString + pm.thoroughfare! + ", "
                    }
                    if pm.locality != nil {
                        addressString = addressString + pm.locality! + ", "
                    }
                    if pm.country != nil {
                        addressString = addressString + pm.country! + ", "
                    }
                    if pm.postalCode != nil {
                        addressString = addressString + pm.postalCode! + " "
                    }
                    
                    
                    print(addressString)
                }
        })
        
        locationManager.stopUpdatingLocation()
    }

    @IBOutlet weak var collectionview: UICollectionView!
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    func setupGridView() {
        let flow = collectionview?.collectionViewLayout as! UICollectionViewFlowLayout
        flow.minimumInteritemSpacing = CGFloat(self.cellMarginSize)
        flow.minimumLineSpacing = CGFloat(self.cellMarginSize)
    }
    
func getlocation()
{
    self.areaName_Dic.removeAll()
    self.cityname_array.removeAll()
    self.city_idArray.removeAll()
    self.clicked_cityid.removeAll()
    self.clicked_cityname.removeAll()
    self.clicked_citysaloncount.removeAll()
    let url = CardName.sharedInstance.preferred_location
    print(url)
    let type = UserDefaults.standard.string(forKey: "token")!
    let token = UserDefaults.standard.string(forKey: "Authorizent_token")!
    
    
    let header = [
        "Authorization": "\(type) \(token)",
    ]
    Alamofire.request(url, method: .post,encoding: JSONEncoding.default,headers : header)
        .responseJSON { response in
            
            let responce_array = response.result.value as! NSArray
            for i in 0..<responce_array.count
            {
                let responce_value = responce_array[i] as! NSDictionary
                let id = responce_value.value(forKey: "ID") as! Int
                self.city_idArray.append(id)
                let name = responce_value.value(forKey: "Name") as! String
                self.cityname_array.append(name)
               
                let area = responce_value.value(forKey: "Area") as! NSArray
                self.areaName_Dic.append(area)
                
            }
            for i in 0..<self.areaName_Dic.count{
                let name = UserDefaults.standard.string(forKey: "city_name")
               
                if self.cityname_array[i] == name{
                        let response_cityvalue = self.areaName_Dic[i] as! NSArray
                    print("clicked_city\(name)")
                    print("clicked_city\(response_cityvalue)")
                    for j in 0..<response_cityvalue.count
                    {
                        let responce_value = response_cityvalue[j] as! NSDictionary
                       
                        let clicked_id = responce_value.value(forKey: "ID") as! Int
                        self.clicked_cityid.append(clicked_id)
                        let clicked_cityname = responce_value.value(forKey: "Name") as! String
                        self.clicked_cityname.append(clicked_cityname)
                        let clicked_saloncount = responce_value.value(forKey: "SalonCount") as! Int
                        self.clicked_citysaloncount.append(clicked_saloncount)
                    }
                }
            
            }
           
            self.collectionview.reloadData()
    }
}
func getcityname()
{
    self.clicked_cityid.removeAll()
    self.clicked_cityname.removeAll()
    self.clicked_citysaloncount.removeAll()
    let url = CardName.sharedInstance.LocationListing_Url
    let type = UserDefaults.standard.string(forKey: "token")!
    let token = UserDefaults.standard.string(forKey: "Authorizent_token")!
    
    
    let header = [
        "Authorization": "\(type) \(token)",
    ]
    Alamofire.request(url, method: .get,headers : header)
        .responseJSON { response in
            if response.error == nil{
            let responseArray = response.result.value as! NSArray
          print("responce\(responseArray)")
            for i in 0..<responseArray.count
            {
                let responseValue = responseArray[i] as! NSDictionary
                print(responseValue.value(forKey: "City")!)
                let cityname = responseValue.value(forKey: "City") as! String
                print("place2\(cityname)")
                self.clicked_cityname.append(cityname)
                let cityid = responseValue.value(forKey: "CityID") as! Int
                self.clicked_cityid.append(cityid)
                let salon_count = responseValue.value(forKey: "SaloonCount") as! Int
                self.clicked_citysaloncount.append(salon_count)
            }
            self.collectionview.reloadData()
    }
            else{
                let alert = UIAlertController(title: "Message", message: "Network Error ! Try Again Later", preferredStyle: .alert)
                let okaction = alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
                self.present(alert,animated: true,completion: nil)
            }
    }
   
    }
}
extension demoViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.clicked_cityname.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "demoCollectionViewCell", for: indexPath) as! demoCollectionViewCell
        
        
        let fullString = String( self.clicked_cityname[indexPath.row])
        
        // create our NSTextAttachment
        let image1Attachment:NSTextAttachment = NSTextAttachment()
        image1Attachment.image = UIImage(named: "nearby.png")
        image1Attachment.bounds = CGRect(x: 5, y: -2, width: 13, height: 13)
        
        // wrap the attachment in its own attributed string so we can append it
        let image1String:NSAttributedString = NSAttributedString(attachment: image1Attachment)
        let myString:NSMutableAttributedString = NSMutableAttributedString(attributedString: image1String)
        
        let atr_rating:NSMutableAttributedString = NSMutableAttributedString(string: "   " +  fullString)
        
        // add the NSTextAttachment wrapper to our full string, then add some more text.
        myString.append(atr_rating)
        
        // draw the result in a label
        // yourLabel.attributedText = fullString
        cell.demo_lbl.attributedText = myString
        
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       // CardName.sharedInstance.iscurrentLocation = false
        UserDefaults.standard.set(false, forKey: "iscurrentlocation")
        if clicked_type == "Area"
        {
            UserDefaults.standard.set(self.clicked_cityname[indexPath.row], forKey: "clicked_city")
            UserDefaults.standard.set(self.clicked_cityid[indexPath.row], forKey: "city_id")
            print("Local")
            savepreferencelocation(cityname: clicked_cityname[indexPath.row])
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//            let subclass = storyboard.instantiateViewController(withIdentifier: "DashboardViewController") as! UITabBarController
//            subclass.selectedIndex = 1
//            self.present(subclass,animated: true,completion: nil)
        }else if clicked_type == "City"{
            if clicked_cityname.count != 0 && clicked_cityname.count != nil
            {
            let cityname = self.clicked_cityname[indexPath.row] as! String
            
            print("cityname\(cityname)")
            UserDefaults.standard.set(cityname, forKey: "clicked_city")
           // UserDefaults.standard.set(cityname, forKey: "city_name")
            print("cityname\(UserDefaults.standard.string(forKey: "clicked_city"))")
          //sr   UserDefaults.standard.set(self.clicked_cityname[indexPath.row], forKey: "city_name")
              savepreferencelocation(cityname: clicked_cityname[indexPath.row])
             UserDefaults.standard.set(self.clicked_cityid[indexPath.row], forKey: "city_id")
            getlocation()
              btn_cityout.setTitle(cityname, for: .normal)
            clicked_type = "Area"
            }
        }
    }
    func savepreferencelocation(cityname : String)
    {
        let url = CardName.sharedInstance.savepreferenceLocation
        var type = UserDefaults.standard.string(forKey: "token")!
        var token = UserDefaults.standard.string(forKey: "Authorizent_token")!
        let header = [
            "Authorization": "\(type) \(token)",
        ]
        var Namecity = ""
        var city = ""
        print("headers\(header)")
        if clicked_type == "Area"
        {
             Namecity = cityname
             city = UserDefaults.standard.string(forKey: "city_name")!
        }else{
        Namecity = UserDefaults.standard.string(forKey: "city_name")!
            city = UserDefaults.standard.string(forKey: "city_name")!
        }
        let parameters: [String: Any] = [
            "AddressCity": city, "AddressPlace": Namecity
        ]
        print("responce123\(parameters)")
        Alamofire.request(url, method: .post,parameters: parameters, encoding: JSONEncoding.default,headers : header)
            .responseJSON { response in
                print("responce123\(response)")
                if response.error == nil
                {
                    let result_value = response.result.value as! NSDictionary
                    let status = result_value.value(forKey: "Status") as! Bool
                    if status {
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let subclass = storyboard.instantiateViewController(withIdentifier: "DashboardViewController") as! UITabBarController
                        subclass.selectedIndex = 1
                        self.present(subclass,animated: true,completion: nil)
                    }else{
                        let alert = UIAlertController(title: "Alert", message: "Network Error! ,Try Again?", preferredStyle: .alert)
                        let subclass = alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
                        self.present(alert,animated: true,completion: nil)
                    }
                }else{
                    let alert = UIAlertController(title: "Alert", message: "Network Error! ,Try Again?", preferredStyle: .alert)
                    let subclass = alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
                    self.present(alert,animated: true,completion: nil)
                }
        }
    }
}

extension demoViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = self.calculateWith()
        return CGSize(width: width, height: 35)
    }
    
    
    func calculateWith() -> CGFloat {
        let estimatedWidth = CGFloat(estimateWidth)
        let cellCount = floor(CGFloat(self.view.frame.size.width / estimatedWidth))
        
        let margin = CGFloat(cellMarginSize * 2)
        let width = (self.view.frame.size.width - CGFloat(cellMarginSize) * (cellCount - 1) - margin) / cellCount
        print("width\(width)")
        
        return width
    }
}
