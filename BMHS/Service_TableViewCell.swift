//
//  Service_TableViewCell.swift
//  BMHS
//
//  Created by TechBT on 06/09/18.
//  Copyright © 2018 TechBT. All rights reserved.
//

import UIKit
import GMStepper
class Service_TableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    @IBOutlet weak var service_name: UILabel!
    @IBOutlet weak var service_amt: UILabel!
    @IBOutlet weak var stepper: GMStepper!
    
    @IBOutlet weak var value_lbl: UILabel!
    @IBOutlet weak var stepper_iOS: UIStepper!
    override func prepareForReuse() {
        super.prepareForReuse()
        
       
        
    }
   
    @IBAction func steppervalue(_ sender: UIStepper) {
        print("value\(sender.value)")
    }
    
    @IBOutlet weak var service_countLbl: UILabel!
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
