//
//  Filters_ViewController.swift
//  BMHS
//
//  Created by TechBT on 25/10/18.
//  Copyright © 2018 TechBT. All rights reserved.
//
import UIKit
import Alamofire
var clicked = 0
class Filters_ViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource {
    var clicked_array = [String]()
    let mycolor = #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1)
    var male_lbl_clicked = true
    var female_lbl_clicked = true
    var unisex_lbl_clicked = true
    var oneplus_lbl_clicked = true
    var twoplus_lbl_clicked = true
    var threeplus_lbl_clicked = true
    var fourplus_lbl_clicked = true
    var all_lbl_clicked = true
    var verified_lbl_clicked = true
    var rating_lbl_clicked = true
    var distance_lbl_clicked = true
    var alphbetical_lbl_clicked = true
    
  
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.facilty_array.count
    }
    
    @IBOutlet weak var backbutton: UIBarButtonItem!
    
    @IBOutlet weak var back_image: UIImageView!
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Facility_CollectionViewCell", for: indexPath) as! Facility_CollectionViewCell
        if CardName.sharedInstance.clicked_facility.count != 0
        {
            for i in 0..<CardName.sharedInstance.clicked_facility.count
            {
                if self.facilty_array[indexPath.row] == CardName.sharedInstance.clicked_facility[i]
                {
                    
                    cell.layer.backgroundColor = UIColor(red: 76/255.0, green: 175/255.0, blue: 80/255.0, alpha: 1.0).cgColor
                }
            }
        }
        cell.facilty_lbl.text = self.facilty_array[indexPath.row]
        //        cell.facilty_lbl.textColor = UIColor.darkText
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        print("clicked")
        
    }
    func collectionView(_ collectionView: UICollectionView, didHighlightItemAt indexPath: IndexPath) {
        let cell = facility_collectionview.cellForItem(at: indexPath)
        print("clicked_higlight")
        print("clicked_position\(indexPath.row)")
        
        if cell?.layer.backgroundColor == UIColor(red: 76/255.0, green: 175/255.0, blue: 80/255.0, alpha: 1.0).cgColor
        {
            
            cell?.layer.backgroundColor = UIColor.white.cgColor
            
            
            for i in 0..<facilty_array.count
            {
                for j in 0..<CardName.sharedInstance.clicked_facility.count{
                    print("\(CardName.sharedInstance.clicked_facility)")
                    print("print\(facilty_array)")
                    if facilty_array[i] == CardName.sharedInstance.clicked_facility[j]
                    {
                        CardName.sharedInstance.clicked_facility.remove(at: j)
                        return
                            print("Removed")
                    }
                }
                // if CardName.sharedInstance.clicked_facility.count != 0 && CardName.sharedInstance.clicked_facility.count != nil && facilty_array.count != 0 && facilty_array.count != nil
                //                {
                //
                //               if facilty_array[indexPath.row] == CardName.sharedInstance.clicked_facility[i]
                //               {
                //
                //
                //                }
                //                }
                
            }
        }
            
        else
        {
            cell?.layer.backgroundColor = UIColor(red: 76/255.0, green: 175/255.0, blue: 80/255.0, alpha: 1.0).cgColor
            let cell2 = collectionView.dequeueReusableCell(withReuseIdentifier: "Facility_CollectionViewCell", for: indexPath) as! Facility_CollectionViewCell
           cell2.facilty_lbl.layer.borderColor = UIColor(red: 76/255.0, green: 175/255.0, blue: 80/255.0, alpha: 1.0).cgColor
        
            CardName.sharedInstance.clicked_facility.append(facilty_array[indexPath.row])
            print("Else part")
        }
        
        print("clicked array\(CardName.sharedInstance.clicked_facility)")
        print("facilty array\(facilty_array)")
    }
    
    //    func collectionView(_ collectionView: UICollectionView, didUnhighlightItemAt indexPath: IndexPath) {
    //        let cell = facility_collectionview.cellForItem(at: indexPath)
    //        cell?.backgroundColor = UIColor.white
    //    }
    
    var facilty_array = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        //backButton.contentMode = .scaleAspectFit
        
        let tapfun = UITapGestureRecognizer(target: self, action: #selector(Filters_ViewController.tapfunction))
        back_image.isUserInteractionEnabled = true
        back_image.addGestureRecognizer(tapfun)
        self.male_lbl.layer.borderWidth = 0.5
        self.male_lbl.layer.borderColor = UIColor.lightGray.cgColor
        self.male_lbl.layer.shadowColor = UIColor.black.cgColor
        self.male_lbl.layer.shadowOpacity = 0.1
        self.male_lbl.highlightedTextColor = UIColor.blue
        self.female_lbl.layer.borderWidth = 0.5
        self.female_lbl.layer.borderColor = UIColor.lightGray.cgColor
        self.female_lbl.layer.shadowColor = UIColor.black.cgColor
        self.female_lbl.layer.shadowOpacity = 0.1
        self.unisex_lbl.layer.borderWidth = 0.5
        self.unisex_lbl.layer.borderColor = UIColor.lightGray.cgColor
        self.unisex_lbl.layer.shadowColor = UIColor.black.cgColor
        self.unisex_lbl.layer.shadowOpacity = 0.1
        self.oneplus_lbl.layer.borderWidth = 0.5
        self.oneplus_lbl.layer.borderColor = UIColor.lightGray.cgColor
        self.oneplus_lbl.layer.shadowColor = UIColor.black.cgColor
        self.oneplus_lbl.layer.shadowOpacity = 0.1
        self.twoplus_lbl.layer.borderWidth = 0.5
        self.twoplus_lbl.layer.borderColor = UIColor.lightGray.cgColor
        self.twoplus_lbl.layer.shadowColor = UIColor.black.cgColor
        self.twoplus_lbl.layer.shadowOpacity = 0.1
        self.threeplus_lbl.layer.borderWidth = 0.5
        self.threeplus_lbl.layer.borderColor = UIColor.lightGray.cgColor
        self.threeplus_lbl.layer.shadowColor = UIColor.black.cgColor
        self.threeplus_lbl.layer.shadowOpacity = 0.1
        self.fourplus_lbl.layer.borderWidth = 0.5
        self.fourplus_lbl.layer.borderColor = UIColor.lightGray.cgColor
        self.fourplus_lbl.layer.shadowColor = UIColor.black.cgColor
        self.fourplus_lbl.layer.shadowOpacity = 0.1
        self.all_lbl.layer.borderWidth = 0.5
        self.all_lbl.layer.borderColor = UIColor.lightGray.cgColor
        self.all_lbl.layer.shadowColor = UIColor.black.cgColor
        self.all_lbl.layer.shadowOpacity = 0.1
        self.verified_lbl.layer.borderWidth = 0.5
        self.verified_lbl.layer.borderColor = UIColor.lightGray.cgColor
        self.verified_lbl.layer.shadowColor = UIColor.black.cgColor
        self.verified_lbl.layer.shadowOpacity = 0.1
        self.alphbetical_lbl.layer.borderWidth = 0.5
        self.alphbetical_lbl.layer.borderColor = UIColor.lightGray.cgColor
        self.alphbetical_lbl.layer.shadowColor = UIColor.black.cgColor
        self.alphbetical_lbl.layer.shadowOpacity = 0.1
        self.distance_lbl.layer.borderWidth = 0.5
        self.distance_lbl.layer.borderColor = UIColor.lightGray.cgColor
        self.distance_lbl.layer.shadowColor = UIColor.black.cgColor
        self.distance_lbl.layer.shadowOpacity = 0.1
        self.rating_lbl.layer.borderWidth = 0.5
        self.rating_lbl.layer.borderColor = UIColor.lightGray.cgColor
        self.rating_lbl.layer.shadowColor = UIColor.black.cgColor
        self.rating_lbl.layer.shadowOpacity = 0.1
        let tap = UITapGestureRecognizer(target: self, action: #selector(Filters_ViewController.tapFunction))
        male_lbl.isUserInteractionEnabled = true
        male_lbl.addGestureRecognizer(tap)
        let femaletab = UITapGestureRecognizer(target: self, action: #selector(Filters_ViewController.femalefunction))
        female_lbl.isUserInteractionEnabled = true
        female_lbl.addGestureRecognizer(femaletab)
        let unisextab = UITapGestureRecognizer(target: self, action: #selector(Filters_ViewController.unisexfunction))
        unisex_lbl.isUserInteractionEnabled = true
        unisex_lbl.addGestureRecognizer(unisextab)
        let oneplustab = UITapGestureRecognizer(target: self, action: #selector(Filters_ViewController.oneplusfunction))
        oneplus_lbl.isUserInteractionEnabled = true
        oneplus_lbl.addGestureRecognizer(oneplustab)
        let twoplustab = UITapGestureRecognizer(target: self, action: #selector(Filters_ViewController.twoplusfunction))
        twoplus_lbl.isUserInteractionEnabled = true
        twoplus_lbl.addGestureRecognizer(twoplustab)
        let threeplustab = UITapGestureRecognizer(target: self, action: #selector(Filters_ViewController.threeplusfunction))
        threeplus_lbl.isUserInteractionEnabled = true
        threeplus_lbl.addGestureRecognizer(threeplustab)
        let fourplustab = UITapGestureRecognizer(target: self, action: #selector(Filters_ViewController.fourplusfunction))
        fourplus_lbl.isUserInteractionEnabled = true
        fourplus_lbl.addGestureRecognizer(fourplustab)
        let alltab = UITapGestureRecognizer(target: self, action: #selector(Filters_ViewController.allfunction))
        all_lbl.isUserInteractionEnabled = true
        all_lbl.addGestureRecognizer(alltab)
        let verifiedtab = UITapGestureRecognizer(target: self, action: #selector(Filters_ViewController.verifiedfunction))
        verified_lbl.isUserInteractionEnabled = true
        verified_lbl.addGestureRecognizer(verifiedtab)
        
        let alphbeticaltab = UITapGestureRecognizer(target: self, action: #selector(Filters_ViewController.alphbeticalfunction))
        alphbetical_lbl.isUserInteractionEnabled = true
        alphbetical_lbl.addGestureRecognizer(alphbeticaltab)
        let distancetab = UITapGestureRecognizer(target: self, action: #selector(Filters_ViewController.distancefunction))
        distance_lbl.isUserInteractionEnabled = true
        distance_lbl.addGestureRecognizer(distancetab)
        let ratingtab = UITapGestureRecognizer(target: self, action: #selector(Filters_ViewController.ratingfunction))
        rating_lbl.isUserInteractionEnabled = true
        rating_lbl.addGestureRecognizer(ratingtab)
        getfacility()
        if CardName.sharedInstance.clicked_genderArray.count != 0
        {
            for i in 0..<CardName.sharedInstance.clicked_genderArray.count
            {
                if CardName.sharedInstance.clicked_genderArray[i] == 1
                {
                    self.male_lbl.backgroundColor = UIColor(red: 76/255.0, green: 175/255.0, blue: 80/255.0, alpha: 1.0)
                   
                    //self.male_lbl.textColor = UIColor.darkText
                    //self.male_lbl_clicked = false
                    male_lbl_clicked = false
                }else if CardName.sharedInstance.clicked_genderArray[i] == 2
                {
                    self.female_lbl.backgroundColor = UIColor(red: 76/255.0, green: 175/255.0, blue: 80/255.0, alpha: 1.0)
                   
                    //self.female_lbl_clicked = false
                    female_lbl_clicked = false
                }else if CardName.sharedInstance.clicked_genderArray[i] == 3
                {
                    self.unisex_lbl.backgroundColor = UIColor(red: 76/255.0, green: 175/255.0, blue: 80/255.0, alpha: 1.0)
                    
                    //self.unisex_lbl_clicked = false
                    unisex_lbl_clicked = false
                }
            }
        }
        if CardName.sharedInstance.clicked_sort != ""
        {
            if CardName.sharedInstance.clicked_sort == "alpha"
            {
                CardName.sharedInstance.alphabeticalselected =  true
                self.alphbetical_lbl.backgroundColor = UIColor(red: 76/255.0, green: 175/255.0, blue: 80/255.0, alpha: 1.0)
                self.distance_lbl.backgroundColor = UIColor.white
                self.rating_lbl.backgroundColor = UIColor.white
                self.alphbetical_lbl_clicked = false
                self.distance_lbl_clicked = true
                self.rating_lbl_clicked = true
            }
            else if CardName.sharedInstance.clicked_sort == "distance"
            {
                self.alphbetical_lbl.backgroundColor = UIColor.white
                self.distance_lbl.backgroundColor = UIColor(red: 76/255.0, green: 175/255.0, blue: 80/255.0, alpha: 1.0)
                self.rating_lbl.backgroundColor = UIColor.white
                self.alphbetical_lbl_clicked = true
                self.distance_lbl_clicked = false
                self.rating_lbl_clicked = true
            }
            else if CardName.sharedInstance.clicked_sort == "rating"
            {
                self.alphbetical_lbl.backgroundColor = UIColor.white
                self.distance_lbl.backgroundColor = UIColor.white
                self.rating_lbl.backgroundColor = UIColor(red: 76/255.0, green: 175/255.0, blue: 80/255.0, alpha: 1.0)
                self.alphbetical_lbl_clicked = true
                self.distance_lbl_clicked = true
                self.rating_lbl_clicked = false
                
            }
        }
        if CardName.sharedInstance.clicked_rating != ""
        {
            if CardName.sharedInstance.clicked_rating == "1"
            {
                
                
                self.oneplus_lbl.backgroundColor = UIColor(red: 76/255.0, green: 175/255.0, blue: 80/255.0, alpha: 1.0)
                //self.oneplus_lbl.textColor = UIColor.blue
                
                self.twoplus_lbl.backgroundColor = UIColor.white
                self.threeplus_lbl.backgroundColor = UIColor.white
                self.fourplus_lbl.backgroundColor = UIColor.white
                self.all_lbl.backgroundColor = UIColor.white
                self.oneplus_lbl_clicked = false
                self.twoplus_lbl_clicked = true
                self.threeplus_lbl_clicked = true
                self.fourplus_lbl_clicked = true
                self.all_lbl_clicked = true
            }
            else if CardName.sharedInstance.clicked_rating == "2"
            {
                self.oneplus_lbl.backgroundColor = UIColor.white
                self.twoplus_lbl.backgroundColor = UIColor(red: 76/255.0, green: 175/255.0, blue: 80/255.0, alpha: 1.0)
                self.threeplus_lbl.backgroundColor = UIColor.white
                self.fourplus_lbl.backgroundColor = UIColor.white
                self.all_lbl.backgroundColor = UIColor.white
                self.oneplus_lbl_clicked = true
                self.twoplus_lbl_clicked = false
                self.threeplus_lbl_clicked = true
                self.fourplus_lbl_clicked = true
                self.all_lbl_clicked = true
            }
            else if CardName.sharedInstance.clicked_rating == "3"
            {
                self.threeplus_lbl.backgroundColor = UIColor(red: 76/255.0, green: 175/255.0, blue: 80/255.0, alpha: 1.0)
                self.oneplus_lbl.backgroundColor = UIColor.white
                self.twoplus_lbl.backgroundColor = UIColor.white
                self.fourplus_lbl.backgroundColor = UIColor.white
                self.all_lbl.backgroundColor = UIColor.white
                self.oneplus_lbl_clicked = true
                self.twoplus_lbl_clicked = true
                self.threeplus_lbl_clicked = false
                self.fourplus_lbl_clicked = true
                self.all_lbl_clicked = true
            }
            else if CardName.sharedInstance.clicked_rating == "4"
            {
                self.threeplus_lbl.backgroundColor = UIColor.white
                self.oneplus_lbl.backgroundColor = UIColor.white
                self.twoplus_lbl.backgroundColor = UIColor.white
                self.fourplus_lbl.backgroundColor = UIColor(red: 76/255.0, green: 175/255.0, blue: 80/255.0, alpha: 1.0)
                self.all_lbl.backgroundColor = UIColor.white
                self.oneplus_lbl_clicked = true
                self.twoplus_lbl_clicked = true
                self.threeplus_lbl_clicked = true
                self.fourplus_lbl_clicked = false
                self.all_lbl_clicked = true
            }
            else if CardName.sharedInstance.clicked_rating == "All"
            {
                self.threeplus_lbl.backgroundColor = UIColor.white
                self.oneplus_lbl.backgroundColor = UIColor.white
                self.twoplus_lbl.backgroundColor = UIColor.white
                self.fourplus_lbl.backgroundColor = UIColor.white
                self.all_lbl.backgroundColor = UIColor(red: 76/255.0, green: 175/255.0, blue: 80/255.0, alpha: 1.0)
                self.oneplus_lbl_clicked = true
                self.twoplus_lbl_clicked = true
                self.threeplus_lbl_clicked = true
                self.fourplus_lbl_clicked = true
                self.all_lbl_clicked = false
            }
        }
        if CardName.sharedInstance.isverified == "true"
        {
            self.verified_lbl.backgroundColor = UIColor(red: 76/255.0, green: 175/255.0, blue: 80/255.0, alpha: 1.0)
            self.verified_lbl_clicked = false
        }
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btn_apply(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let subclass = storyboard.instantiateViewController(withIdentifier: "DashboardViewController") as! UITabBarController
        subclass.selectedIndex = 1
        self.present(subclass,animated: true,completion: nil)
    }
    
    @IBOutlet weak var male_lbl: UILabel!
    @IBOutlet weak var unisex_lbl: UILabel!
    @IBOutlet weak var female_lbl: UILabel!
    @IBOutlet weak var oneplus_lbl: UILabel!
    @IBOutlet weak var twoplus_lbl: UILabel!
    @IBOutlet weak var threeplus_lbl: UILabel!
    @IBOutlet weak var fourplus_lbl: UILabel!
    @IBOutlet weak var all_lbl: UILabel!
    
    @IBOutlet weak var verified_lbl: UILabel!
    
    
    @IBOutlet weak var rating_lbl: UILabel!
    @IBOutlet weak var distance_lbl: UILabel!
    @IBOutlet weak var alphbetical_lbl: UILabel!
    @IBOutlet weak var facility_collectionview: UICollectionView!
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    @objc func tapFunction(sender:UITapGestureRecognizer) {
        if male_lbl_clicked == true
        {
            print("tap function male label clicked")
            self.male_lbl.backgroundColor = UIColor(red: 76/255.0, green: 175/255.0, blue: 80/255.0, alpha: 1.0)
            
           
            //self.male_lbl_clicked = false
            male_lbl_clicked = false
            CardName.sharedInstance.clicked_genderArray.append(1)
        }else{
            print("unclicked")
            self.male_lbl.backgroundColor = UIColor.white
            
            //self.male_lbl_clicked = true
            male_lbl_clicked = true
            for i in 0..<CardName.sharedInstance.clicked_genderArray.count
            {
                if CardName.sharedInstance.clicked_genderArray[i] == 1
                {
                    CardName.sharedInstance.clicked_genderArray.remove(at: i)
                    return
                }
            }
        }
    }
    @objc func femalefunction(sender:UITapGestureRecognizer) {
        if female_lbl_clicked == true
        {
            self.female_lbl.backgroundColor = UIColor(red: 76/255.0, green: 175/255.0, blue: 80/255.0, alpha: 1.0)
           
           // self.female_lbl_clicked = false
            female_lbl_clicked = false
            CardName.sharedInstance.clicked_genderArray.append(2)
        }else{
            self.female_lbl.backgroundColor = UIColor.white
            //self.female_lbl_clicked = true
            female_lbl_clicked = true
            for i in 0..<CardName.sharedInstance.clicked_genderArray.count
            {
                if CardName.sharedInstance.clicked_genderArray[i] == 2
                {
                    CardName.sharedInstance.clicked_genderArray.remove(at: i)
                    return
                }
            }
        }
    }
    @objc func unisexfunction(sender:UITapGestureRecognizer)
    {
        if unisex_lbl_clicked == true
        {
            self.unisex_lbl.backgroundColor = UIColor(red: 76/255.0, green: 175/255.0, blue: 80/255.0, alpha: 1.0)
           
            //self.unisex_lbl_clicked = false
            unisex_lbl_clicked = false
            CardName.sharedInstance.clicked_genderArray.append(3)
        }else{
            self.unisex_lbl.backgroundColor = UIColor.white
            //self.unisex_lbl_clicked = true
            unisex_lbl_clicked = true
            for i in 0..<CardName.sharedInstance.clicked_genderArray.count
            {
                if CardName.sharedInstance.clicked_genderArray[i] == 3
                {
                    CardName.sharedInstance.clicked_genderArray.remove(at: i)
                    return
                }
            }
        }
    }
    @objc func oneplusfunction(sender:UITapGestureRecognizer)
    {
        if oneplus_lbl_clicked == true
        {
            self.oneplus_lbl.backgroundColor = UIColor(red: 76/255.0, green: 175/255.0, blue: 80/255.0, alpha: 1.0)
            //self.oneplus_lbl.textColor = UIColor.white
            oneplus_lbl.highlightedTextColor =  UIColor.white
            
            self.twoplus_lbl.backgroundColor = UIColor.white
            self.threeplus_lbl.backgroundColor = UIColor.white
            self.fourplus_lbl.backgroundColor = UIColor.white
            self.all_lbl.backgroundColor = UIColor.white
            self.oneplus_lbl_clicked = false
            
            self.twoplus_lbl_clicked = true
            self.threeplus_lbl_clicked = true
            self.fourplus_lbl_clicked = true
            self.all_lbl_clicked = true
            CardName.sharedInstance.clicked_rating = "1"
        }else{
            self.oneplus_lbl.backgroundColor = UIColor.white
            // self.oneplus_lbl.textColor = UIColor.red
            self.oneplus_lbl_clicked = true
            
            CardName.sharedInstance.clicked_rating = ""
        }
    }
    @objc func twoplusfunction(sender:UITapGestureRecognizer)
    {
        if twoplus_lbl_clicked == true
        {
            self.oneplus_lbl.backgroundColor = UIColor.white
            self.twoplus_lbl.backgroundColor = UIColor(red: 76/255.0, green: 175/255.0, blue: 80/255.0, alpha: 1.0)
            self.threeplus_lbl.backgroundColor = UIColor.white
            self.fourplus_lbl.backgroundColor = UIColor.white
            self.all_lbl.backgroundColor = UIColor.white
            self.oneplus_lbl_clicked = true
            self.twoplus_lbl_clicked = false
            self.threeplus_lbl_clicked = true
            self.fourplus_lbl_clicked = true
            self.all_lbl_clicked = true
            CardName.sharedInstance.clicked_rating = "2"
        }else{
            self.twoplus_lbl.backgroundColor = UIColor.white
            self.twoplus_lbl_clicked = true
            CardName.sharedInstance.clicked_rating = ""
        }
    }
    @objc func threeplusfunction(sender:UITapGestureRecognizer)
    {
        if threeplus_lbl_clicked == true
        {
            self.threeplus_lbl.backgroundColor = UIColor(red: 76/255.0, green: 175/255.0, blue: 80/255.0, alpha: 1.0)
            self.oneplus_lbl.backgroundColor = UIColor.white
            self.twoplus_lbl.backgroundColor = UIColor.white
            self.fourplus_lbl.backgroundColor = UIColor.white
            self.all_lbl.backgroundColor = UIColor.white
            self.oneplus_lbl_clicked = true
            self.twoplus_lbl_clicked = true
            self.threeplus_lbl_clicked = false
            self.fourplus_lbl_clicked = true
            self.all_lbl_clicked = true
            CardName.sharedInstance.clicked_rating = "3"
        }else{
            self.threeplus_lbl.backgroundColor = UIColor.white
            self.threeplus_lbl_clicked = true
            CardName.sharedInstance.clicked_rating = ""
        }
    }
    @objc func fourplusfunction(sender:UITapGestureRecognizer)
    {
        if fourplus_lbl_clicked == true
        {
            self.threeplus_lbl.backgroundColor = UIColor.white
            self.oneplus_lbl.backgroundColor = UIColor.white
            self.twoplus_lbl.backgroundColor = UIColor.white
            self.fourplus_lbl.backgroundColor = UIColor(red: 76/255.0, green: 175/255.0, blue: 80/255.0, alpha: 1.0)
            self.all_lbl.backgroundColor = UIColor.white
            self.oneplus_lbl_clicked = true
            self.twoplus_lbl_clicked = true
            self.threeplus_lbl_clicked = true
            self.fourplus_lbl_clicked = false
            self.all_lbl_clicked = true
            CardName.sharedInstance.clicked_rating = "4"
        }else{
            self.fourplus_lbl.backgroundColor = UIColor.white
            self.fourplus_lbl_clicked = true
            CardName.sharedInstance.clicked_rating = ""
        }
    }
    @objc func allfunction(sender:UITapGestureRecognizer) {
        if all_lbl_clicked == true
        {
            self.threeplus_lbl.backgroundColor = UIColor.white
            self.oneplus_lbl.backgroundColor = UIColor.white
            self.twoplus_lbl.backgroundColor = UIColor.white
            self.fourplus_lbl.backgroundColor = UIColor.white
            self.all_lbl.backgroundColor = UIColor(red: 76/255.0, green: 175/255.0, blue: 80/255.0, alpha: 1.0)
            self.oneplus_lbl_clicked = true
            self.twoplus_lbl_clicked = true
            self.threeplus_lbl_clicked = true
            self.fourplus_lbl_clicked = true
            self.all_lbl_clicked = false
            CardName.sharedInstance.clicked_rating = "All"
        }else{
            self.all_lbl.backgroundColor = UIColor.white
            self.all_lbl_clicked = true
            CardName.sharedInstance.clicked_rating = ""
        }
    }
    @objc func verifiedfunction(sender:UITapGestureRecognizer) {
        if verified_lbl_clicked == true
        {
            self.verified_lbl.backgroundColor = UIColor(red: 76/255.0, green: 175/255.0, blue: 80/255.0, alpha: 1.0)
            self.verified_lbl_clicked = false
            print("clicked_rating \(CardName.sharedInstance.clicked_rating)")
            print("clicked_gender \(CardName.sharedInstance.clicked_genderArray)")
            CardName.sharedInstance.isverified = "true"
        }else{
            self.verified_lbl.backgroundColor = UIColor.white
            self.verified_lbl_clicked = true
            CardName.sharedInstance.isverified = "false"
        }
    }
    @objc func alphbeticalfunction(sender:UITapGestureRecognizer) {
        if alphbetical_lbl_clicked == true
        {
            
            CardName.sharedInstance.alphabeticalselected =  true
            self.alphbetical_lbl.backgroundColor = UIColor(red: 76/255.0, green: 175/255.0, blue: 80/255.0, alpha: 1.0)
            self.distance_lbl.backgroundColor = UIColor.white
            self.rating_lbl.backgroundColor = UIColor.white
            self.alphbetical_lbl_clicked = false
            self.distance_lbl_clicked = true
            self.rating_lbl_clicked = true
            CardName.sharedInstance.clicked_sort = "alpha"
            print("clicked_rating \(CardName.sharedInstance.clicked_rating)")
            print("clicked_gender \(CardName.sharedInstance.clicked_genderArray)")
        }else{
            self.alphbetical_lbl.backgroundColor = UIColor.white
            self.alphbetical_lbl_clicked = true
            CardName.sharedInstance.clicked_sort = ""
        }
    }
    @objc func distancefunction(sender:UITapGestureRecognizer) {
        if distance_lbl_clicked == true
        {
            self.alphbetical_lbl.backgroundColor = UIColor.white
            self.distance_lbl.backgroundColor = UIColor(red: 76/255.0, green: 175/255.0, blue: 80/255.0, alpha: 1.0)
            self.rating_lbl.backgroundColor = UIColor.white
            self.alphbetical_lbl_clicked = true
            self.distance_lbl_clicked = false
            self.rating_lbl_clicked = true
            CardName.sharedInstance.clicked_sort = "distance"
        }else{
            self.distance_lbl.backgroundColor = UIColor.white
            self.distance_lbl_clicked = true
            CardName.sharedInstance.clicked_sort = ""
        }
    }
    @objc func ratingfunction(sender:UITapGestureRecognizer) {
        if rating_lbl_clicked == true
        {
            self.alphbetical_lbl.backgroundColor = UIColor.white
            self.distance_lbl.backgroundColor = UIColor.white
            self.rating_lbl.backgroundColor = UIColor(red: 76/255.0, green: 175/255.0, blue: 80/255.0, alpha: 1.0)
            self.alphbetical_lbl_clicked = true
            self.distance_lbl_clicked = true
            self.rating_lbl_clicked = false
            CardName.sharedInstance.clicked_sort = "rating"
        }else{
            self.rating_lbl.backgroundColor = UIColor.white
            self.rating_lbl_clicked = true
            CardName.sharedInstance.clicked_sort = ""
        }
    }
    func getfacility()
    {
        let url = CardName.sharedInstance.facility_url
        let type = UserDefaults.standard.string(forKey: "token")!
        let token = UserDefaults.standard.string(forKey: "Authorizent_token")!
        
        
        let header = [
            "Authorization": "\(type) \(token)",
        ]
        Alamofire.request(url, method: .post,encoding: JSONEncoding.default,headers : header)
            .responseJSON { response in
                if response.error == nil
                {
                    let responce_value = response.result.value as! NSDictionary
                    let facilityresult_Array = responce_value.value(forKey: "Facilities") as! NSArray
                    for i in 0..<facilityresult_Array.count
                    {
                        self.facilty_array.append(facilityresult_Array[i] as! String)
                    }
                    self.facility_collectionview.reloadData()
                    print("facility_array\(self.facilty_array)")
                }
        }
    }
    
    
    
    @IBAction func reset_btn(_ sender: Any)
    {
        clearDetails()
        CardName.sharedInstance.clicked_sort = "distance"
        CardName.sharedInstance.isverified = "true"
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let subclass = storyboard.instantiateViewController(withIdentifier: "DashboardViewController") as! UITabBarController
        subclass.selectedIndex = 1
        self.present(subclass,animated: true,completion: nil)
        
    }
    
    
    func clearDetails()
    {
        self.male_lbl.backgroundColor = UIColor.white
        self.male_lbl_clicked = true
        self.female_lbl.backgroundColor = UIColor.white
        self.male_lbl_clicked = true
        self.unisex_lbl.backgroundColor = UIColor.white
        self.male_lbl_clicked = true
        CardName.sharedInstance.clicked_genderArray.removeAll()
        self.oneplus_lbl.backgroundColor = UIColor.white
        self.oneplus_lbl_clicked = true
        self.twoplus_lbl.backgroundColor = UIColor.white
        self.twoplus_lbl_clicked = true
        self.threeplus_lbl.backgroundColor = UIColor.white
        self.threeplus_lbl_clicked = true
        self.fourplus_lbl.backgroundColor = UIColor.white
        self.fourplus_lbl_clicked = true
        self.all_lbl.backgroundColor = UIColor.white
        self.all_lbl_clicked = true
        CardName.sharedInstance.clicked_rating = ""
        CardName.sharedInstance.clicked_sort = ""
        self.verified_lbl.backgroundColor = UIColor.white
        self.verified_lbl_clicked = true
        CardName.sharedInstance.isverified = "false"
        self.alphbetical_lbl.backgroundColor = UIColor.white
        self.alphbetical_lbl_clicked = true
        self.distance_lbl.backgroundColor = UIColor.white
        self.distance_lbl_clicked = true
        self.rating_lbl.backgroundColor = UIColor.white
        self.rating_lbl_clicked = true
        CardName.sharedInstance.clicked_facility.removeAll()
    }
    
    @objc func tapfunction(sender: UIGestureRecognizer)
    {
        clearDetails()
        CardName.sharedInstance.clicked_sort = "distance"
        CardName.sharedInstance.isverified = "true"
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let subclass = storyboard.instantiateViewController(withIdentifier: "DashboardViewController") as! UITabBarController
        subclass.selectedIndex = 1
        self.present(subclass,animated: true,completion: nil)
    }
    
    
    
}
