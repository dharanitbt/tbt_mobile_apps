//
//  ActiveBookings_ViewController.swift
//  BMHS
//
//  Created by TechBT on 24/09/18.
//  Copyright © 2018 TechBT. All rights reserved.
//

import UIKit
import Alamofire

class ActiveBookings_ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var activity_indicator: UIActivityIndicatorView!
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("Came number of sections")
       
        return saloonname_Array.count
    }
    @IBOutlet weak var back_img: UIView!
    var servicesaloon_name = [String]()
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomCellOne") as! ActiveBookingsTableViewCell
        print("bookingstatus\(bookingstatus_Array)")
        cell.saloonName_Label.text = self.saloonname_Array[indexPath.row] as! String
        print("date\(UTCToLocal(date: self.createddate_Array[indexPath.row]))")
        let fullName    = UTCToLocal(date: self.createddate_Array[indexPath.row])
        let fullNameArr = fullName.components(separatedBy: ",")
        
        let day_calender    = fullNameArr[0]
        let date_calender = fullNameArr[1]
        cell.Daylabel.text = day_calender
        let fullNameArr1 = date_calender.components(separatedBy: " ")
        //print("date\(date_calender)")
        cell.month_label.text = fullNameArr1[1]
        cell.date_label.text = fullNameArr1[2]
        print("date\(fullNameArr1[1])")
        if self.bookingstatus_Array[indexPath.row] == "Confirmed"
        {
            cell.status_label.backgroundColor = UIColor(red: 156/255, green: 39/255, blue: 176/255, alpha: 1.0)
             cell.status_label.text = self.bookingstatus_Array[indexPath.row]
        }else if self.bookingstatus_Array[indexPath.row] == "Booked"
        {
            
              cell.status_label.backgroundColor = UIColor(red: 76/255, green: 175/255, blue: 80/255, alpha: 1.0)
             cell.status_label.text = "waiting for confirmation"
        }
        else{
            cell.status_label.backgroundColor = UIColor(red: 156/255, green: 39/255, blue: 176/255, alpha: 1.0)
             cell.status_label.text = self.bookingstatus_Array[indexPath.row]
        }
       
        cell.time_label.text = self.starttimeslot_Array[indexPath.row] as! String
        self.servicesaloon_name.removeAll()
        print("Array\(saloonservice_NSArray.count)")
        for i in 0..<self.saloonservice_NSArray.count
        {
            let saloonservice_Arrayvalue = self.saloonservice_NSArray[i] as! NSArray
             print("Array\(saloonservice_NSArray.count)")
            for j in 0..<saloonservice_Arrayvalue.count
            {
               let saloonservice_dic = saloonservice_Arrayvalue[j] as! NSDictionary
            let saloonservice_value = saloonservice_dic.value(forKey: "SaloonServiceName") as! String
                print("array\(saloonservice_value)")
           self.servicesaloon_name.append(saloonservice_value)
            }
        }
        print("arraynew\(servicesaloon_name)")
         cell.saloonservice_Label.text = "\(servicesaloon_name[indexPath.row])."
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 94
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        CardName.sharedInstance.booking_dictionary = CardName.sharedInstance.responce_array[indexPath.row]
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let subclass = storyboard.instantiateViewController(withIdentifier: "OrderDetailsViewController") as! OrderDetailsViewController
        self.present(subclass,animated: true,completion: nil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        activity_indicator.startAnimating()
        let tap = UITapGestureRecognizer(target: self, action: #selector(ActiveBookings_ViewController.tapFunction))
        back_img.isUserInteractionEnabled = true
        
        back_img.addGestureRecognizer(tap)
        
   tableview.register(UINib(nibName: "ActiveBookingsTableViewCell", bundle: nil), forCellReuseIdentifier: "CustomCellOne")
        getActiveBooking()
        
       
        // Do any additional setup after loading the view.
    }
    var createddate_Array = [String]()
    var starttimeslot_Array = [String]()
    var bookingstatus_Array = [String]()
    var saloonname_Array = [String]()
    var saloonservice_NSArray = [NSArray]()
    var servicename_Array = [String]()
    
    @IBOutlet weak var tableview: UITableView!
    func getActiveBooking()
    {
        createddate_Array.removeAll()
        starttimeslot_Array.removeAll()
        bookingstatus_Array.removeAll()
        saloonname_Array.removeAll()
        saloonservice_NSArray.removeAll()
        servicename_Array.removeAll()
        let clicked_id = UserDefaults.standard.string(forKey: "clicked_id")
        print("clicked_id\(clicked_id)")
        print("Dictionary print: \(CardName.sharedInstance.booking_dictionary)")
        let url = CardName.sharedInstance.Active_Booking_Url
        print("info_url\(url)")
        let type = UserDefaults.standard.string(forKey: "token")!
        let token = UserDefaults.standard.string(forKey: "Authorizent_token")!
        let header = [
            "Authorization": "\(type) \(token)",
        ]
        print("header\(header)")
        Alamofire.request(url, method: .post,encoding: JSONEncoding.default,headers : header)
            .responseJSON { response in
                print("booking response\(response)")
                self.activity_indicator.stopAnimating()
                if response.error == nil{
                    let responce_Array = response.result.value as! NSArray
                    if responce_Array.count != 0{
                        self.nobookings_view.isHidden = true
                        self.tableview.isHidden = false
                    }
                    else{
                        self.tableview.isHidden = true
                        self.nobookings_view.isHidden = false
                        self.nobooking_lbl.text = "No bookings available. Make a booking right away!!"
                    }
                    
                    for i in (0..<responce_Array.count).reversed()
                    {
                        let responce_value = responce_Array[i] as! NSDictionary
                        CardName.sharedInstance.responce_array.append(responce_value)
                        let created_date = responce_value.value(forKey: "BookingDate") as! String
                        self.createddate_Array.append(created_date)
                        let starttimeslot = responce_value.value(forKey: "StartTimeSlot") as! String
                        self.starttimeslot_Array.append(starttimeslot)
                        
                        if responce_value.value(forKey: "BookingStatus") is NSNull
                        {
                            self.bookingstatus_Array.append("")
                        }
                        else
                        {
                            let booking_status = responce_value.value(forKey: "BookingStatus") as! String
                            self.bookingstatus_Array.append(booking_status)
                        }
                        
                        let saloonName = responce_value.value(forKey: "SaloonName") as!  String
                        self.saloonname_Array.append(saloonName)
                        var servicee_responce = responce_value.value(forKey: "SaloonService") as! NSArray
                        //                    for i in 0..<saloonservice_Array.count
                        //                    {
                        //                        let service_value = saloonservice_Array[i] as! NSDictionary
                        //                        let service_name = service_value.value(forKey: "SaloonServiceName") as! String
                        //                        self.servicename_Array.append(service_name)
                        //
                        //                    }
                        self.saloonservice_NSArray.append(servicee_responce)
                        print("array\(self.saloonservice_NSArray[0])")
                    }
                    self.tableview.reloadData()
                    self.tableview.delegate = self
                    self.tableview.dataSource = self
                    if self.saloonname_Array.count == 0
                    {
                        self.tableview.isHidden = true
                        self.nobookings_view.isHidden = false
                        self.nobooking_lbl.text = "No bookings available. Make a booking right away!!"
                    }
                    else
                    {
                        self.nobookings_view.isHidden = true
                        self.tableview.isHidden = false
                    }
                    
                }
                else{
                    let alert = UIAlertController(title: "Alert", message: "Network Error,Try Again!", preferredStyle: .alert)
                    let okaction = alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
                    self.present(alert,animated: true,completion: nil)
                }
                }
               
        
    }
    func UTCToLocal(date:String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        let dt = dateFormatter.date(from: date)
        print("Date\(dt)")
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "EEE, MMM d"
        if(dt != nil){
            return dateFormatter.string(from: dt!)}
        return ""
    }
    
    
   @objc func tapFunction(sender:UITapGestureRecognizer) {
        
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let subclass = storyboard.instantiateViewController(withIdentifier: "DashboardViewController") as! UITabBarController
        subclass.selectedIndex = 2
        self.present(subclass,animated: true,completion: nil)
        
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBOutlet weak var nobookings_view: UIView!
    
    @IBOutlet weak var nobooking_lbl: UILabel!
}
