//
//  helpTableViewCell.swift
//  BMHS
//
//  Created by TechBT on 03/01/19.
//  Copyright © 2019 TechBT. All rights reserved.
//

import UIKit

class helpTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBOutlet weak var content_lbl: UILabel!
    @IBOutlet weak var heading_lbl: UILabel!
    @IBOutlet weak var imageview: UIImageView!
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
