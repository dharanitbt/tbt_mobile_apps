//
//  Help_ViewController.swift
//  BMHS
//
//  Created by TechBT on 03/01/19.
//  Copyright © 2019 TechBT. All rights reserved.
//

import UIKit
import Alamofire
class Help_ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return helpimage.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! helpTableViewCell
        cell.heading_lbl.text = heading_Array[indexPath.row]
        cell.content_lbl.text = content_Array[indexPath.row]
        cell.imageview.image = helpimage[indexPath.row]
        return cell
    }
    override func viewDidAppear(_ animated: Bool) {
        self.txt_feedback.frame = CGRect(x: 15, y: help_us.frame.origin.y + help_us.frame.height + 5, width: txt_feedback.frame.width
            , height: 40)
        txt_feedback.underlined()
    }
    
    var heading_Array = ["RATING","BROWSE","VERIFIED","CHECK INS","MEMBERSHIP","BOOK","CALL BACK","ONLINE PAYMENT"]
    var content_Array = ["Fool Proof peer rating","Most optimised listing","Authenticated salon and spa","Bookings through BMHS","Enjoy discounts on all salons","book to confirm bookings","A click away from call","pay through online"]
    @IBOutlet weak var help_us: UILabel!
    
    var helpimage = [UIImage]()
    override func viewDidLoad() {
        super.viewDidLoad()
        txt_feedback.delegate = self
helpimage.append(UIImage(named: "help5.png")!)
        helpimage.append(UIImage(named: "help1.png")!)
        helpimage.append(UIImage(named: "help7.png")!)
        helpimage.append(UIImage(named: "help2.png")!)
        helpimage.append(UIImage(named: "help3.png")!)
        helpimage.append(UIImage(named: "help.png")!)
        helpimage.append(UIImage(named: "help6.png")!)
        helpimage.append(UIImage(named: "help4.png")!)
        // Do any additional setup after loading the view.
    }
    
    @IBOutlet weak var txt_feedback: UITextField!
    
    @IBOutlet weak var feedback_view: UIView!
    @IBAction func btn_feedback(_ sender: Any) {
        if UserDefaults.standard.bool(forKey: "isLogged") != nil
        {
            var usertype = UserDefaults.standard.bool(forKey: "isLogged")
            if usertype
            {
                self.feedback_view.isHidden = false
            }else{
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let subclass = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! UITabBarController
                self.present(subclass,animated: true,completion: nil)
            }
        }
        
    }
    @IBAction func btn_cancel(_ sender: Any) {
        self.feedback_view.isHidden = true
    }
    
    
    @IBAction func btn_send(_ sender: Any) {
        var feedname = txt_feedback.text
        if feedname?.count == 0
        {
            let alert = UIAlertController(title: "Message", message: "Enter Valid Feedback Name", preferredStyle: .alert)
            let okaction = alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
            self.present(alert,animated: true,completion: nil)
        }else
        {
            getSalons(feedback: txt_feedback.text ?? "")
            
        }
    }
    func getSalons(feedback:String)
    {
        
        var str_feedback = feedback
        str_feedback = str_feedback.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) ?? ""
        let url = "\(CardName.sharedInstance.feedback_url)?feedBack=\(str_feedback)"
        print(url)
        var type = UserDefaults.standard.string(forKey: "token")!
        var token = UserDefaults.standard.string(forKey: "Authorizent_token")!
        let header = [
            "Authorization": "\(type) \(token)",
        ]
        print("headers\(header)")
        Alamofire.request(url, method: .get,encoding: JSONEncoding.default,headers : header)
            .responseJSON { response in
                print("response\(response)")
                
                let responce_value = response.result.value as! NSDictionary
                let status = responce_value.value(forKey: "Status") as! Int
                if status == 1
                {
                  //  self.missing_salonpopup.isHidden = true
                    self.feedback_view.isHidden = true
                    self.txt_feedback.text = ""
                   // self.txt_missingsalon.text = ""
                }else{
                    let alert = UIAlertController(title: "Message", message: "Something went wrong! Please Try Again", preferredStyle: .alert)
                    let okaction = alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
                    self.present(alert,animated: true,completion: nil)
                 //   self.missing_salonpopup.isHidden = true
                }
        }
        
        
        
        
        
    }
    @IBAction func btn_back(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let subclass = storyboard.instantiateViewController(withIdentifier: "DashboardViewController") as! UITabBarController
        subclass.selectedIndex = 0
        self.present(subclass,animated: true,completion: nil)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return true
    }
    
    @IBOutlet weak var tableview: UITableView!
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
