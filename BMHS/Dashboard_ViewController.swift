//
//  Dashboard_ViewController.swift
//  BMHS
//
//  Created by TechBT on 11/10/18.
//  Copyright © 2018 TechBT. All rights reserved.
//

import UIKit
import AACarousel
import Alamofire
import Kingfisher
import SDWebImage
import Crashlytics

class Dashboard_ViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UITableViewDelegate,UITableViewDataSource,UICollectionViewDelegateFlowLayout
{
    @IBOutlet weak var notification_img: UIImageView!
    
    var infoVC = Infosalon_ViewController()
    var salonname_Array:[String] = [""]
    var Locationname_Array:[String] = [""]
    var salontype_Array:[String] = [""]
    var salonrating_Array = [Double]()
    var saloncheckins_Array:[Int] = []
    var salonDistance_Array = [Double]()
    var salonLogo_Array:[String] = [""]
    var clicked_id_Array:[Int] = []
    var gender_array = [Int]()
    var clickedsalon_id_Array:[Int] = []
    var salonservice_id_Array:[String] = []
    
    
    
    @IBOutlet weak var Salon_button: UIButton!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var help_action: UIImageView!

    var salon_latArray = [Double]()
    var salon_logArray = [Double]()
    var isVerified = ""
    var facility_array = [String]()
    var count = 0
    var verified_array = [Bool]()
   
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var trending_lbl: UILabel!
    @IBOutlet weak var pageControl_2: UIPageControl!
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return salonname_Array.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 94
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomCellOne", for: indexPath) as! SalonTableViewCell
        if verified_array.count != nil && verified_array.count != 0
        {
        if self.verified_array[indexPath.row]
        {
            let fullString = NSMutableAttributedString(string: self.salonname_Array[indexPath.row])
            
            // create our NSTextAttachment
            let image1Attachment = NSTextAttachment()
            image1Attachment.image = UIImage(named: "tick1.png")
            image1Attachment.bounds = CGRect(x: 5, y: -2, width: 13, height: 13)
            
            // wrap the attachment in its own attributed string so we can append it
            let image1String = NSAttributedString(attachment: image1Attachment)
            
            // add the NSTextAttachment wrapper to our full string, then add some more text.
            fullString.append(image1String)
            
            // draw the result in a label
            // yourLabel.attributedText = fullString
            cell.salon_Name.attributedText = fullString
        }else{
            cell.salon_Name.text = self.salonname_Array[indexPath.row]
        }
        }
        if salonLogo_Array.count != nil && salonLogo_Array.count != 0
        {
        if salonLogo_Array[indexPath.row] != nil && salonLogo_Array[indexPath.row] != ""
        {
            let salon_img = salonLogo_Array[indexPath.row]
            print(salonLogo_Array[indexPath.row] )
            let url = URL(string: salon_img)
            if url != nil
            {
                let data = try? Data(contentsOf: url!)
                
                if  let imageData = data {
                    let image = UIImage(data: imageData)
                    cell.salon_img.image = image
                }
            }
        }
        }
        if Locationname_Array.count != nil && Locationname_Array.count != 0
        {
        cell.salon_Location.text = "\(Locationname_Array[indexPath.row]) | \(salontype_Array[indexPath.row])"
        }
        if salonrating_Array.count != nil && salonrating_Array.count != 0
        {
        var Str_rating = String(salonrating_Array[indexPath.row] )
        
        if Str_rating == "0.0"
        {
            Str_rating = "NEW"
        }
        
        let attachment:NSTextAttachment = NSTextAttachment()
        attachment.image = UIImage(named: "star")
        attachment.bounds = CGRect(x: 0, y: -2, width: 13, height: 13)
        let attachmentString:NSAttributedString = NSAttributedString(attachment: attachment)
        let myString:NSMutableAttributedString = NSMutableAttributedString(attributedString: attachmentString)
        let atr_rating:NSMutableAttributedString = NSMutableAttributedString(string:" " +  Str_rating)
        myString.append(atr_rating)
        
        
        //  self.attributedText = myString
        cell.salon_rating.attributedText = myString
        //var str_checkins = String(saloncheckins_Array[indexPath.row] as! Int)
        let str_checkins = String(saloncheckins_Array[indexPath.row] )
        let checkin_attachment:NSTextAttachment = NSTextAttachment()
        checkin_attachment.image = UIImage(named: "checkins")
        checkin_attachment.bounds = CGRect(x: 0, y: -2, width: 13, height: 13)
        let checkin_attachmentString:NSAttributedString = NSAttributedString(attachment: checkin_attachment)
        let checkin_myString:NSMutableAttributedString = NSMutableAttributedString(attributedString: checkin_attachmentString)
        let checkin_atr_String:NSMutableAttributedString = NSMutableAttributedString(string: " " + str_checkins)
        let atr_text:NSMutableAttributedString = NSMutableAttributedString(string: " Check ins")
        checkin_myString.append(checkin_atr_String)
        checkin_myString.append(atr_text)
        cell.salon_checkins.attributedText = checkin_myString
        }
        if salonDistance_Array.count != nil && salonDistance_Array.count != 0
        {
        let dou_distance:Double = salonDistance_Array[indexPath.row]
        if dou_distance != nil && dou_distance >= 0.0
        {
            let str_distance:String = String(format:"%.1f", dou_distance)
            let checkin_attachment:NSTextAttachment = NSTextAttachment()
            checkin_attachment.image = UIImage(named: "nearby")
            checkin_attachment.bounds = CGRect(x: 0, y: -2, width: 13, height: 13)
            let checkin_attachmentString:NSAttributedString = NSAttributedString(attachment: checkin_attachment)
            let checkin_myString:NSMutableAttributedString = NSMutableAttributedString(attributedString: checkin_attachmentString)
            let checkin_atr_String:NSMutableAttributedString = NSMutableAttributedString(string: " " + str_distance)
            let atr_kmtext:NSMutableAttributedString = NSMutableAttributedString(string: "km")
            checkin_myString.append(checkin_atr_String)
            checkin_myString.append(atr_kmtext)
            cell.salon_distance.attributedText = checkin_myString
        }
        }
        return cell
    }
    
    @IBOutlet weak var citylocation: UIImageView!
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == dashboardcollectionview
        {
            //pathArray.removeAll()
            return pathArray.count


        }
        else if collectionView == exclusivecollectionview
        {
            //exclusive_patharray.removeAll()
            return exclusive_patharray.count
        }
        return 0
        
    }
    
    @IBOutlet weak var scrollview: UIScrollView!
    
    @IBOutlet weak var exclusivecollectionview: UICollectionView!
    @IBOutlet weak var welcome_lbl: UILabel!
    @IBAction func btn_usercity(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let subclass = storyboard.instantiateViewController(withIdentifier: "LocationViewController") as! LocationViewController
        self.present(subclass,animated: true,completion: nil)
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == dashboardcollectionview
        {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "dashboardcell", for: indexPath) as! dashboardcell
            if pathArray.count != 0
            {
                print("Cell for item came")
                print("Path array Count checking : \(pathArray.count)")

                let url = URL(string: pathArray[indexPath.row])
                // this downloads the image asynchronously if it's not cached yet
                print("salon offer URL \(url)")
                cell.imageview.kf.setImage(with: url)
                cell.imageview.contentMode = .scaleToFill
               

            }
            return cell
            
        }
        else if collectionView == exclusivecollectionview
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "exclusiveCollectionViewCell", for: indexPath) as! exclusiveCollectionViewCell
            if exclusive_patharray.count != 0
            {
            print("Exclusive array Count checking : \(exclusive_patharray.count)")
            let url = URL(string: exclusive_patharray[indexPath.row])
            // this downloads the image asynchronously if it's not cached yet
            print("Exclusive offer URL \(url)")
            cell.imageview.kf.setImage(with: url)
            cell.imageview.contentMode = .scaleToFill
            
            let screenSize = UIScreen.main.bounds
            let screenWidth = screenSize.width
            _ = screenSize.height
            
            print("screenwidth\(screenWidth)")
            //cell.frame.width = screenWidth

            //            if screenWidth == 320.0
            //            {
            //                cell.imageview.frame = CGRect(x: 0, y: cell.imageview.frame.origin.y , width:  screenWidth, height:  cell.imageview.frame.height)
            //
            //                //   videoPlayer.view.frame = CGRect(x: 15.0, y: 76.0, width: 289.0, height: 182.0)
            //                // self.tableview.frame = CGRect(x: 0, y: 52.0, width: screenWidth, height: screenheight)
            //            }
            //            else if screenWidth == 375
            //            {
            //                imageview.frame = CGRect(x: exclusivecollectionview.frame.origin.x, y: exclusivecollectionview.frame.origin.y , width:  exclusivecollectionview.frame.width, height:  exclusivecollectionview.frame.height)
            //                print("iphone 6")
            //                //  videoPlayer.view.frame = CGRect(x: 15.0, y: 100, width: 345.0, height: 200.0)
            //
            //            }
            //            else if screenWidth == 414.0
            //            {
            //                imageview.frame = CGRect(x: exclusivecollectionview.frame.origin.x, y: exclusivecollectionview.frame.origin.y , width:  exclusivecollectionview.frame.width, height:  exclusivecollectionview.frame.height)
            //                print("iphone 6s plus")
            //
            //                //   self.tableview.frame = CGRect(x: 0, y: 69.0, width: 414.0, height: screenheight)
            //                //  videoPlayer.view.frame = CGRect(x: 15.0, y: 100.0, width: 384, height: 200.0)
            //
            //            }
            //            else if screenWidth > 415
            //            {
            //                print("unexpested screensize")
            //            }
            //
            }
            return cell
        }
        return UICollectionViewCell()
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let screenSize = UIScreen.main.bounds
        let screenWidth = screenSize.width
        _ = screenSize.height
//        return CGSize(width: screenWidth, height: collectionView.frame.height)
        if collectionView ==  dashboardcollectionview
        
        {
            if pathArray.count == 1
            {
                return CGSize(width: screenWidth, height: collectionView.frame.height)

            }
            else
            {
                  return CGSize(width: screenWidth/2, height: collectionView.frame.height)
            }

        }
        if collectionView == exclusivecollectionview
        {
                return CGSize(width: screenWidth, height: collectionView.frame.height)

        }
            return CGSize(width: screenWidth, height: collectionView.frame.height)

    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
        if collectionView == dashboardcollectionview
        {
            UserDefaults.standard.set(self.clickedsalon_id_Array[indexPath.row], forKey: "clicked_id")
            getSalonInfo()
            
            // UserDefaults.standard.set(self.clicked_name_Array[indexPath.row], forKey: "Salon_Name")
            // UserDefaults.standard.set(self.clicked_logo_Array[indexPath.row], forKey: "Salon_Logo")
            //UserDefaults.standard.set(self.clicked_city_Array[indexPath.row], forKey: "Salon_Location")
            
            //            var int_id = Int(String: clicked_id)
            //infoVC.getDetails(salon_id: Int(self.clickedsalon_id_Array[indexPath.row]))
            
         
            print("You selected dashboard collection view cell #\(indexPath.row)!")
            
        }
        if collectionView == exclusivecollectionview
        {
            print("You selected exclusive collection view cell #\(indexPath.row)!")
            
        }
    }
    
    
    
    var pathArray = [""]
    var exclusive_patharray = [""]
    
    @IBOutlet weak var trendingsalon_lbl: UILabel!
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var topmenu_view: UIView!
    @IBOutlet weak var trndindsalon_view: UIView!
    @IBOutlet weak var mainview : UIView!
    
    @IBOutlet weak var bmhsoffer_lbl: UILabel!
    @IBOutlet weak var salonoffer_lbl: UILabel!
    var titleArray = [String]()
    @IBOutlet weak var imageview: AACarousel!
    override func viewDidLoad() {
        super.viewDidLoad()
//        mobilebanner()
//        getdashboardoffers()
//
//        getuserlocation()
//        getTrendingsalon()
        

        //pageControl?.numberOfPages = pathArray.count
        self.dashboardcollectionview?.isPagingEnabled = true
        //dashboardcollectionview.contentInsetAdjustmentBehavior = .automatic
        startTimer()
        startTimer2()
        //ScrollstartTimer()
        scrollview.bounces = false
        //Salon_button.delegate =  self
        self.Salon_button.layer.cornerRadius = 15
        let mycolor = #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1)
        self.Salon_button.layer.borderColor = mycolor.cgColor
        self.Salon_button.layer.borderWidth = 1
        //self.exclusivecollectionview.decelerationRate = UIScrollViewDecelerationRateFast
        //   scrollview.contentSize = CGSize(width: self.view.frame.size.width, height: 1500)
        print("width\(self.view.frame.size.width)")
        btn_salonLocation?.titleLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping
        var name = ""
        print("string\(UserDefaults.standard.string(forKey: "city_name"))")
        if UserDefaults.standard.string(forKey: "city_name") != nil
        {
            name = UserDefaults.standard.string(forKey: "city_name") as! String
            if name == "Coimbatore"
            {
                self.citylocation.image = UIImage(named:"Maruthamalai.png")
                // UserDefaults.standard.set("Coimbatore", forKey: "city_name")
            }
            else if name == "Chennai"
            {
                self.citylocation.image = UIImage(named: "ic_home_user_info_bg_chennai")
                //UserDefaults.standard.set("Chennai", forKey: "city_name")
            }else if name == "Bangalore"
            {
                self.citylocation.image = UIImage(named: "ic_home_user_info_bg_blore")
                // UserDefaults.standard.set("Bangalore", forKey: "city_name")
            }
        }
        else{
            name = "Coimbatore"
        }
        let fullString = NSMutableAttributedString(string : "TRENDING SALONS")
        
        // create our NSTextAttachment
        let image1Attachment = NSTextAttachment()
        image1Attachment.image = UIImage(named: "TRENDING.png")
        image1Attachment.bounds = CGRect(x: 5, y: -2, width: 13, height: 13)
        
        // wrap the attachment in its own attributed string so we can append it
        let image1String = NSAttributedString(attachment: image1Attachment)
        
        // add the NSTextAttachment wrapper to our full string, then add some more text.
        fullString.append(image1String)
        
        // draw the result in a label
        // yourLabel.attributedText = fullString
        trendingsalon_lbl.attributedText = fullString
        
        let image2Attachment = NSTextAttachment()
        image2Attachment.image = UIImage(named: "downarrow.png")
        image2Attachment.bounds = CGRect(x: 10, y: 0, width: 13, height: 13)
        
        // wrap the attachment in its own attributed string so we can append it
        let image2String = NSAttributedString(attachment: image2Attachment)
        
        // add the NSTextAttachment wrapper to our full string, then add some more text.
        
        let buttonText: NSString = "   Your City\n \(name) " as NSString
        
        //getting the range to separate the button title strings
        let newlineRange: NSRange = buttonText.range(of: "\n")
        
        //getting both substrings
        var substring1: NSString = ""
        var substring2: NSString = ""
        
        if(newlineRange.location != NSNotFound) {
            substring1 = buttonText.substring(to: newlineRange.location) as NSString
            substring2 = buttonText.substring(from: newlineRange.location) as NSString
        }
        
        //assigning diffrent fonts to both substrings
        // let font:UIFont? = UIFont(name: "Arial", size: 8.0)
        let myString = substring1
        let myAttribute = [ NSAttributedStringKey.foregroundColor: UIColor.white ,  NSAttributedStringKey.font: UIFont(name: "Arial", size: 10.0)! ]
        let myAttrString = NSAttributedString(string: myString as String, attributes: myAttribute)
        
        let _:UIFont? = UIFont(name: "Arial", size: 17.0)
        let myString1 = substring2
        let myAttribute1 = [ NSAttributedStringKey.foregroundColor: UIColor.white,  NSAttributedStringKey.font: UIFont(name: "Arial", size: 17.0)!]
        
        let myAttrString1 = NSAttributedString(string: myString1 as String, attributes: myAttribute1)
        
        //appending both attributed strings
        
        let combination = NSMutableAttributedString()
        
        combination.append(myAttrString)
        combination.append(myAttrString1)
        combination.append(image2String)
        
        
        //assigning the resultant attributed strings to the button
        btn_salonLocation?.setAttributedTitle(combination, for: UIControlState.normal)
        tableview.register(UINib(nibName: "SalonTableViewCell", bundle: nil), forCellReuseIdentifier: "CustomCellOne")
        
        let gesture = UITapGestureRecognizer(target: self, action: Selector(("someAction:")))
        //or for swift 2 +
        _ = UITapGestureRecognizer(target: self, action:  #selector (self.someAction (_:)))
        self.user_backgroundimg.addGestureRecognizer(gesture)
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        help_action.isUserInteractionEnabled = true
        help_action.addGestureRecognizer(tapGestureRecognizer)
        let tap = UITapGestureRecognizer(target: self, action: #selector(Dashboard_ViewController.tapFunction))
        notification_img.isUserInteractionEnabled = true
        
       notification_img.addGestureRecognizer(tap)
        
    }
    @objc func tapFunction(sender:UITapGestureRecognizer) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let subclass = storyboard.instantiateViewController(withIdentifier: "Notification_Viewcontroller") as! UIViewController
        self.present(subclass,animated: true,completion: nil)
    }
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        _ = tapGestureRecognizer.view as! UIImageView
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let subclass = storyboard.instantiateViewController(withIdentifier: "Help_ViewController") as! Help_ViewController
        self.present(subclass,animated: true,completion: nil)
        // Your action
    }
    @objc func someAction(_ sender:UITapGestureRecognizer){
        if UserDefaults.standard.bool(forKey: "isLogged")
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let subclass = storyboard.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
            self.present(subclass,animated: true,completion: nil)
        }else{
            CardName.sharedInstance.camefrom = "Dashboard"
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let subclass = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! UITabBarController
            self.present(subclass,animated: true,completion: nil)
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        DispatchQueue.main.async {
            print("Main queue started")
            self.getuserlocation()
            
            self.mobilebanner()
            self.exclusivecollectionview.delegate = self
            self.exclusivecollectionview.dataSource = self
           
            self.getdashboardoffers()
            self.dashboardcollectionview.delegate = self
            self.dashboardcollectionview.dataSource = self

            //getexclusiveoffers()
            //self.getuserlocation()
            self.tableview.delegate = self
            self.tableview.dataSource = self
            self.getTrendingsalon()
            print("Main queue ended")
        }
        let deadlineTime = DispatchTime.now() + .seconds(3)
        DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
            self.user_backgroundimg.backgroundColor = UIColor(patternImage: UIImage(named:"ic_home_user_info_bg")!)
            print("Secondary queue started")
//            self.exclusivecollectionview.delegate = self
//            self.exclusivecollectionview.dataSource = self
//            self.dashboardcollectionview.delegate = self
//            self.dashboardcollectionview.dataSource = self
            print("patharray count\(self.pathArray.count)")
            print("exclusive count\(self.exclusive_patharray.count)")
            // self.dashboardcollectionview.isHidden = true
            
            
            if self.pathArray.count != 0 && self.exclusive_patharray.count != 0
            {
                
                self.dashboardcollectionview.isHidden = false
                self.exclusivecollectionview.isHidden = false
                let screenSize = UIScreen.main.bounds
                let screenWidth = screenSize.width
_ = screenSize.height
                
                print("path and exclusive != 0:screenwidth\(screenWidth)")
                
                if screenWidth == 320.0
                {
                    self.scrollview.contentSize = CGSize(width: self.scrollview.contentSize.width, height: 1200 )
                    self.topmenu_view.frame = CGRect(x: 0, y: -1, width: 320, height: 70)
                    self.user_backgroundimg.frame = CGRect(x: 0, y: 65, width: 320, height: 86)
                    self.dashboardcollectionview.frame = CGRect(x: self.dashboardcollectionview.frame.origin.x, y: self.salonoffer_lbl.frame.origin.y + self.salonoffer_lbl.frame.height + 5 , width: self.dashboardcollectionview.frame.width, height: self.dashboardcollectionview.frame.height)
                    self.pageControl.frame = CGRect(x: self.pageControl.frame.origin.x, y: self.dashboardcollectionview.frame.origin.y + self.dashboardcollectionview.frame.height + 1 , width: self.pageControl.frame.width, height: self.pageControl.frame.height)
                    self.bmhsoffer_lbl.frame = CGRect(x: self.bmhsoffer_lbl.frame.origin.x, y: self.pageControl.frame.origin.y + self.pageControl.frame.height - 6 , width: self.bmhsoffer_lbl.frame.width, height: self.bmhsoffer_lbl.frame.height)
                    self.exclusivecollectionview.frame = CGRect(x: self.exclusivecollectionview.frame.origin.x, y: self.bmhsoffer_lbl.frame.origin.y + self.bmhsoffer_lbl.frame.height + 5, width: self.exclusivecollectionview.frame.width, height: self.exclusivecollectionview.frame.height)
                    self.pageControl_2.frame = CGRect(x: self.pageControl_2.frame.origin.x, y: self.exclusivecollectionview.frame.origin.y + self.exclusivecollectionview.frame.height + 1 , width: self.pageControl_2.frame.width, height: self.pageControl_2.frame.height)
                    
                    self.trendingsalon_lbl.frame = CGRect(x: self.trendingsalon_lbl.frame.origin.x, y: self.pageControl_2.frame.origin.y + self.pageControl_2.frame.height - 6, width: self.trendingsalon_lbl.frame.width, height: self.trendingsalon_lbl.frame.height)
                    self.trndindsalon_view.frame = CGRect(x: self.trndindsalon_view.frame.origin.x, y: self.trendingsalon_lbl.frame.origin.y + self.trendingsalon_lbl.frame.height + 5, width: self.trndindsalon_view.frame.width, height: self.trndindsalon_view.frame.height)
                    return
                    //   videoPlayer.view.frame = CGRect(x: 15.0, y: 76.0, width: 289.0, height: 182.0)
                    // self.tableview.frame = CGRect(x: 0, y: 52.0, width: screenWidth, height: screenheight)
                }
                else if screenWidth == 375
                {
                    print("iphone 6")
                    self.scrollview.contentSize = CGSize(width: self.scrollview.contentSize.width, height: 1200 )
                    self.dashboardcollectionview.frame = CGRect(x: self.dashboardcollectionview.frame.origin.x, y: self.salonoffer_lbl.frame.origin.y + self.salonoffer_lbl.frame.height + 5 , width: self.dashboardcollectionview.frame.width, height: self.dashboardcollectionview.frame.height)
                    self.pageControl.frame = CGRect(x: self.pageControl.frame.origin.x, y: self.dashboardcollectionview.frame.origin.y + self.dashboardcollectionview.frame.height + 1 , width: self.pageControl.frame.width, height: self.pageControl.frame.height)
                    self.bmhsoffer_lbl.frame = CGRect(x: self.bmhsoffer_lbl.frame.origin.x, y: self.pageControl.frame.origin.y + self.pageControl.frame.height - 6 , width: self.bmhsoffer_lbl.frame.width, height: self.bmhsoffer_lbl.frame.height)
                    self.exclusivecollectionview.frame = CGRect(x: self.exclusivecollectionview.frame.origin.x, y: self.bmhsoffer_lbl.frame.origin.y + self.bmhsoffer_lbl.frame.height + 5, width: self.exclusivecollectionview.frame.width, height: self.exclusivecollectionview.frame.height)
                    self.pageControl_2.frame = CGRect(x: self.pageControl_2.frame.origin.x, y: self.exclusivecollectionview.frame.origin.y + self.exclusivecollectionview.frame.height + 1 , width: self.pageControl_2.frame.width, height: self.pageControl_2.frame.height)
                    
                    self.trendingsalon_lbl.frame = CGRect(x: self.trendingsalon_lbl.frame.origin.x, y: self.pageControl_2.frame.origin.y + self.pageControl_2.frame.height - 6, width: self.trendingsalon_lbl.frame.width, height: self.trendingsalon_lbl.frame.height)
                    self.trndindsalon_view.frame = CGRect(x: self.trndindsalon_view.frame.origin.x, y: self.trendingsalon_lbl.frame.origin.y + self.trendingsalon_lbl.frame.height + 5, width: self.trndindsalon_view.frame.width, height: self.trndindsalon_view.frame.height)
                    //  videoPlayer.view.frame = CGRect(x: 15.0, y: 100, width: 345.0, height: 200.0)
                    
                }
                else if screenWidth == 414.0
                {
                   self.scrollview.contentSize = CGSize(width: self.scrollview.contentSize.width, height: 1200 )
                   self.topmenu_view.frame = CGRect(x: 0, y: -1, width: 414, height: 70)
                   self.user_backgroundimg.frame = CGRect(x: 0, y: 65, width: 414, height: 86)
                    
                    self.dashboardcollectionview.frame = CGRect(x: self.dashboardcollectionview.frame.origin.x, y: self.salonoffer_lbl.frame.origin.y + self.salonoffer_lbl.frame.height + 5 , width: self.dashboardcollectionview.frame.width, height: self.dashboardcollectionview.frame.height)
                    self.pageControl.frame = CGRect(x: self.pageControl.frame.origin.x, y: self.dashboardcollectionview.frame.origin.y + self.dashboardcollectionview.frame.height + 1 , width: self.pageControl.frame.width, height: self.pageControl.frame.height)
                    self.bmhsoffer_lbl.frame = CGRect(x: self.bmhsoffer_lbl.frame.origin.x, y: self.pageControl.frame.origin.y + self.pageControl.frame.height - 6 , width: self.bmhsoffer_lbl.frame.width, height: self.bmhsoffer_lbl.frame.height)
                    self.exclusivecollectionview.frame = CGRect(x: self.exclusivecollectionview.frame.origin.x, y: self.bmhsoffer_lbl.frame.origin.y + self.bmhsoffer_lbl.frame.height + 5, width: self.exclusivecollectionview.frame.width, height: self.exclusivecollectionview.frame.height)
                    self.pageControl_2.frame = CGRect(x: self.pageControl_2.frame.origin.x, y: self.exclusivecollectionview.frame.origin.y + self.exclusivecollectionview.frame.height + 1 , width: self.pageControl_2.frame.width, height: self.pageControl_2.frame.height)
                    
                    self.trendingsalon_lbl.frame = CGRect(x: self.trendingsalon_lbl.frame.origin.x, y: self.pageControl_2.frame.origin.y + self.pageControl_2.frame.height - 6, width: self.trendingsalon_lbl.frame.width, height: self.trendingsalon_lbl.frame.height)
                    self.trndindsalon_view.frame = CGRect(x: self.trndindsalon_view.frame.origin.x, y: self.trendingsalon_lbl.frame.origin.y + self.trendingsalon_lbl.frame.height + 5, width: self.trndindsalon_view.frame.width, height: self.trndindsalon_view.frame.height)
                    
                    
//                    //self.salonoffer_lbl.frame =  CGRect(x: 110, y: 170, width: 150, height: 25)
//                    self.dashboardcollectionview.frame = CGRect(x: 0, y: 190, width: 414, height: 194)
//                    self.bmhsoffer_lbl.frame = CGRect(x: 110, y: 370, width: 152, height: 29)
//                    self.exclusivecollectionview.frame = CGRect(x: 0, y: 390, width: 414, height: 183)
//                    self.trendingsalon_lbl.frame = CGRect(x: 90, y: 570, width: 180, height: 33)
//                    self.trndindsalon_view.frame = CGRect(x: 0, y: 610, width: 414, height: 425)
                    
                    
                    
                    
                    //                        self.bmhsoffer_lbl.frame = CGRect(x: 109, y: 252, width: 196, height: 38)
                    //                        self.exclusivecollectionview.frame = CGRect(x: 0, y: 295, width: 414, height: 191)
                    //                        self.trendingsalon_lbl.frame = CGRect(x: 101, y: 486, width: 213, height: 26)
                    //                        self.trndindsalon_view.frame = CGRect(x: 6, y: 518, width: 406, height: 400)
                    //                        //  return
                    print("iphone 6s plus")
                    return
                    //   self.tableview.frame = CGRect(x: 0, y: 69.0, width: 414.0, height: screenheight)
                    //  videoPlayer.view.frame = CGRect(x: 15.0, y: 100.0, width: 384, height: 200.0)
                    
                }
                else if screenWidth > 415
                {
                    print("unexpested screensize")
                    
                }
                
                
                
                
                
                //                  self.mainview.frame = CGRect(x: mainview.frame.origin.x, y: mainview.frame.origin.y, width: mainview.frame.width, height: 1000)
                ////
                ////
                ////
                //                    let bottomconstraint = NSLayoutConstraint(item: self, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1, constant: 0)
                //                    bottomconstraint.isActive = true
                //self.view.addConstraint(bottomconstraint)
                
                //                    self.trendingsalon_lbl.frame = CGRect(x: trendingsalon_lbl.frame.origin.x, y: exclusivecollectionview.frame.origin.y + exclusivecollectionview.frame.height + 5, width: trendingsalon_lbl.frame.width
                //                        , height: trendingsalon_lbl.frame.height)
                //
                //                    self.trndindsalon_view.frame = CGRect(x: trndindsalon_view.frame.origin.x + 5 , y: exclusivecollectionview.frame.origin.y + exclusivecollectionview.frame.height + trendingsalon_lbl.frame.height + 10, width: trndindsalon_view.frame.width, height: trndindsalon_view.frame.height)
                
                
                
            }
            else if self.pathArray.count == 0 && self.exclusive_patharray.count == 0
            {
                
                self.dashboardcollectionview.isHidden = true
                self.exclusivecollectionview.isHidden = true
                let screenSize = UIScreen.main.bounds
                let screenWidth = screenSize.width
                _ = screenSize.height
                
                print("path and dashboard = 0:screenwidth\(screenWidth)")
                
                if screenWidth == 320.0
                {
                    self.scrollview.contentSize = CGSize(width: self.scrollview.contentSize.width, height: 800 )
                    
                    self.topmenu_view.frame = CGRect(x: 0, y: -1, width: 320, height: 70)
                    self.user_backgroundimg.frame = CGRect(x: 0, y: 65, width: 320, height: 86)
                    self.bmhsoffer_lbl.frame = CGRect(x: self.bmhsoffer_lbl.frame.origin.x, y: self.salonoffer_lbl.frame.origin.y + self.salonoffer_lbl.frame.height + 10 , width: self.bmhsoffer_lbl.frame.width, height: self.bmhsoffer_lbl.frame.height)
                    self.trendingsalon_lbl.frame = CGRect(x: self.trendingsalon_lbl.frame.origin.x, y: self.bmhsoffer_lbl.frame.origin.y + self.bmhsoffer_lbl.frame.height + 5, width: self.trendingsalon_lbl.frame.width, height: self.trendingsalon_lbl.frame.height)
                    self.trndindsalon_view.frame = CGRect(x: self.trndindsalon_view.frame.origin.x, y: self.trendingsalon_lbl.frame.origin.y + self.trendingsalon_lbl.frame.height + 5, width: self.trndindsalon_view.frame.width, height: self.trndindsalon_view.frame.height)
                    
                    return
                    //videoPlayer.view.frame = CGRect(x: 15.0, y: 76.0, width: 289.0, height: 182.0)
                    //self.tableview.frame = CGRect(x: 0, y: 52.0, width: screenWidth, height: screenheight)
                }
                else if screenWidth == 375
                {
                    print("iphone 6")
                    self.scrollview.contentSize = CGSize(width: self.scrollview.contentSize.width, height: self.mainview.frame.height - self.dashboardcollectionview.frame.height - self.dashboardcollectionview.frame.height )
                    self.bmhsoffer_lbl.frame = CGRect(x: self.bmhsoffer_lbl.frame.origin.x, y: self.salonoffer_lbl.frame.origin.y + self.salonoffer_lbl.frame.height + 10 , width: self.bmhsoffer_lbl.frame.width, height: self.bmhsoffer_lbl.frame.height)
                    self.trendingsalon_lbl.frame = CGRect(x: self.trendingsalon_lbl.frame.origin.x, y: self.bmhsoffer_lbl.frame.origin.y + self.bmhsoffer_lbl.frame.height + 5, width: self.trendingsalon_lbl.frame.width, height: self.trendingsalon_lbl.frame.height)
                    self.trndindsalon_view.frame = CGRect(x: self.trndindsalon_view.frame.origin.x, y: self.trendingsalon_lbl.frame.origin.y + self.trendingsalon_lbl.frame.height + 5, width: self.trndindsalon_view.frame.width, height: self.trndindsalon_view.frame.height)
                    
                    //videoPlayer.view.frame = CGRect(x: 15.0, y: 100, width: 345.0, height: 200.0)
                    
                }
                else if screenWidth == 414.0
                {
                    
                    
                    self.scrollview.contentSize = CGSize(width: self.scrollview.contentSize.width, height: self.mainview.frame.height - self.dashboardcollectionview.frame.height - self.dashboardcollectionview.frame.height )
                    self.topmenu_view.frame = CGRect(x: 0, y: -1, width: 414, height: 70)
                    self.user_backgroundimg.frame = CGRect(x: 0, y: 65, width: 414, height: 86)
                    self.bmhsoffer_lbl.frame = CGRect(x: self.bmhsoffer_lbl.frame.origin.x, y: self.salonoffer_lbl.frame.origin.y + self.salonoffer_lbl.frame.height + 10 , width: self.bmhsoffer_lbl.frame.width, height: self.bmhsoffer_lbl.frame.height)
                    self.trendingsalon_lbl.frame = CGRect(x: self.trendingsalon_lbl.frame.origin.x, y: self.bmhsoffer_lbl.frame.origin.y + self.bmhsoffer_lbl.frame.height + 5, width: self.trendingsalon_lbl.frame.width, height: self.trendingsalon_lbl.frame.height)
                    self.trndindsalon_view.frame = CGRect(x: self.trndindsalon_view.frame.origin.x, y: self.trendingsalon_lbl.frame.origin.y + self.trendingsalon_lbl.frame.height + 5, width: self.trndindsalon_view.frame.width, height: self.trndindsalon_view.frame.height)
                    
                    
                    
                    
                    
                    //                self.bmhsoffer_lbl.frame = CGRect(x: 109, y: 252, width: 196, height: 38)
                    //                self.trendingsalon_lbl.frame = CGRect(x: 101, y: 300, width: 213, height: 26)
                    //                self.trndindsalon_view.frame = CGRect(x: 0, y: 330, width: 398, height: 400)
                    
                    print("iphone 6s plus")
                    return
                    //   self.tableview.frame = CGRect(x: 0, y: 69.0, width: 414.0, height: screenheight)
                    //  videoPlayer.view.frame = CGRect(x: 15.0, y: 100.0, width: 384, height: 200.0)
                    
                }
                else if screenWidth > 415
                {
                    print("unexpested screensize")
                }
            }else if self.pathArray.count == 0
            {
                //self.mainview.frame = CGRect(x: mainview.frame.origin.x, y: mainview.frame.origin.y, width: mainview.frame.width, height: mainview.frame.height - dashboardcollectionview.frame.height + 100)
                //          let bottomconstraint = NSLayoutConstraint(item: mainview, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1, constant: 0)
                //           bottomconstraint.isActive = true
                //          self.view.addConstraint(bottomconstraint)
                // self.scrollview.contentSize = CGSize(width: self.scrollview.contentSize.width, height: mainview.frame.height - dashboardcollectionview.frame.height + 50 )
                self.dashboardcollectionview.isHidden = true
                self.exclusivecollectionview.isHidden = false
                let screenSize = UIScreen.main.bounds
                let screenWidth = screenSize.width
                _ = screenSize.height
                
                print("patharray:0 screenwidth\(screenWidth)")
                
                if screenWidth == 320.0
                {
                    print("path:0 Came")
                    self.scrollview.contentSize = CGSize(width: self.scrollview.contentSize.width, height: 1050 )
                    
                    self.topmenu_view.frame = CGRect(x: 0, y: -1, width: 320, height: 70)
                    self.user_backgroundimg.frame = CGRect(x: 0, y: 65, width: 320, height: 86)
                    self.bmhsoffer_lbl.frame = CGRect(x: self.bmhsoffer_lbl.frame.origin.x, y: self.salonoffer_lbl.frame.origin.y + self.salonoffer_lbl.frame.height + 10 , width: self.bmhsoffer_lbl.frame.width, height: self.bmhsoffer_lbl.frame.height)
                    self.exclusivecollectionview.frame = CGRect(x: self.exclusivecollectionview.frame.origin.x, y: self.bmhsoffer_lbl.frame.origin.y + self.bmhsoffer_lbl.frame.height + 5, width: self.exclusivecollectionview.frame.width, height: self.exclusivecollectionview.frame.height)
                    self.pageControl_2.frame = CGRect(x: self.pageControl_2.frame.origin.x, y: self.exclusivecollectionview.frame.origin.y + self.exclusivecollectionview.frame.height + 1 , width: self.pageControl_2.frame.width, height: self.pageControl_2.frame.height)
                    
                    self.trendingsalon_lbl.frame = CGRect(x: self.trendingsalon_lbl.frame.origin.x, y: self.pageControl_2.frame.origin.y + self.pageControl_2.frame.height - 6, width: self.trendingsalon_lbl.frame.width, height: self.trendingsalon_lbl.frame.height)
                    self.trndindsalon_view.frame = CGRect(x: self.trndindsalon_view.frame.origin.x, y: self.trendingsalon_lbl.frame.origin.y + self.trendingsalon_lbl.frame.height + 5, width: self.trndindsalon_view.frame.width, height: self.trndindsalon_view.frame.height)
                    return
                    //   videoPlayer.view.frame = CGRect(x: 15.0, y: 76.0, width: 289.0, height: 182.0)
                    // self.tableview.frame = CGRect(x: 0, y: 52.0, width: screenWidth, height: screenheight)
                }
                else if screenWidth == 375
                {
                    print("iphone 6")
                    self.scrollview.contentSize = CGSize(width: self.scrollview.contentSize.width, height: self.mainview.frame.height - self.dashboardcollectionview.frame.height )
                    
                    self.bmhsoffer_lbl.frame = CGRect(x: self.bmhsoffer_lbl.frame.origin.x, y: self.salonoffer_lbl.frame.origin.y + self.salonoffer_lbl.frame.height + 10 , width: self.bmhsoffer_lbl.frame.width, height: self.bmhsoffer_lbl.frame.height)
                    self.exclusivecollectionview.frame = CGRect(x: self.exclusivecollectionview.frame.origin.x, y: self.bmhsoffer_lbl.frame.origin.y + self.bmhsoffer_lbl.frame.height + 5, width: self.exclusivecollectionview.frame.width, height: self.exclusivecollectionview.frame.height)
                    self.pageControl_2.frame = CGRect(x: self.pageControl_2.frame.origin.x, y: self.exclusivecollectionview.frame.origin.y + self.exclusivecollectionview.frame.height + 1 , width: self.pageControl_2.frame.width, height: self.pageControl_2.frame.height)
                    
                    self.trendingsalon_lbl.frame = CGRect(x: self.trendingsalon_lbl.frame.origin.x, y: self.pageControl_2.frame.origin.y + self.pageControl_2.frame.height - 6, width: self.trendingsalon_lbl.frame.width, height: self.trendingsalon_lbl.frame.height)
                    self.trndindsalon_view.frame = CGRect(x: self.trndindsalon_view.frame.origin.x, y: self.trendingsalon_lbl.frame.origin.y + self.trendingsalon_lbl.frame.height + 5, width: self.trndindsalon_view.frame.width, height: self.trndindsalon_view.frame.height)
                    //videoPlayer.view.frame = CGRect(x: 15.0, y: 100, width: 345.0, height: 200.0)
                    
                }
                else if screenWidth == 414.0
                {
                    self.scrollview.contentSize = CGSize(width: self.scrollview.contentSize.width, height: self.mainview.frame.height - self.dashboardcollectionview.frame.height )
                    
                    self.topmenu_view.frame = CGRect(x: 0, y: -1, width: 414, height: 70)
                    self.user_backgroundimg.frame = CGRect(x: 0, y: 65, width: 414, height: 86)
                    self.bmhsoffer_lbl.frame = CGRect(x: self.bmhsoffer_lbl.frame.origin.x, y: self.salonoffer_lbl.frame.origin.y + self.salonoffer_lbl.frame.height + 10 , width: self.bmhsoffer_lbl.frame.width, height: self.bmhsoffer_lbl.frame.height)
                    self.exclusivecollectionview.frame = CGRect(x: self.exclusivecollectionview.frame.origin.x, y: self.bmhsoffer_lbl.frame.origin.y + self.bmhsoffer_lbl.frame.height + 5, width: self.exclusivecollectionview.frame.width, height: self.exclusivecollectionview.frame.height)
                    self.pageControl_2.frame = CGRect(x: self.pageControl_2.frame.origin.x, y: self.exclusivecollectionview.frame.origin.y + self.exclusivecollectionview.frame.height + 1 , width: self.pageControl_2.frame.width, height: self.pageControl_2.frame.height)
                    
                    self.trendingsalon_lbl.frame = CGRect(x: self.trendingsalon_lbl.frame.origin.x, y: self.pageControl_2.frame.origin.y + self.pageControl_2.frame.height - 6, width: self.trendingsalon_lbl.frame.width, height: self.trendingsalon_lbl.frame.height)
                    self.trndindsalon_view.frame = CGRect(x: self.trndindsalon_view.frame.origin.x, y: self.trendingsalon_lbl.frame.origin.y + self.trendingsalon_lbl.frame.height + 5, width: self.trndindsalon_view.frame.width, height: self.trndindsalon_view.frame.height)                    //  return
                    print("iphone 6s plus")
                    return
                    //self.tableview.frame = CGRect(x: 0, y: 69.0, width: 414.0, height: screenheight)
                    //videoPlayer.view.frame = CGRect(x: 15.0, y: 100.0, width: 384, height: 200.0)
                    
                }
                else if screenWidth > 415
                {
                    print("unexpested screensize")
                    
                }
            }else if self.exclusive_patharray.count == 0
            {
                self.dashboardcollectionview.isHidden = false
                self.exclusivecollectionview.isHidden = true
                
                let screenSize = UIScreen.main.bounds
                let screenWidth = screenSize.width
                _ = screenSize.height
                self.bmhsoffer_lbl.isHidden = false
                print("exclusive:0 screenwidth\(screenWidth)")
                
                if screenWidth == 320.0
                {
                    self.scrollview.contentSize = CGSize(width: self.scrollview.contentSize.width, height: 1050 )
                    self.topmenu_view.frame = CGRect(x: 0, y: -1, width: 320, height: 70)
                    self.user_backgroundimg.frame = CGRect(x: 0, y: 65, width: 320, height: 86)
                    self.dashboardcollectionview.frame = CGRect(x: self.dashboardcollectionview.frame.origin.x, y: self.salonoffer_lbl.frame.origin.y + self.salonoffer_lbl.frame.height + 5 , width: self.dashboardcollectionview.frame.width, height: self.dashboardcollectionview.frame.height)
                    self.pageControl.frame = CGRect(x: self.pageControl.frame.origin.x, y: self.dashboardcollectionview.frame.origin.y + self.dashboardcollectionview.frame.height + 1 , width: self.pageControl.frame.width, height: self.pageControl.frame.height)
                    self.bmhsoffer_lbl.frame = CGRect(x: self.bmhsoffer_lbl.frame.origin.x, y: self.pageControl.frame.origin.y + self.pageControl.frame.height - 6 , width: self.bmhsoffer_lbl.frame.width, height: self.bmhsoffer_lbl.frame.height)
                    self.trendingsalon_lbl.frame = CGRect(x: self.trendingsalon_lbl.frame.origin.x, y: self.bmhsoffer_lbl.frame.origin.y + self.bmhsoffer_lbl.frame.height + 5, width: self.trendingsalon_lbl.frame.width, height: self.trendingsalon_lbl.frame.height)
                    self.trndindsalon_view.frame = CGRect(x: self.trndindsalon_view.frame.origin.x, y: self.trendingsalon_lbl.frame.origin.y + self.trendingsalon_lbl.frame.height + 5, width: self.trndindsalon_view.frame.width, height: self.trndindsalon_view.frame.height)
                    
                    //                self.bmhsoffer_lbl.frame = CGRect(x: 84, y: 333, width: 152, height: 16)
                    //                self.trendingsalon_lbl.frame = CGRect(x: 78, y: 360, width: 165, height: 20)
                    //                self.trndindsalon_view.frame = CGRect(x: 6, y: 385, width: 308, height: 300)
                    //                return
                    //   videoPlayer.view.frame = CGRect(x: 15.0, y: 76.0, width: 289.0, height: 182.0)
                    // self.tableview.frame = CGRect(x: 0, y: 52.0, width: screenWidth, height: screenheight)
                }
                else if screenWidth == 375
                {
                    print("iphone 6")
                    self.scrollview.contentSize = CGSize(width: self.scrollview.contentSize.width, height: self.mainview.frame.height - self.dashboardcollectionview.frame.height )
                    self.dashboardcollectionview.frame = CGRect(x: self.dashboardcollectionview.frame.origin.x, y: self.salonoffer_lbl.frame.origin.y + self.salonoffer_lbl.frame.height + 5 , width: self.dashboardcollectionview.frame.width, height: self.dashboardcollectionview.frame.height)
                    self.pageControl.frame = CGRect(x: self.pageControl.frame.origin.x, y: self.dashboardcollectionview.frame.origin.y + self.dashboardcollectionview.frame.height + 1 , width: self.pageControl.frame.width, height: self.pageControl.frame.height)
                    self.bmhsoffer_lbl.frame = CGRect(x: self.bmhsoffer_lbl.frame.origin.x, y: self.pageControl.frame.origin.y + self.pageControl.frame.height - 6 , width: self.bmhsoffer_lbl.frame.width, height: self.bmhsoffer_lbl.frame.height)
                    self.trendingsalon_lbl.frame = CGRect(x: self.trendingsalon_lbl.frame.origin.x, y: self.bmhsoffer_lbl.frame.origin.y + self.bmhsoffer_lbl.frame.height + 5, width: self.trendingsalon_lbl.frame.width, height: self.trendingsalon_lbl.frame.height)
                    self.trndindsalon_view.frame = CGRect(x: self.trndindsalon_view.frame.origin.x, y: self.trendingsalon_lbl.frame.origin.y + self.trendingsalon_lbl.frame.height + 5, width: self.trndindsalon_view.frame.width, height: self.trndindsalon_view.frame.height)
                        //  videoPlayer.view.frame = CGRect(x: 15.0, y: 100, width: 345.0, height: 200.0)
                    
                }
                else if screenWidth == 414.0
                {
                    self.scrollview.contentSize = CGSize(width: self.scrollview.contentSize.width, height: self.mainview.frame.height - self.dashboardcollectionview.frame.height )
                    
                    self.topmenu_view.frame = CGRect(x: 0, y: -1, width: 414, height: 70)
                    self.user_backgroundimg.frame = CGRect(x: 0, y: 65, width: 414, height: 86)
                    self.dashboardcollectionview.frame = CGRect(x: self.dashboardcollectionview.frame.origin.x, y: self.salonoffer_lbl.frame.origin.y + self.salonoffer_lbl.frame.height + 5 , width: self.dashboardcollectionview.frame.width, height: self.dashboardcollectionview.frame.height)
                    self.pageControl.frame = CGRect(x: self.pageControl.frame.origin.x, y: self.dashboardcollectionview.frame.origin.y + self.dashboardcollectionview.frame.height + 1 , width: self.pageControl.frame.width, height: self.pageControl.frame.height)
                    self.bmhsoffer_lbl.frame = CGRect(x: self.bmhsoffer_lbl.frame.origin.x, y: self.pageControl.frame.origin.y + self.pageControl.frame.height - 6 , width: self.bmhsoffer_lbl.frame.width, height: self.bmhsoffer_lbl.frame.height)
                    self.trendingsalon_lbl.frame = CGRect(x: self.trendingsalon_lbl.frame.origin.x, y: self.bmhsoffer_lbl.frame.origin.y + self.bmhsoffer_lbl.frame.height + 5, width: self.trendingsalon_lbl.frame.width, height: self.trendingsalon_lbl.frame.height)
                    self.trndindsalon_view.frame = CGRect(x: self.trndindsalon_view.frame.origin.x, y: self.trendingsalon_lbl.frame.origin.y + self.trendingsalon_lbl.frame.height + 5, width: self.trndindsalon_view.frame.width, height: self.trndindsalon_view.frame.height)
                    
                    
                    
                    
                    
                    //                self.bmhsoffer_lbl.frame = CGRect(x: 109, y: 435, width: 196, height: 38)
                    //                self.trendingsalon_lbl.frame = CGRect(x: 101, y: 475, width: 213, height: 26)
                    //                self.trndindsalon_view.frame = CGRect(x: 8, y: 501, width: 398, height: 400)
                    //  return
                    print("iphone 6s plus")
                    return
                    //   self.tableview.frame = CGRect(x: 0, y: 69.0, width: 414.0, height: screenheight)
                    //  videoPlayer.view.frame = CGRect(x: 15.0, y: 100.0, width: 384, height: 200.0)
                    
                }
                else if screenWidth > 415
                {
                    print("unexpested screensize")
                    
                }
                
                
            }
            print("Secondary queue ended")
        }
       
        
        
        
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let visibleRect = CGRect(origin:
            self.dashboardcollectionview.contentOffset, size: self.dashboardcollectionview.bounds.size)
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        if let visibleIndexPath = self.dashboardcollectionview.indexPathForItem(at: visiblePoint) {
            self.pageControl.numberOfPages = pathArray.count
            self.pageControl.currentPage = visibleIndexPath.row
        }
        
        let visibleRect2 = CGRect(origin:
            self.exclusivecollectionview.contentOffset, size: self.exclusivecollectionview.bounds.size)
        let visiblePoint2 = CGPoint(x: visibleRect2.midX, y: visibleRect2.midY)
        if let visibleIndexPath2 = self.exclusivecollectionview.indexPathForItem(at: visiblePoint2) {
            self.pageControl_2.numberOfPages = exclusive_patharray.count
            self.pageControl_2.currentPage = visibleIndexPath2.row
        }
        
        }
    
   
    //Do any additional setup after loading the view.
    @IBOutlet weak var dashboardcollectionview: UICollectionView!
    
    @IBOutlet weak var user_backgroundimg: UIView!
    @IBOutlet weak var superview: UIView!
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //        mobilebanner()
        //        getdashboardoffers()
        //        getexclusiveoffers()
        //        getuserlocation()
        //        getTrendingsalon()
        
     
        
        //        let fullString = NSMutableAttributedString(string : "TRENDING SALON")
        //
        //        // create our NSTextAttachment
        //        let image1Attachment = NSTextAttachment()
        //        image1Attachment.image = UIImage(named: "trendinglogo")
        //        image1Attachment.bounds = CGRect(x: 5, y: -2, width: 13, height: 13)
        //
        //        // wrap the attachment in its own attributed string so we can append it
        //        let image1String = NSAttributedString(attachment: image1Attachment)
        //
        //        // add the NSTextAttachment wrapper to our full string, then add some more text.
        //        fullString.append(image1String)
        //
        //        // draw the result in a label
        //        // yourLabel.attributedText = fullString
        //        trendingsalon_lbl.attributedText = fullString
        //        let screenSize = UIScreen.main.bounds
        //        let screenWidth = screenSize.width
        //        let screenheight = screenSize.height
        //
        //        print("screenwidth\(screenWidth)")
        //
        //        if screenWidth == 320.0
        //        {
        //
        //
        //        if pathArray.count != 0
        //        {
        //            trending_lbl.frame = CGRect(x: trending_lbl.frame.origin.x, y: exclusivecollectionview.frame.origin.y + exclusivecollectionview.frame.height + 5, width: trending_lbl.frame.width
        //                , height: trending_lbl.frame.height)
        //
        //
        //
        //
        //
        //        }
        //        }
        
        
    }
    
    
    
    
//    let screenSize = UIScreen.main.bounds
//    let screenWidth = screenSize.width
//    let screenheight = screenSize.height
//    if screenWidth == 375
//    {
//    pageControl_2.frame =  CGRect(x: pageControl_2.frame.origin.x , y: pageControl_2.frame.origin.y + 40 , width: pageControl_2.frame.width, height:  pageControl_2.frame.height)
//    }
//    
    
    
    
    @IBOutlet weak var btn_salonLocation: UIButton!
    @IBAction func salon_location(_ sender: Any) {
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    func startTimer() {
        
        _ =  Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(self.scrollAutomatically), userInfo: nil, repeats: true)
    }
    func startTimer2() {
        
        _ =  Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(self.scrollAutomatically2), userInfo: nil, repeats: true)
    }
    
    
    @objc func scrollAutomatically(_ timer1: Timer) {
        
        if let coll  = dashboardcollectionview {
            for cell in coll.visibleCells {
                let indexPath: IndexPath? = coll.indexPath(for: cell)
                if ((indexPath?.row)!  < pathArray.count - 1){
                    let indexPath1: IndexPath?
                    indexPath1 = IndexPath.init(row: (indexPath?.row)! + 1, section: (indexPath?.section)!)
                    
                    coll.scrollToItem(at: indexPath1!, at: .centeredHorizontally, animated: true)
                }
                else{
                    let indexPath1: IndexPath?
                    indexPath1 = IndexPath.init(row: 0, section: (indexPath?.section)!)
                    coll.scrollToItem(at: indexPath1!, at: .left, animated: true)
                }
                
            }
        }
    }
    @objc func scrollAutomatically2(_ timer1: Timer) {
        
        if let coll  = exclusivecollectionview {
            for cell in coll.visibleCells {
                let indexPath: IndexPath? = coll.indexPath(for: cell)
                if ((indexPath?.row)!  < exclusive_patharray.count - 1){
                    let indexPath1: IndexPath?
                    indexPath1 = IndexPath.init(row: (indexPath?.row)! + 1, section: (indexPath?.section)!)
                    
                    coll.scrollToItem(at: indexPath1!, at: .centeredHorizontally, animated: true)
                }
                else{
                    let indexPath1: IndexPath?
                    indexPath1 = IndexPath.init(row: 0, section: (indexPath?.section)!)
                    coll.scrollToItem(at: indexPath1!, at: .left, animated: true)
                }
                
            }
        }
    }
    @objc func scrollToNextCell(){
        
        let cellSize = view.frame.size
        
        //get current content Offset of the Collection view
        let contentOffset = dashboardcollectionview.contentOffset
        
        if dashboardcollectionview.contentSize.width <= dashboardcollectionview.contentOffset.x + cellSize.width
        {
            let r = CGRect(x: 0, y: contentOffset.y, width: cellSize.width, height: cellSize.height)
            dashboardcollectionview.scrollRectToVisible(r, animated: true)
            print("Printing R1 \(r)")
            
        } else {
            let r = CGRect(x: contentOffset.x + cellSize.width, y: contentOffset.y, width: cellSize.width, height: cellSize.height)
            dashboardcollectionview.scrollRectToVisible(r, animated: true);
            print("Printing R2 \(r)")
        }
        
        
    }
    
    /**
     Invokes Timer to start Automatic Animation with repeat enabled
     */
    func ScrollstartTimer() {
        
        _ = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(scrollToNextCell), userInfo: nil, repeats: true);
        
        
    }
    
    func getdashboardoffers()
    {
        clickedsalon_id_Array.removeAll()
        
        self.pathArray.removeAll()
        let url = CardName.sharedInstance.dashboardoffers_url
        print(url)
        _ = UserDefaults.standard.string(forKey: "tokentype")
        var token = ""
        var type = ""
        
        type = UserDefaults.standard.string(forKey: "token")!
        token = UserDefaults.standard.string(forKey: "Authorizent_token")!
        
        
        let header = [
            "Authorization": "\(type) \(token)",
        ]
        print("headers\(header)")
        Alamofire.request(url, method: .get, encoding: JSONEncoding.default,headers : header)
            .responseJSON { response in
                print("Salon offers response\(response)")
//                self.dashboardcollectionview.delegate = self
//                self.dashboardcollectionview.dataSource = self
                if response.error == nil
                {
                    let responce_array = response.result.value as! NSArray
                    if responce_array.count == 0
                    {
                        print("count 0")
                    }else{
                        for i in 0..<responce_array.count
                        {
                            let responce_value = responce_array[i] as! NSDictionary
                            let banner_url = responce_value.value(forKey: "BannerURL") as! String
                            let salon_id = responce_value.value(forKey: "SaloonDetailsID") as! Int
                            self.clickedsalon_id_Array.append(salon_id)
                            
                            //                        let salonservice_id = responce_value.value(forKey: "ServiceIDs") as! String
                            //                        self.salonservice_id_Array.append(salonservice_id)
                            
                            self.pathArray.append(banner_url)
                        }
                        self.dashboardcollectionview.reloadData()
                    }
                }else
                {
                    let alert = UIAlertController(title: "Alert", message: "Network Error, Try Again!", preferredStyle: .alert)
                    _ = alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
                    self.present(alert,animated: true,completion: nil)
                }
                
        }
    }
    
    func mobilebanner()
    {
        self.exclusive_patharray.removeAll()
        
        let url = CardName.sharedInstance.mobileBanner_url
        var token = ""
        var type = ""
        
        type = UserDefaults.standard.string(forKey: "token")!
        token = UserDefaults.standard.string(forKey: "Authorizent_token")!
        
        
        let header = [
            "Authorization": "\(type) \(token)",
        ]
        print("headers1212\(header)")
        Alamofire.request(url, method: .get, encoding: JSONEncoding.default,headers : header)
            .responseJSON { response in
                print("Exclusive offer response\(response)")
                print("token_ex\(token)")
//                self.exclusivecollectionview.delegate = self
//                self.exclusivecollectionview.dataSource = self
                if response.error == nil{
                    let responce_array = response.result.value as! NSArray
                    
                    if responce_array.count == 0
                    {
                        print("count 0")
                        //self.exclusivecollectionview.isHidden = true
                    }else
                    {
                        //self.exclusive_patharray.removeAll()
                        for i in 0..<responce_array.count
                        {
                            let responce_value = responce_array[i] as! NSDictionary
                            let banner_url = responce_value.value(forKey: "ImageURL") as! String
                            self.exclusive_patharray.append(banner_url)
                        }
                        self.exclusivecollectionview.reloadData()
                    }
                    //self.exclusivecollectionview.reloadData()
                }else
                {
                    let alert = UIAlertController(title: "Alert", message: "Network Error, Try Again!", preferredStyle: .alert)
                    _ = alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
                    self.present(alert,animated: true,completion: nil)
                }
                
                
                
        }
        
    }
    
    func getSalonInfo()
    {
        
        let clicked_id = UserDefaults.standard.string(forKey: "clicked_id")
        print("clicked_id\(clicked_id)")
        let parameters: [String: Any] = [
            "saloonID" : clicked_id
        ]
        let url = CardName.sharedInstance.infoDetails_url
        print("info_url\(url)")
        let type = UserDefaults.standard.string(forKey: "token")!
        let token = UserDefaults.standard.string(forKey: "Authorizent_token")!
        let header = [
            "Authorization": "\(type) \(token)",
        ]
        Alamofire.request(url, method: .post,parameters: parameters, encoding: JSONEncoding.default,headers : header)
            .responseJSON { response in
                print("DASHBOARD::salonInforesponse==\(response)")
                let response_value = response.result.value as! NSDictionary
                let salon_name = response_value.value(forKey: "Name") as! String
                UserDefaults.standard.set(salon_name, forKey: "Salon_Name")
                let salon_logo = response_value.value(forKey: "Logo") as! String
                UserDefaults.standard.set(salon_logo, forKey: "Salon_Logo")
                let salon_city = response_value.value(forKey: "City") as! String
                UserDefaults.standard.set(salon_city, forKey: "Salon_Location")
                let gender = response_value.value(forKey: "GenderType") as! String
                CardName.sharedInstance.gender_type = gender
                print("Gender Checking : \(CardName.sharedInstance.gender_type)")
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let subclass = storyboard.instantiateViewController(withIdentifier: "SalonDetails_ViewController") as! UITabBarController
                subclass.selectedIndex = 1
                self.present(subclass,animated: true,completion: nil)
        }
    }
    @IBAction func btn_moresalons(_ sender: Any) {
        
      //Crashlytics.sharedInstance().crash()
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let subclass = storyboard.instantiateViewController(withIdentifier: "DashboardViewController") as! UITabBarController
        subclass.selectedIndex = 1
        self.present(subclass,animated: true,completion: nil)
    }
    //    func getexclusiveoffers()
    //    {
    //        self.exclusive_patharray.removeAll()
    //        let url = CardName.sharedInstance.exclusiveoffers_url
    //        print("exclusive_url\(url)")
    //        let token_type = UserDefaults.standard.string(forKey: "tokentype")
    //        var token = ""
    //        var type = ""
    //
    //            type = UserDefaults.standard.string(forKey: "token")!
    //            token = UserDefaults.standard.string(forKey: "Authorizent_token")!
    //
    //        let header = [
    //            "Authorization": "\(type) \(token)",
    //        ]
    //        print("header_ex\(header)")
    //        Alamofire.request(url, method: .get, encoding: JSONEncoding.default,headers : header)
    //            .responseJSON { response in
    //                print("responce\(response)")
    //                print("token_ex\(token)")
    //                if response.error == nil{
    //                     let responce_array = response.result.value as! NSArray
    //                    if responce_array.count == 0
    //                    {
    //                        print("count 0")
    //                        //   self.exclusivecollectionview.isHidden = true
    //                    }else
    //                    {
    //                        for i in 0..<responce_array.count
    //                        {
    //                            let responce_value = responce_array[i] as! NSDictionary
    //                            let banner_url = responce_value.value(forKey: "BannerURL") as! String
    //                            self.exclusive_patharray.append(banner_url)
    //                        }
    //                      //  self.exclusivecollectionview.reloadData()
    //                    }
    //                    self.exclusivecollectionview.reloadData()
    //                }else
    //                {
    //                    let alert = UIAlertController(title: "Alert", message: "Network Error, Try Again!", preferredStyle: .alert)
    //                    let okaction = alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
    //                    self.present(alert,animated: true,completion: nil)
    //              }
    //
    //
    //
    //        }
    //    }
    func getuserlocation()
    {
        let url = CardName.sharedInstance.userlocation_url
        print(url)
        _ = UserDefaults.standard.string(forKey: "tokentype")
        var token = ""
        var type = ""
        
        type = UserDefaults.standard.string(forKey: "token")!
        token = UserDefaults.standard.string(forKey: "Authorizent_token")!
        
        let header = [
            "Authorization": "\(type) \(token)",
        ]
        print("headers\(header)")
        Alamofire.request(url, method: .get, encoding: JSONEncoding.default,headers : header)
            .responseJSON { response in
                print("responcelocation\(response)")
                if response.error == nil
                {
                    let responce_value = response.result.value as! NSDictionary
                    if UserDefaults.standard.string(forKey: "MasterCityName") == nil{
                        let city = responce_value.value(forKey: "MasterCityName") as! String
                        
                        if city == ""
                        {
                            if UserDefaults.standard.string(forKey: "city_name") != nil && UserDefaults.standard.string(forKey: "city_name") != ""{
                                
                            }else
                            {
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                let subclass = storyboard.instantiateViewController(withIdentifier: "LocationViewController") as! LocationViewController
                                self.present(subclass,animated: true,completion: nil)
                            }
                            
                        }else{
                            UserDefaults.standard.set(city, forKey: "city_name")
                        }
                        //                        if UserDefaults.standard.bool(forKey: "city_selected") != nil
                        //                        {
                        //                          let islogged = UserDefaults.standard.bool(forKey: "city_selected")
                        //                            if islogged
                        //                            {
                        //
                        //                            }else
                        //                            {
                        //
                        //                            }
                        //                        }
                        //
                        //                    }else if city == "Coimbatore"
                        //                    {
                        //                        self.citylocation.image = UIImage(named:"Maruthamalai.png")
                        //                        UserDefaults.standard.set("Coimbatore", forKey: "city_name")
                        //                    }
                        //                    else if city == "Chennai"
                        //                    {
                        //                        self.citylocation.image = UIImage(named: "ic_home_user_info_bg_chennai")
                        //                        UserDefaults.standard.set("Chennai", forKey: "city_name")
                        //                    }else if city == "Bangalore"
                        //                    {
                        //                        self.citylocation.image = UIImage(named: "ic_home_user_info_bg_blore")
                        //                        UserDefaults.standard.set("Bangalore", forKey: "city_name")
                        //                    }
                        let username = UserDefaults.standard.bool(forKey: "isLogged")
                        if username
                        {
                            self.getusername()
                            
                        }else{
                            self.welcome_lbl.text = "Hi Guest!"
                            
                        }
                    }
                    else
                    {
                        let alert = UIAlertController(title: "Alert", message: "Network Error, Try Again!", preferredStyle: .alert)
                        _ = alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
                        self.present(alert,animated: true,completion: nil)
                    }
                    
                }
        }
    }
    func getusername()
    {
        
        
        let url = CardName.sharedInstance.profile_url
        print(url)
        
        _ = UserDefaults.standard.string(forKey: "tokentype")
        var token = ""
        var type = ""
        
        type = UserDefaults.standard.string(forKey: "token")!
        token = UserDefaults.standard.string(forKey: "Authorizent_token")!
        
        let header = [
            "Authorization": "\(type) \(token)",
        ]
        Alamofire.request(url, method: .post, encoding: JSONEncoding.default,headers : header)
            .responseJSON { response in
                if response.error == nil{
                    print("header\(header)")
                    print("response\(response)")
                    let responce_value = response.result.value as! NSDictionary
                    let email = responce_value.value(forKey: "EmailID") as! String
                    UserDefaults.standard.set(email, forKey: "Email_ID")
                    let fullname = responce_value.value(forKey: "FullName") as! String
                    UserDefaults.standard.set(fullname, forKey: "FullName")
                    self.welcome_lbl.text = "Hi \(fullname)!"
                    _ = responce_value.value(forKey: "Gender") as! Int
                    _ = responce_value.value(forKey: "UserName") as! String
                }
                    
                else
                {
                    let alert = UIAlertController(title: "Alert", message: "Network Error, Try Again!", preferredStyle: .alert)
                    _ = alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
                    self.present(alert,animated: true,completion: nil)
                }
                
                
        }
        
        
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        CardName.sharedInstance.camefrom = "Dashboard"
        if clicked_id_Array.count != 0 && clicked_id_Array.count != nil
        {
        UserDefaults.standard.set(self.clicked_id_Array[indexPath.row], forKey: "clicked_id")
        }
        if salonname_Array.count != 0 && salonname_Array.count != nil
        {
        UserDefaults.standard.set(self.salonname_Array[indexPath.row], forKey: "Salon_Name")
        }
        if salonLogo_Array.count != 0 && salonLogo_Array.count != nil
        {
            UserDefaults.standard.set(self.salonLogo_Array[indexPath.row], forKey: "Salon_Logo")
        }
        if Locationname_Array.count != 0 && Locationname_Array.count != nil
        {
        UserDefaults.standard.set(self.Locationname_Array[indexPath.row], forKey: "Salon_Location")
        }
        if salontype_Array.count != 0 && salontype_Array.count != nil
        {
        CardName.sharedInstance.gender_type = salontype_Array[indexPath.row]
        }
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let subclass = storyboard.instantiateViewController(withIdentifier: "SalonDetails_ViewController") as! UITabBarController
        subclass.selectedIndex = 1
        self.present(subclass,animated: true,completion: nil)
    }
    func getTrendingsalon()
    {
        salonname_Array.removeAll()
        salontype_Array.removeAll()
        salonrating_Array.removeAll()
        saloncheckins_Array.removeAll()
        salonDistance_Array.removeAll()
        Locationname_Array.removeAll()
        salonLogo_Array.removeAll()
        facility_array.removeAll()
        clicked_id_Array.removeAll()
        verified_array.removeAll()
        let url = CardName.sharedInstance.trendingsalon_url
        print(url)
        let type = UserDefaults.standard.string(forKey: "token")!
        let token = UserDefaults.standard.string(forKey: "Authorizent_token")!
        
        
        let header = [
            "Authorization": "\(type) \(token)",
        ]
        
        Alamofire.request(url, method: .get,encoding: JSONEncoding.default,headers : header)
            .responseJSON { response in
                print("trendingsalon\(response)")
                if response.error == nil
                {
                    let response_value = response.result.value as! NSArray
                    print(response_value.count)
                    for i in 0..<response_value.count
                    {
                        do
                        {
                            let salon_response = response_value[i] as! NSDictionary
                            let count = try? salon_response.value(forKey: "Count") as! Int
                            self.saloncheckins_Array.append(count!)
                            // print(salon_response.value(forKey: "Distance"))
                            let Distance = try? salon_response.value(forKey: "Distance") as! Double
                            self.salonDistance_Array.append(Distance!)
                            let genderType = try? salon_response.value(forKey: "SaloonTypeName") as! String
                            self.salontype_Array.append(genderType!)
                            let id = try? salon_response.value(forKey: "SaloonDetailsId") as! Int
                            print("Salon_id\(id)")
                            //self.salon_id.append(id ?? 0)
                            self.clicked_id_Array.append(id!)
                            let Latitute = try? salon_response.value(forKey: "Latitude") as! Double
                            self.salon_latArray.append(Latitute ?? 0.0)
                            let Location = try? salon_response.value(forKey: "AreaName") as! String
                            self.Locationname_Array.append(Location!)
                            let Logo = try? salon_response.value(forKey: "SaloonLogo") as! String
                            self.salonLogo_Array.append(Logo!)
                            let Longitute = try? salon_response.value(forKey: "Longitude") as! Double
                            self.salon_logArray.append(Longitute ?? 0.0)
                            let name = try? salon_response.value(forKey: "SaloonName") as! String
                            self.salonname_Array.append(name!)
                            _ = try? salon_response.value(forKey: "OfferCount") as! Int
                            let rating = try? salon_response.value(forKey: "UserRating") as! Double
                            self.salonrating_Array.append(rating!)
                            _ = try? salon_response.value(forKey: "RatingCount") as! Int
                            let isverified = try? salon_response.value(forKey: "IsVerified") as! Bool
                            self.verified_array.append(isverified ?? true)
                            
                        }
                        catch
                        {
                            print(error)
                        }
                        
                    }
                    self.tableview.reloadData()
                }else
                {
                    let alert = UIAlertController(title: "Alert", message: "Network Error, Try Again!", preferredStyle: .alert)
                    _ = alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
                    self.present(alert,animated: true,completion: nil)
                }
        }
        
    }
}
