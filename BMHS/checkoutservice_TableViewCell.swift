//
//  checkoutservice_TableViewCell.swift
//  BMHS
//
//  Created by TechBT on 22/10/18.
//  Copyright © 2018 TechBT. All rights reserved.
//

import UIKit

class checkoutservice_TableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    @IBOutlet weak var service_amount: UILabel!
    @IBOutlet weak var service_name: UILabel!
    
    @IBOutlet weak var tickimage: UIImageView!
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
