//
//  SignupViewController.swift
//  BMHS
//
//  Created by TechBT on 21/09/18.
//  Copyright © 2018 TechBT. All rights reserved.
//

import UIKit
import Alamofire

class SignupViewController: UIViewController,SSRadioButtonControllerDelegate,UITextFieldDelegate{
 var radioButtonController: SSRadioButtonsController?
    var clicked_gender = ""
    var coupan_code = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        let layer = self.overall_view.layer
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        layer.shadowRadius = 6.0
        layer.shadowOpacity = 0.4
        layer.masksToBounds = false
        self.overall_view.layer.cornerRadius = 5
        let layer1 = self.btn_back.layer
        layer1.shadowColor = UIColor.black.cgColor
        layer1.shadowOffset = CGSize(width: 0.0, height: 1.0)
        layer1.shadowRadius = 2.0
        layer1.shadowOpacity = 0.1
        layer1.masksToBounds = false
        self.btn_back.layer.cornerRadius = 1
        if CardName.sharedInstance.camefrom == "SIGNIN"
        {
            mobile_number.text = CardName.sharedInstance.save_username
        }
        mobile_number.underlined()
        full_name.underlined()
        email.underlined()
        referral_code.underlined()
        radioButtonController = SSRadioButtonsController(buttons: btn_maleoutlet, btn_femaleoutlet)
        radioButtonController!.delegate = self
        radioButtonController!.shouldLetDeSelect = true
        
        mobile_number.delegate = self
    
        email.delegate = self
   
        full_name.delegate = self
  
        referral_code.delegate = self
        
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(SignupViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
        
        
        
        // Do any additional setup after loading the view.
    }
    @objc func dismissKeyboard()
    {
        view.endEditing(true)
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        
        
        
        if let nextField = textField.superview?.viewWithTag(textField.tag + 1) as? UITextField {
            nextField.becomeFirstResponder()
        } else
        {
            
            textField.resignFirstResponder()
        }
        
        return false
        
        
        
        
    }
    
    
    
    
    func textField(_ textfield: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
         if textfield == mobile_number
         {
        let text = mobile_number.text!
        let newLength = text.characters.count + string.characters.count
        return newLength <= 10
        }
        
        else
         {
            print("text")
         }
        return true
    }
    
    

    @IBAction func btn_signup(_ sender: Any)
    {
        var mobile = mobile_number.text?.count
        var fullname = full_name.text?.count
        var email_id = email.text
        if mobile == 10
        {
            if fullname! >= 4
            {
                if isValidEmail(testStr: email_id ?? "")
                {
                    if clicked_gender != ""
                    {
                        getSalons()
                    }else
                    {
                        str_alert(message: "Select a gender")
                    }
                }
                else{
                    str_alert(message: "Enter Valid Email id")
                }
            }else{
                str_alert(message: "FullName Must be 4 digit")
            }
        }
        else{
            str_alert(message: "Invalid Mobile Number")
        }
    }
    @IBOutlet weak var btn_back: UIButton!
    func didSelectButton(selectedButton: UIButton?)
    {
        clicked_gender = selectedButton?.currentTitle ?? ""
        NSLog("zcjkzckb \(selectedButton?.currentTitle)" )
    }
    @IBOutlet weak var btn_femaleoutlet: SSRadioButton!
    @IBOutlet weak var btn_maleoutlet: SSRadioButton!
    @IBOutlet weak var referral_code: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var full_name: UITextField!
    @IBOutlet weak var mobile_number: UITextField!
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    @IBOutlet weak var overall_view: UIView!
    func getSalons()
    {
      var gender = ""
        var uuid = UIDevice.current.identifierForVendor!.uuidString
        if clicked_gender == "Male"
        {
            gender = "1"
        }else if clicked_gender == "Female"
        {
            gender = "2"
        }
        
        if referral_code.text?.count == 0
        {
          coupan_code = ""
        }else{
            coupan_code = referral_code.text ?? ""
        }
        let parameters: [String: Any] = [
            "FullName": full_name.text!,
            "UserName": mobile_number.text!,
            "PhoneNumber": mobile_number.text!,
            "Gender" :gender,
            "EmailID":email.text!,
            "Password":mobile_number.text!,
            "RoleName":"Customer",
            "CouponCode":coupan_code,
            "UserUniqueID":UserDefaults.standard.string(forKey: "uuid")
        ]
        
        let url = CardName.sharedInstance.signup_url
        print(url)
        
      //let header = [
       //    "Authorization": "bearer xrcVorXyYHzOmvSP1Fpg2uLwYQAFz8dRI8YEqzdUFa7-PumSEX8hNCpTmJjyyVc0ipBZFiU8SShptXtRfZtHiaSQj1ZTnWfH-4z_3UnhhkpkE2EJJ3dgPczRYX--aqeCq0dfsHpHA7fRIML2steqqaFfO_DQxAoTV0FYGUs_wmy6T_ot9ASjfTFVpgVdnU31DBf7rbm-ewT0WbhG1FcxtsbmZQQ8lV6Lbc7j0VNjbYP1qIMHITCTxZ9ScR-nu9vw-PEmTMEaCmogoG5yan2_Cqeyk3T3YTmbvmtyd4GBFrhHHsVApaXaJ2EWB4Ta8LZoj3UgPdbeNQtBZLt3EBT90wqnSaWPjrOrC9sCIZNRdBedoIJe2Oa7CRQnT4wV7By0lI0DYTBUp4YgV9WcS1WOVkM6p3qQoakdBK_2ImK7GDJZqzGhD4fWvk0ZRK7TbbZv",
        //  ]
        print("params\(parameters)")
        Alamofire.request(url, method: .post,parameters: parameters, encoding: JSONEncoding.default)
            .responseJSON { response in
                print("response\(response)")
             let responce_value = response.result.value as! NSDictionary
                let status = responce_value.value(forKey: "Status") as! Int
                let message = responce_value.value(forKey: "Message") as! String
                print(status)
                if status == 0
                {
                    let alert = UIAlertController(title: "Message", message: message, preferredStyle: .alert)
                    let okaction = alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                    self.present(alert,animated: true,completion: nil)
                    
                }
                else if status == 1
                {
                    let alert = UIAlertController(title: "Message", message: message, preferredStyle: .alert)
                    
                    let okaction = alert.addAction(UIAlertAction(title: "OK", style: .default)
                    { (action) in
                        let storuboard = UIStoryboard(name:"Main", bundle: nil)
                        let subclass = storuboard.instantiateViewController(withIdentifier: "Sign_InViewController") as! Sign_InViewController
                        self.present(subclass,animated: true,completion: nil)
                        
                    })
                    
                    
                    self.present(alert,animated: true,completion: nil)

                    
                }
                
        }
        
        
        
    }
    
    @IBAction func btn_back(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let subclass = storyboard.instantiateViewController(withIdentifier: "DashboardViewController") as! UITabBarController
        subclass.selectedIndex = 2
        self.present(subclass,animated: true,completion: nil)
    }
    func str_alert(message:String)
    {
        let alert = UIAlertController(title: "Message", message: message, preferredStyle: .alert)
        let okaction = alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        self.present(alert,animated: true,completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
//extension UITextField {
//
//    // Next step here
//    func underlined(){
//        let border = CALayer()
//        let width = CGFloat(1.0)
//        border.borderColor = UIColor.gray.cgColor
//        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width:  self.frame.size.width, height: self.frame.size.height)
//        border.borderWidth = width
//        self.layer.addSublayer(border)
//        self.layer.masksToBounds = true
//    }
//}
