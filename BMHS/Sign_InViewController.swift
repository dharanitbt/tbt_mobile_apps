//
//  Sign_InViewController.swift
//  BMHS
//
//  Created by TechBT on 20/09/18.
//  Copyright © 2018 TechBT. All rights reserved.
//

import UIKit
import Alamofire

class Sign_InViewController: UIViewController , UITextFieldDelegate {
    var counter = 120
    var timer = Timer()
    override func viewDidLoad() {
        super.viewDidLoad()
        user_number.underlined()
        self.activityindicator.isHidden = true
        otp_view.isHidden = true
        let layer = self.overall_view.layer
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        layer.shadowRadius = 6.0
        layer.shadowOpacity = 0.4
        layer.masksToBounds = false
        self.overall_view.layer.cornerRadius = 5
        let layer1 = self.btn_backoutlet.layer
        layer1.shadowColor = UIColor.black.cgColor
        layer1.shadowOffset = CGSize(width: 0.0, height: 1.0)
        layer1.shadowRadius = 2.0
        layer1.shadowOpacity = 0.1
        layer1.masksToBounds = false
        self.btn_backoutlet.layer.cornerRadius = 1
        let otplayer = self.otp_view.layer
        otplayer.shadowColor = UIColor.black.cgColor
        otplayer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        otplayer.shadowRadius = 2.0
        otplayer.shadowOpacity = 0.1
        otplayer.masksToBounds = false
        self.otp_view.layer.cornerRadius = 5
        otp_textfield.underlined()
         self.addDoneButtonOnKeyboard()
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(Sign_InViewController.hidekeyboard))
        tapGesture.numberOfTapsRequired = 1
        view.addGestureRecognizer(tapGesture)
        
        
        
        user_number.delegate = self
        otp_textfield.delegate = self
        
        
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func btn_back(_ sender: Any) {
        if CardName.sharedInstance.camefrom == "Dashboard"
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let subclass = storyboard.instantiateViewController(withIdentifier: "DashboardViewController") as! UITabBarController
            subclass.selectedIndex = 0
            self.present(subclass,animated: true,completion: nil)
            
        }
        else{
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let subclass = storyboard.instantiateViewController(withIdentifier: "DashboardViewController") as! UITabBarController
        subclass.selectedIndex = 2
        self.present(subclass,animated: true,completion: nil)
        }
    }
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle = UIBarStyle.default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(Sign_InViewController.hidekeyboard))
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.user_number.inputAccessoryView = doneToolbar
        self.otp_textfield.inputAccessoryView = doneToolbar
        
    }
    
    @IBOutlet weak var activityindicator: UIActivityIndicatorView!
    @objc func hidekeyboard()
    {
        self.view.endEditing(true)
    }

  func doneButtonAction()
    {
        self.user_number.resignFirstResponder()
    }
    @IBAction func btn_signin(_ sender: Any) {
        //openAlertView()
        view.endEditing(true)
        let  mobile = user_number.text
        if user_number.text?.count == 10
            
            
        {
            isValidPhoneNumber(phoneStr: mobile ?? "")
            self.activityindicator.isHidden = false
            self.activityindicator.startAnimating()
            self.getstatus()
        }
        else
        {
            let alert = UIAlertController(title: "Message", message: "Mobile Number invalid ", preferredStyle: .alert)
            let okaction = alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alert,animated: true,completion: nil)
        }
        
    }
    
    
    
    func isValidPhoneNumber(phoneStr:String) -> Bool
    {
        
        let phoneRegEx = "^[0-9]{9}$"
        let phoneTest = NSPredicate(format:"SELF MATCHES %@", phoneRegEx)
        return phoneTest.evaluate(with: phoneStr)
    }
    
    
    @objc func timerAction() {
        counter -= 1
        if counter == 0
        {
            timer.invalidate()
            self.otp_view.isHidden = true
            let alert = UIAlertController(title: "Message", message: "Requested Timeout! Please try Again", preferredStyle: .alert)
            let okaction = alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
            self.present(alert,animated: true,completion: nil)
        }
        otp_secondsLbl.text = "\(counter) SECONDS"
        
    } 
    @IBOutlet weak var user_number: UITextField!
    @IBOutlet weak var overall_view: UIView!
    
    @IBOutlet weak var otp_textfield: UITextField!
    @IBOutlet weak var otp_secondsLbl: UILabel!
    @IBOutlet weak var otp_view: UIView!
    @IBOutlet weak var btn_signinoutlet: UIButton!
    @IBOutlet weak var btn_backoutlet: UIButton!
   var textField: UITextField?
    
    @IBAction func btn_otpback(_ sender: Any) {
        timer.invalidate()
        otp_view.isHidden = true
        
    }
    
    @IBAction func btn_verifyotp(_ sender: Any) {
        var otp_number = otp_textfield.text
        if otp_number?.count == 6
        {
            timer.invalidate()
            self.otp_view.isHidden = true
            verifyotp()
        }
        else
        {
            let alert = UIAlertController(title: "Message", message: "Enter Valid OTP!", preferredStyle: .alert)
            let okaction = alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
            self.present(alert,animated: true,completion: nil)
        }
      
        
    }
    func verifyotp()
    {
        let username = user_number.text
        let parameters: [String: Any] = [
            
                    "UserName": username,
                        "OtpCode": otp_textfield.text
        ]
        
        let url = CardName.sharedInstance.verify_otp
        
        print(url)
        
        Alamofire.request(url, method: .post,parameters: parameters, encoding: JSONEncoding.default)
            .responseJSON { response in
                print("VERIFY OTP RESPONSE \(response)")
              let responce_value = response.result.value as! NSDictionary
                if response.error == nil
                {
                   
                    let containsKey = responce_value.allKeys.contains { return $0 as? String == "Status" }
                    if containsKey {
                        print("Invalid OTP")
                        let alert = UIAlertController(title: "Alert", message: "Invalid OTP!", preferredStyle: .alert)
                        let okaction = alert.addAction(UIAlertAction(title: "Ok", style: .default)
                        { (action) in
                            self.otp_textfield.text = ""
                         })
                        self.present(alert,animated: true,completion: nil)
                    }
                    
                else
                    {
                let access_token = responce_value.value(forKey: "access_token") as! String
                let role = responce_value.value(forKey: "role") as! String
                let token_type = responce_value.value(forKey: "token_type") as! String
                CardName.sharedInstance.token_type = token_type
                CardName.sharedInstance.user_token = access_token
                
                UserDefaults.standard.set(token_type, forKey: "token")
                UserDefaults.standard.set(access_token, forKey: "Authorizent_token")
                UserDefaults.standard.set(true, forKey: "isLogged")
                UserDefaults.standard.set(username, forKey: "username")
                UserDefaults.standard.set("user_token", forKey: "tokentype")
                //CardName.sharedInstance.email_id = responce_value.value(forKey: "EmailID ") as! String
                if role == "owner"
                {
                    let alert = UIAlertController(title: "Message", message: "This  Number is already registered with Bussiness App!", preferredStyle: .alert)
                    let okaction = alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
                    self.present(alert,animated: true,completion: nil)
                }else{
                    if CardName.sharedInstance.comming_from == "Checkout"
                    {
                    let stotyboard = UIStoryboard(name: "Main", bundle: nil)
                    let subclass = stotyboard.instantiateViewController(withIdentifier: "Checkout_ViewController") as! Checkout_ViewController
                    self.present(subclass,animated: true,completion: nil)
                    }
                    else
                    {
                        let stotyboard = UIStoryboard(name: "Main", bundle: nil)
                        let subclass = stotyboard.instantiateViewController(withIdentifier: "DashboardViewController") as! UITabBarController
                        self.present(subclass,animated: true,completion: nil)
                    }
                    
                }
                    }
//                    }
//                    
//                    else
//                    {
//                        let alert = UIAlertController(title: "Message", message: "Invalid OTP! Try Again", preferredStyle: .alert)
//                        let okaction = alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
//                        self.present(alert,animated: true,completion: nil)
//                    }
                }
                    
                else
                {
                    let alert = UIAlertController(title: "Message", message: "Invalid OTP", preferredStyle: .alert)
                    let okaction = alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
                    self.present(alert,animated: true,completion: nil)
                }
        }
       // }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == user_number
        {
            let text = user_number.text!
            let newLength = text.characters.count + string.characters.count
            return newLength <= 10
        }
            
        else if textField == otp_textfield
        {
            let text = otp_textfield.text!
            let newLength = text.characters.count + string.characters.count
            return newLength <= 6
            
        }
        
        return true
    }
    
    func getstatus()
    {
        let username = user_number.text
        let parameters: [String: Any] = [
                    "UserName": username
        ]
        
        let url = CardName.sharedInstance.generate_otp
        print(url)
       Alamofire.request(url, method: .post,parameters: parameters, encoding: JSONEncoding.default)
            .responseJSON { response in
                print("GET STATUS RESPONSE\(response)")
                let responce_value = response.result.value as! NSDictionary
                let status = responce_value.value(forKey:"Status") as! Bool
                if status
                {
                    self.activityindicator.isHidden = true
                    self.activityindicator.stopAnimating()
                    self.otp_view.isHidden = false
                    self.timer.invalidate()
                    self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.timerAction), userInfo: nil, repeats: true)
                }
                else
                {
                    CardName.sharedInstance.camefrom = "SIGNIN"
                    CardName.sharedInstance.save_username = username!
                    let stotyboard = UIStoryboard(name: "Main", bundle: nil)
                    let subclass = stotyboard.instantiateViewController(withIdentifier: "SignupViewController") as! SignupViewController
                    self.present(subclass,animated: true,completion: nil)
                    
                    
                    
                    
                    
                    //let alert = UIAlertController(title: "Message", message: "Error Try Again!", preferredStyle: .alert)
                    //let okaction = alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
                    //self.present(alert,animated: true,completion: nil)
                }
                
        }
        
        
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
  
}
extension UITextField {
    
    // Next step here
    func underlined(){
        let border = CALayer()
        let width = CGFloat(1.0)
        border.borderColor = UIColor(red: 243/255, green: 160/255, blue: 27/255, alpha: 1.0).cgColor
        
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width:  self.frame.size.width, height: self.frame.size.height)
        border.borderWidth = width
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }
}
