//
//  Aboutus_ViewController.swift
//  BMHS
//
//  Created by TechBT on 10/10/18.
//  Copyright © 2018 TechBT. All rights reserved.
//

import UIKit

class Aboutus_ViewController: UIViewController {
    
    @IBOutlet weak var back_img: UIImageView!
    @IBOutlet weak var title_lbl: UILabel!
    @objc func tapFunction(sender:UITapGestureRecognizer) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let subclass = storyboard.instantiateViewController(withIdentifier: "DashboardViewController") as! UITabBarController
        subclass.selectedIndex = 2
        self.present(subclass,animated: true,completion: nil)
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        let screenSize = UIScreen.main.bounds
        let screenWidth = screenSize.width
        _ = screenSize.height
        
        print("screenwidth\(screenWidth)")
        if screenWidth == 320
        {
        title_lbl.font = UIFont(name: "ProximaNova-SemiBold", size: 15.0)
        }
        let tap = UITapGestureRecognizer(target: self, action: #selector(Aboutus_ViewController.tapFunction))
        back_img.isUserInteractionEnabled = true
        
        back_img.addGestureRecognizer(tap)
        var version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
version_Lbl.text = "Version \(version!)"
        // Do any additional setup after loading the view.
    }
    @IBOutlet weak var version_Lbl : UILabel!

       // print(version)


    @IBAction func btn_phonr(_ sender: Any) {
        if let url = URL(string: "tel://04224393889"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    @IBAction func btn_privacypolicy(_ sender: Any) {
        guard let url = URL(string: "http://bmhstylist.com/Account/PrivacyPolicy") else { return }
        UIApplication.shared.open(url)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
}
