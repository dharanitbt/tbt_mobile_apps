//
//  Api .swift
//  BMHS
//
//  Created by TechBT on 14/08/18.
//  Copyright © 2018 TechBT. All rights reserved.
//

import Foundation
class CardName {
    
    static var sharedInstance = CardName()
    private init() {
        salonListing_url = demo_Url + "api/Saloon/Info"
        LocationListing_Url = demo_Url + "api/Saloon/GetTopCities"
        infoDetails_url = demo_Url + "api/Saloon/InfoDetail"
        info_serviceUrl = demo_Url + "api/Saloon/GetServiceInfo"
        service_timeslots = demo_Url + "api/saloon/GetTimeSlots"
        Active_Booking_Url = demo_Url + "api/Order/SaloonOrderPendingSummary"
        BookingHistroy_url = demo_Url + "api/Order/SaloonOrderHistorySummary"
        signup_url = demo_Url + "api/Account/Register"
        generate_otp = demo_Url + "api/Account/GenerateOTP"
        verify_otp = demo_Url + "api/Account/VerifyOTP"
        dashboardoffers_url = demo_Url + "api/Account/DashBoardOffers"
        profile_url = demo_Url + "api/Account/GetUserDetails"
        feedback_url = demo_Url + "api/Account/SaveFeedBack"
        exclusiveoffers_url = demo_Url + "api/Saloon/GetExclusiveOffers"
        userlocation_url = demo_Url + "api/Account/UserPreferredLocation"
        facility_url = demo_Url + "api/Saloon/FacilityInfo"
        preferred_location = demo_Url + "api/Saloon/CityInfo"
        getpricedetails = demo_Url + "api/Saloon/GetPriceDetails"
        payment_url = demo_Url + "api/Account/InitiatePayment"
        onlinepayment_url = demo_Url + "api/Account/InitiateOnlinePayment"
        instamojo_Url = "https://test.instamojo.com/" + "api/1.1/payment-requests/"
        savepayment_Url =  demo_Url + "api/Order/SavePaymentTransactionDetail"
        order_save_url = demo_Url + "api/Order/Save"
        device_register_url = demo_Url + "api/Account/RegisterDevice"
        guest_token_url = demo_Url + "token"
        validate_token_url = demo_Url + "api/Account/ValidateToken"
        trendingsalon_url = demo_Url + "api/Saloon/GetRecommendedSaloons"
        savepreferenceLocation = demo_Url + "api/Account/SavePreferredLocation"
        saveusercity = demo_Url + "api/Account/SaveUserCity"
        mobileBanner_url = demo_Url + "api/Saloon/GetMobileDashboardBanner"
        instamojoApikey = "23f0d06f71ab6dada3a96e85f4759281"
        instamojoApitestkey = "5b93672cd798d7d7eb89380bdd032dad"
        instamojoToken = "d45bace7de88a80c30fd1b2e8a08f86f"
        instamojoTokentest = "0b98e603a06efa0695ed524a7bd33cc1"
        notification_url = demo_Url + "api/Account/PushNotification"
        //print("salonListing_Url\(salonListing_url)")
    }
    var clicked_id = 0
    var instamojoBase_url = "https://www.instamojo.com/"
    //var demo_Url = "http://bmhstylist.com/"
    var demo_Url = "http://101.53.152.203/"
    //"http://101.53.152.203/"
    //var instamojoBasetest_url = "https://test.instamojo.com/"
    var getpricedetails = ""
    var onlinepayment_url = ""
    var instamojo_Url = ""
    var dashboardoffers_url = ""
    var userlocation_url = ""
    var exclusiveoffers_url = ""
    var facility_url = ""
    var salonListing_url = ""
    var verify_otp = ""
    var comming_from = ""
    var LocationListing_Url = ""
    var infoDetails_url = ""
    var feedback_url = ""
    var Active_Booking_Url = ""
    var profile_url = ""
    var user_token = ""
    var token_type = ""
    var info_serviceUrl = ""
    var service_timeslots = ""
    var BookingHistroy_url = ""
    var generate_otp = ""
    var signup_url = ""
    var order_ref = ""
    var DateArray = [String]()
    var TimeArray = [String]()
    var salon_service = [String:Any]()
    var selected_servicename = [String]()
    var selected_indexpath = [Int]()
    var selected_service_count = [Int]()
    var selected_serviceamount = [String]()
    var selected_serviceid = [Int]()
    var total_time = ""
    var total_amount = ""
    var gender_type = ""
    var payment_mode = 0
    var payment_type = 0
    var isonlinepayment = false
    var today_day = ""
    var type_array = [String]()
    var name_array = [String]()
    var price_Array = [String]()
    var responce_array = [NSDictionary]()
    var booking_dictionary = NSDictionary()
    var stepper_amount = 0.0
    var clicked_rating = ""
    var clicked_sort = "distance"
    var clicked_genderArray = [Int]()
    var clicked_facility = [String]()
    var service_selected_time = [String]()
    var isverified = "true"
    var preferred_location = ""
    var iscurrentLocation = false
    var today_day_int = 0
    var service_total_Amount = ""
    var payment_url = ""
    var endslot_id = ""
    var startslot_id = ""
    var order_save_url = ""
    var device_register_url = ""
    var guest_token_url = ""
    var validate_token_url = ""
    var savepreferenceLocation = ""
    var saveusercity = ""
    var trendingsalon_url = ""
    var mobileBanner_url = ""
    var isaddservice_clicked = false
    var ismaleselected = false
    var isfemaleselected = false
    var isgenderselected = false
    var alphabeticalselected = false
    var contactarray_count = 0
    var contact_Array = [String]()
    var instamojoApikey = ""
    var instamojoApitestkey = ""
    var instamojoToken = ""
    var instamojoTokentest = ""
    var email_id = ""
    var savepayment_Url = ""
    var price_dictionary = NSDictionary()
    var SalonTransaction_id = ""
    var Paymentreference_id = ""
    var order_num = ""
    var camefrom = ""
    var save_username = ""
    var notification_url = ""
}
