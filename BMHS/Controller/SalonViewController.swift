//
//  SalonViewController.swift
//  BMHS
//
//  Created by TechBT on 09/08/18.
//  Copyright © 2018 TechBT. All rights reserved.
//

import UIKit
import Alamofire 
import GoogleMaps
import GooglePlaces
class SalonViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,CLLocationManagerDelegate,UISearchBarDelegate,UITextFieldDelegate,GMSMapViewDelegate {
    
    var salonname_Array:[String] = [""]
    var Locationname_Array:[String] = [""]
    var salontype_Array:[String] = [""]
    var salonrating_Array = [Double]()
    var saloncheckins_Array:[Int] = []
    var salonDistance_Array = [Double]()
    var salonLogo_Array:[String] = [""]
    var clicked_id_Array:[Int] = []
    var gender_array = [Int]()
    var salon_rating = ""
    var salon_latArray = [Double]()
    var salon_logArray = [Double]()
    var isVerified = ""
    var facility_array = [String]()
    var count = 0
    var user_latitute = ""
    var search_text = ""
    var user_longitute = ""
    var verified_array = [Bool]()
    var locationManager = CLLocationManager()
    
    @IBOutlet weak var activityindicator: UIActivityIndicatorView!
    
    @IBOutlet weak var nosalon_view: UIView!
    @IBOutlet weak var btn_nearbyout: UIButton!
    @IBOutlet weak var btn_recommendedsalon: UIButton!

    @IBOutlet weak var searchbar: UISearchBar!
    
    @IBOutlet weak var mapview: GMSMapView!
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar)
    {
        print("end editing")
    }
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar)
    {
        print("start editing")
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        print(salonname_Array.count)
        
           return salonname_Array.count
    }
    
  
    @IBOutlet weak var missing_salon_view: UIView!
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        print("Clicked")
        CardName.sharedInstance.camefrom = "Salonpage"
        if (self.verified_array[indexPath.row])
        {
        UserDefaults.standard.set(self.clicked_id_Array[indexPath.row], forKey: "clicked_id")
        UserDefaults.standard.set(self.salonname_Array[indexPath.row], forKey: "Salon_Name")
        UserDefaults.standard.set(self.salonLogo_Array[indexPath.row], forKey: "Salon_Logo")
        UserDefaults.standard.set(self.Locationname_Array[indexPath.row], forKey: "Salon_Location")
        CardName.sharedInstance.gender_type = salontype_Array[indexPath.row] as! String
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let subclass = storyboard.instantiateViewController(withIdentifier: "SalonDetails_ViewController") as! UITabBarController
        subclass.selectedIndex = 1
        self.present(subclass,animated: true,completion: nil)
        }
        
        else{
            UserDefaults.standard.set(self.clicked_id_Array[indexPath.row], forKey: "clicked_id")
            UserDefaults.standard.set(self.salonname_Array[indexPath.row], forKey: "Salon_Name")
            UserDefaults.standard.set(self.salonLogo_Array[indexPath.row], forKey: "Salon_Logo")
            UserDefaults.standard.set(self.Locationname_Array[indexPath.row], forKey: "Salon_Location")
            CardName.sharedInstance.gender_type = salontype_Array[indexPath.row] as! String
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let subclass = storyboard.instantiateViewController(withIdentifier: "Infosalon_ViewController") as! Infosalon_ViewController
            //subclass.selectedIndex = 0
            self.present(subclass,animated: true,completion: nil)
        }
        
    }
   
    @IBAction func cancel(_ sender: Any)
    {
    }
    @IBAction func btn_nearby(_ sender: Any) {
        view.endEditing(true)
        if mapview.isHidden == true
        {
            //btn_nearbyout.setTitle("ListView", for: .normal)
            let fullString = "  Listview"
            print("here map")
            if UserDefaults.standard.bool(forKey: "iscurrentlocation")
            {
               determineMyCurrentLocation()
            }
            // create our NSTextAttachment
            let image1Attachment:NSTextAttachment = NSTextAttachment()
            image1Attachment.image = UIImage(named: "listview")
            image1Attachment.bounds = CGRect(x: 1, y: -2, width: 15, height: 15)
            
            // wrap the attachment in its own attributed string so we can append it
            let image1String:NSAttributedString = NSAttributedString(attachment: image1Attachment)
            let myString:NSMutableAttributedString = NSMutableAttributedString(attributedString: image1String)
            
            let atr_rating:NSMutableAttributedString = NSMutableAttributedString(string: fullString)
            
            // add the NSTextAttachment wrapper to our full string, then add some more text.
            myString.append(atr_rating)
            btn_nearbyout?.setAttributedTitle(myString, for: UIControlState.normal)
//            btn_nearbyout.setTitleColor(.lightGray, for: .normal)
//            btn_nearbyout.setTitleColor(.lightGray, for: .highlighted)
            self.mapview.isHidden = false
        }
        else
        {
            //btn_nearbyout.setTitle("Nearby", for: .normal)
            let fullString = "  Nearby"
            
            // create our NSTextAttachment
            let image1Attachment:NSTextAttachment = NSTextAttachment()
            image1Attachment.image = UIImage(named: "placeholder")
            image1Attachment.bounds = CGRect(x: 1, y: -2, width: 15, height: 15)
            
            // wrap the attachment in its own attributed string so we can append it
            let image1String:NSAttributedString = NSAttributedString(attachment: image1Attachment)
            let myString:NSMutableAttributedString = NSMutableAttributedString(attributedString: image1String)
            
            let atr_rating:NSMutableAttributedString = NSMutableAttributedString(string: fullString)
            
            // add the NSTextAttachment wrapper to our full string, then add some more text.
            myString.append(atr_rating)
           // btn_nearbyout.setTitleColor(.lightGray, for: .normal)
//            btn_nearbyout.setTitleColor(.lightGray, for: .highlighted)
            btn_nearbyout?.setAttributedTitle(myString, for: UIControlState.normal)
            
            self.mapview.isHidden = true
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomCellOne", for: indexPath) as! SalonTableViewCell
        print("count_verified\(self.verified_array.count)")
        
        
        if(self.verified_array.count != nil  && self.verified_array.count != 0)
        {
        
        if self.verified_array[indexPath.row]
        {
            let fullString = NSMutableAttributedString(string: self.salonname_Array[indexPath.row])
            
            // create our NSTextAttachment
            let image1Attachment = NSTextAttachment()
            image1Attachment.image = UIImage(named: "tick1.png")
            image1Attachment.bounds = CGRect(x: 5, y: -2, width: 13, height: 13)
            
            // wrap the attachment in its own attributed string so we can append it
            let image1String = NSAttributedString(attachment: image1Attachment)
            
            // add the NSTextAttachment wrapper to our full string, then add some more text.
            fullString.append(image1String)
            
            // draw the result in a label
           // yourLabel.attributedText = fullString
              cell.salon_Name.attributedText = fullString
        }
        else
        {
            cell.salon_Name.text = self.salonname_Array[indexPath.row] as! String
        }
      
        if salonLogo_Array[indexPath.row] as! String != nil && salonLogo_Array[indexPath.row] as! String != ""
        {
    var salon_img = salonLogo_Array[indexPath.row] as! String
            print(salonLogo_Array[indexPath.row] as! String)
        let url = URL(string: salon_img)
            if url != nil{
                URLSession.shared.dataTask(with: NSURL(string: salon_img)! as URL, completionHandler: { (data, response, error) -> Void in
                    
                    if error != nil {
                        print("dff\(error)")
                        return
                    }
                    DispatchQueue.main.async(execute: { () -> Void in
                        let image = UIImage(data: data!)
                        cell.salon_img.image = image
                    })
                    
                }).resume()
            }
            
        }
       cell.salon_Location.text = "\(Locationname_Array[indexPath.row]) | \(salontype_Array[indexPath.row])"
            
        var Str_rating = String(salonrating_Array[indexPath.row] as! Double)
          if Str_rating == "0.0"
          {
            Str_rating = "NEW"
            }
        let attachment:NSTextAttachment = NSTextAttachment()
        attachment.image = UIImage(named: "star")
        attachment.bounds = CGRect(x: 0, y: -2, width: 13, height: 13)
        let attachmentString:NSAttributedString = NSAttributedString(attachment: attachment)
        let myString:NSMutableAttributedString = NSMutableAttributedString(attributedString: attachmentString)
        let atr_rating:NSMutableAttributedString = NSMutableAttributedString(string: " " + Str_rating)
        
        myString.append(atr_rating)
        
        
     //  self.attributedText = myString
        cell.salon_rating.attributedText = myString
        var str_checkins = String(saloncheckins_Array[indexPath.row] as! Int)
        let checkin_attachment:NSTextAttachment = NSTextAttachment()
        checkin_attachment.image = UIImage(named: "checkins")
        checkin_attachment.bounds = CGRect(x: 0, y: -2, width: 13, height: 13)
        let checkin_attachmentString:NSAttributedString = NSAttributedString(attachment: checkin_attachment)
        let checkin_myString:NSMutableAttributedString = NSMutableAttributedString(attributedString: checkin_attachmentString)
        let checkin_atr_String:NSMutableAttributedString = NSMutableAttributedString(string: " " + str_checkins)
        let atr_text:NSMutableAttributedString = NSMutableAttributedString(string: " Check ins")
        checkin_myString.append(checkin_atr_String)
        checkin_myString.append(atr_text)
        cell.salon_checkins.attributedText = checkin_myString
       
            let dou_distance:Double = salonDistance_Array[indexPath.row]
            
        if dou_distance != nil && dou_distance >= 0.0
        {
            let str_distance:String = String(format:"%.1f", dou_distance)
            let checkin_attachment:NSTextAttachment = NSTextAttachment()
            checkin_attachment.image = UIImage(named: "nearby")
            checkin_attachment.bounds = CGRect(x: 0, y: -2, width: 13, height: 13)
            let checkin_attachmentString:NSAttributedString = NSAttributedString(attachment: checkin_attachment)
            let checkin_myString:NSMutableAttributedString = NSMutableAttributedString(attributedString: checkin_attachmentString)
            let checkin_atr_String:NSMutableAttributedString = NSMutableAttributedString(string: " " + str_distance)
            let atr_kmtext:NSMutableAttributedString = NSMutableAttributedString(string: "km")
            checkin_myString.append(checkin_atr_String)
            checkin_myString.append(atr_kmtext)
            cell.salon_distance.attributedText = checkin_myString
        }
//        print(salonDistance_Array.count)
//        let position = indexPath.row
    }
     
        return cell
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print("current location")
        let userLocation:CLLocation = locations[0] as CLLocation
        var position = CLLocationCoordinate2DMake(userLocation.coordinate.latitude ?? 0.0,userLocation.coordinate.longitude ?? 0.0)
        var gmsposititon = GMSCameraPosition(target: position, zoom: 15.0, bearing: 50, viewingAngle: 360)
        let circleCenter = CLLocationCoordinate2D(latitude: userLocation.coordinate.latitude, longitude: userLocation.coordinate.longitude)
        let circ = GMSCircle(position: circleCenter, radius: 500)
        circ.fillColor = UIColor.lightGray
        circ.map = self.mapview
        self.mapview.camera = gmsposititon
        // Call stopUpdatingLocation() to stop listening for location updates,
        // other wise this function will be called every time when user location changes.
        
        // manager.stopUpdatingLocation()
        locationManager.stopUpdatingLocation()
       
        
    }
    func determineMyCurrentLocation()
    {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
            //locationManager.startUpdatingHeading()
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        self.tableview.delegate = self
        self.tableview.dataSource = self
        self.activityindicator.isHidden = false
        self.activityindicator.startAnimating()
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                print("No access")
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
                // self.locationManager.delegate = self
                // self.locationManager.requestAlwaysAuthorization()
                self.locationManager.requestWhenInUseAuthorization()
                self.locationManager.startUpdatingLocation()
                self.mapview?.isMyLocationEnabled = true
                
            }
        } else {
            print("Location services are not enabled")
        }
         getSalons()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 104
    }
    
    
 
   
    func searchBar (_ searchBar: UISearchBar, textDidChange searchText: String) {
        print("text changed")
        if searchText == ""
        {
        search_text = searchText
          getSalons()
            
            self.tableview.isHidden = false
           self.nosalon_view.isHidden = true
       }else
        {
            self.activityindicator.isHidden = false
            self.activityindicator.startAnimating()
            print("searchtext\(searchText)")
            search_text = searchText
            DispatchQueue.main.async {
                
                self.getSalons()
            }
           
//            if salonname_Array.count == 0
//            {
//                self.tableview.isHidden = true
//                self.nosalon_view.isHidden = false
//                searchBar.text = ""
//            }
            
            
        }
     
    }
    
    @IBAction func btn_send(_ sender: Any)
    {
        view.endEditing(true)
        activityindicator.startAnimating()

        var missingsalon_name = txt_missingsalon.text
        if missingsalon_name?.count == 0
        {
            activityindicator.stopAnimating()

            let alert = UIAlertController(title: "Message", message: "Enter Valid Missing Salon Name", preferredStyle: .alert)
            let okaction = alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
            self.present(alert,animated: true,completion: nil)
        }else{
            getSalons(feedback: txt_missingsalon.text ?? "")
            
        }
    }
    @IBAction func btn_Cancel(_ sender: Any)
    {
        activityindicator.stopAnimating()
        
        self.missing_salon_view.isHidden = true
        view.endEditing(true)
        self.nosalon_view.isHidden = false
        self.tableview.isHidden = true

        
        
        
    }
    @IBOutlet weak var txt_missingsalon: UITextField!
    @IBAction func btn_recommendsalon(_ sender: Any) {
        if UserDefaults.standard.bool(forKey: "isLogged") != nil{
            var usertype = UserDefaults.standard.bool(forKey: "isLogged")
            if usertype
            {
                self.searchbar.text = ""
                
                self.nosalon_view.isHidden = true
                self.tableview.isHidden = false
                self.missing_salon_view.isHidden = false
            }
            else
            {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let subclass = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! UITabBarController
                self.present(subclass,animated: true,completion: nil)
            }
        }
      
        
    }
    override func viewDidLoad()
    {
        super.viewDidLoad()
        searchbar.delegate = self
         btn_nearbyout.frame = CGRect(x: self.btn_nearbyout.frame.origin.x, y: self.searchbar.frame.origin.y + 11, width: self.btn_nearbyout.frame.width, height: 34)
        searchbar.layer.borderWidth = 1
        let mycolor =  #colorLiteral(red: 0.8063836694, green: 0.1898734868, blue: 0.1415854096, alpha: 1)
        searchbar.layer.borderColor = mycolor.cgColor
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).font = UIFont.systemFont(ofSize: 12)
        self.mapview.delegate = self
        
        btn_recommendedsalon.titleLabel?.adjustsFontSizeToFitWidth = true;
        
          //searchbar.layer.shadowColor = UIColor.clear.cgColor
         // searchbar.layer.shadowOffset = 0.0
        //while typing, change the background color
        //UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).defaultTextAttributes = [NSAttributedString.Key.foregroundColor.rawValue: UIColor.white]
        
        txt_missingsalon.delegate = self
        btn_nearbyout.layer.cornerRadius = 5
        //btn_nearbyout.
        if mapview.isHidden == true
        {
            //btn_nearbyout.setTitle("Nearby", for: .normal)
            let fullString = "  Nearby"
            
            // create our NSTextAttachment
            let image1Attachment:NSTextAttachment = NSTextAttachment()
            image1Attachment.image = UIImage(named: "placeholder")
            image1Attachment.bounds = CGRect(x: 1, y: -2, width: 15, height: 15)
            
            // wrap the attachment in its own attributed string so we can append it
            let image1String:NSAttributedString = NSAttributedString(attachment: image1Attachment)
            let myString:NSMutableAttributedString = NSMutableAttributedString(attributedString: image1String)
            
            let atr_rating:NSMutableAttributedString = NSMutableAttributedString(string: fullString)
            
            // add the NSTextAttachment wrapper to our full string, then add some more text.
            myString.append(atr_rating)
            btn_nearbyout?.setAttributedTitle(myString, for: UIControlState.normal)
            // self.mapview.isHidden = false
        }else{
            btn_nearbyout.setTitle("Listview", for: .normal)
            
            
            let fullString = "  Listview"
            
            // create our NSTextAttachment
            let image1Attachment:NSTextAttachment = NSTextAttachment()
            image1Attachment.image = UIImage(named: "listview")
            image1Attachment.bounds = CGRect(x: 1, y: -2, width: 15, height: 15)
            
            // wrap the attachment in its own attributed string so we can append it
            let image1String:NSAttributedString = NSAttributedString(attachment: image1Attachment)
            let myString:NSMutableAttributedString = NSMutableAttributedString(attributedString: image1String)
            
            let atr_rating:NSMutableAttributedString = NSMutableAttributedString(string: fullString)
            
            // add the NSTextAttachment wrapper to our full string, then add some more text.
            myString.append(atr_rating)
            btn_nearbyout?.setAttributedTitle(myString, for: UIControlState.normal)
            
            
            //x   self.mapview.isHidden = true
        }
        //btn_nearbyout.frame.size = CGSize(width: searchbar.frame.width - 206 , height: searchbar.frame.height - 26)
     tableview.register(UINib(nibName: "SalonTableViewCell", bundle: nil), forCellReuseIdentifier: "CustomCellOne")
      
        print("salonListing\(CardName.sharedInstance.salonListing_url)")
    
        let layer = self.missing_salon_view.layer
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        layer.shadowRadius = 6.0
        layer.shadowOpacity = 0.4
        layer.masksToBounds = false
        self.missing_salon_view.layer.cornerRadius = 5
        self.txt_missingsalon.underlined()
        
       
        // Do any additional setup after loading the view.
    }
    @IBAction func btn_filter(_ sender: Any)
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let subclass = storyboard.instantiateViewController(withIdentifier: "Filters_ViewController") as! Filters_ViewController
        self.present(subclass,animated: true,completion: nil)
    }
    @IBAction func btn_saloncity(_ sender: Any)
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let subclass = storyboard.instantiateViewController(withIdentifier: "demoViewController") as! demoViewController
        self.present(subclass,animated: true,completion: nil)
    }
   

    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
         print("Marker Tapped")
         print("title::\(marker.title)")
         print("Data::\(marker.userData)")
         let markerdata = marker.userData as! NSDictionary
         print("Marker Data \(markerdata)")
        let id = markerdata.value(forKey: "Id") as! Int
        let location = markerdata.value(forKey: "Location") as! String
        let Logo = markerdata.value(forKey: "Logo") as! String
        let verified = markerdata.value(forKey: "Verified") as! Bool
        let gender = markerdata.value(forKey: "Gender") as! String
        if verified
        {
            UserDefaults.standard.set(id, forKey: "clicked_id")
            UserDefaults.standard.set(marker.title, forKey: "Salon_Name")
            UserDefaults.standard.set(Logo, forKey: "Salon_Logo")
            UserDefaults.standard.set(location, forKey: "Salon_Location")
            CardName.sharedInstance.gender_type = gender as! String
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let subclass = storyboard.instantiateViewController(withIdentifier: "SalonDetails_ViewController") as! UITabBarController
            subclass.selectedIndex = 1
            self.present(subclass,animated: true,completion: nil)
        }
//
     else{
//
        UserDefaults.standard.set(id, forKey: "clicked_id")
        UserDefaults.standard.set(marker.title, forKey: "Salon_Name")
        UserDefaults.standard.set(Logo, forKey: "Salon_Logo")
        UserDefaults.standard.set(location, forKey: "Salon_Location")
        CardName.sharedInstance.gender_type = gender as! String
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let subclass = storyboard.instantiateViewController(withIdentifier: "Infosalon_ViewController") as! Infosalon_ViewController
            // subclass.selectedIndex = 0
            self.present(subclass,animated: true,completion: nil)
        }
        
    }
    
    func getSalons(feedback:String)
    {
        activityindicator.stopAnimating()
        var str_feedback = feedback
        str_feedback = str_feedback.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) ?? ""
        let url = "\(CardName.sharedInstance.feedback_url)?feedBack=\(str_feedback)"
        print(url)
        var type = UserDefaults.standard.string(forKey: "token")!
        var token = UserDefaults.standard.string(forKey: "Authorizent_token")!
        let header = [
            "Authorization": "\(type) \(token)",
        ]
        print("headers\(header)")
        Alamofire.request(url, method: .get,encoding: JSONEncoding.default,headers : header)
            .responseJSON { response in
                print("response\(response)")
                let responce_value = response.result.value as! NSDictionary
                let status = responce_value.value(forKey: "Status") as! Int
                if status == 1
                {
                    self.missing_salon_view.isHidden = true
                     //self.feedback_view.isHidden = true
                    //self.txt_feedback.text = ""
                    self.getSalons()
                    self.txt_missingsalon.text = ""
                }else
                {
                    let alert = UIAlertController(title: "Message", message: "Something went wrong! Please Try Again", preferredStyle: .alert)
                    let okaction = alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
                    self.present(alert,animated: true,completion: nil)
                    self.missing_salon_view.isHidden = true
                }
        }
        
        
        
        
        
    }
    
    func getSalons()
    {
        salonname_Array.removeAll(keepingCapacity: false)
        salontype_Array.removeAll(keepingCapacity: false)
        salonrating_Array.removeAll(keepingCapacity: false)
        saloncheckins_Array.removeAll(keepingCapacity: false)
        salonDistance_Array.removeAll(keepingCapacity: false)
        Locationname_Array.removeAll(keepingCapacity: false)
        salonLogo_Array.removeAll(keepingCapacity: false)
        facility_array.removeAll(keepingCapacity: false)
        clicked_id_Array.removeAll(keepingCapacity: false)
        verified_array.removeAll(keepingCapacity: false)
        if CardName.sharedInstance.clicked_genderArray.count == 0
        {
            gender_array = [1,2,3]
        }
        else
        {
            self.gender_array = CardName.sharedInstance.clicked_genderArray
        }
        if CardName.sharedInstance.clicked_rating == ""
        {
            self.salon_rating = ""
        }else{
            self.salon_rating = CardName.sharedInstance.clicked_rating
        }
        if CardName.sharedInstance.isverified == ""
        {
            self.isVerified = "true"
        }else{
            self.isVerified = CardName.sharedInstance.isverified
        }
        if CardName.sharedInstance.clicked_facility.count == 0
        {
            self.facility_array.append("")
        }
        else
        {
            self.facility_array = CardName.sharedInstance.clicked_facility
        }
      
     
       
        if UserDefaults.standard.bool(forKey: "iscurrentlocation")
        {
            user_latitute = UserDefaults.standard.string(forKey: "current_latitute") ?? ""
            user_longitute = UserDefaults.standard.string(forKey: "current_longitute") ?? ""
        }
        else{
            user_longitute = ""
             user_latitute = ""
        }
        var id = 0
        if UserDefaults.standard.bool(forKey: "iscurrentlocation")
        {
            id = 0
        }else{
           id = UserDefaults.standard.integer(forKey: "city_id") as! Int
        }
        if search_text == ""
        {
            search_text = "a"
        }
        
        let parameters: [String: Any] = [
            "SaloonName" : search_text,
            
            "Area": [
                    "LocationID" : id,
                    "Latitude": user_latitute,
                    "Longitude" : user_longitute,
                    "Radius" : "50"
                ],
                "Gender" : gender_array,
                "UserRating" : salon_rating,
                "Facility" : self.facility_array,
                "IsVerified" : isVerified,
                "OfferTypeID": 4
               ]
        //10.998942   76.990154
print("params\(parameters)")
        let url = CardName.sharedInstance.salonListing_url
        print(url)
      let type = UserDefaults.standard.string(forKey: "token")!
       let token = UserDefaults.standard.string(forKey: "Authorizent_token")!
        
        
        let header = [
            "Authorization": "\(type) \(token)",
        ]
        Alamofire.request(url, method: .post,parameters: parameters, encoding: JSONEncoding.default,headers : header)
            .responseJSON { response in
                print("response\(response)")
                if response.error == nil{
                    var response_value = response.result.value as! NSArray
                    
                    print("respone value = \(response_value)")
                    print(response_value.count)
                    if response_value.count == 0
                    {
                        //self.searchbar.text = ""
                        self.tableview.isHidden = true
                        self.nosalon_view.isHidden = false
                       
                        
                    }
                    else
                    {
                        if CardName.sharedInstance.alphabeticalselected
                        {
                            
                            let sortedArray = (response_value as NSArray).sortedArray(using: [NSSortDescriptor(key: "Name", ascending: true)]) as! [[String:AnyObject]]
                            response_value = sortedArray as NSArray
                            
                        }
                        print("count_verified\(response_value.count)")
                        for i in 0..<response_value.count
                        {
                            do
                            {
                                let salon_response = response_value[i] as! NSDictionary
                                
                                //print("salon response = \(response_value[i])")
                                let count = try? salon_response.value(forKey: "Count") as! Int
                                self.saloncheckins_Array.append(count!)
                                // print(salon_response.value(forKey: "Distance"))
                                let Distance = try? salon_response.value(forKey: "Distance") as! Double
                                self.salonDistance_Array.append(Distance!)
                                let genderType = try? salon_response.value(forKey: "GenderType") as! String
                                self.salontype_Array.append(genderType!)
                                let id = try? salon_response.value(forKey: "ID") as! Int
                                print("Salon_id\(id)")
                                //self.salon_id.append(id ?? 0)
                                self.clicked_id_Array.append(id!)
                                let Latitute = try? salon_response.value(forKey: "Latitude") as! Double
                                self.salon_latArray.append(Latitute ?? 0.0)
                                let Location = try? salon_response.value(forKey: "Location") as! String
                                self.Locationname_Array.append(Location!)
                                let Logo = try? salon_response.value(forKey: "Logo") as! String
                                self.salonLogo_Array.append(Logo!)
                                let Longitute = try? salon_response.value(forKey: "Longitude") as! Double
                                self.salon_logArray.append(Longitute ?? 0.0)
                                let name = try? salon_response.value(forKey: "Name") as! String
                                self.salonname_Array.append(name!)
                                var position = CLLocationCoordinate2DMake(Latitute ?? 0.0,Longitute ?? 0.0)
                                var gmsposititon = GMSCameraPosition(target: position, zoom: 15.0, bearing: 100, viewingAngle: 360)
                                var marker = GMSMarker(position: position)
                               
                                marker.title = name
                                marker.snippet = "\(Location!),\(Distance!)Km"
                                marker.map = self.mapview
                                
                                self.mapview.camera = gmsposititon
                                self.mapview.selectedMarker = marker
                                let offercount = try? salon_response.value(forKey: "OfferCount") as! Int
                                let rating = try? salon_response.value(forKey: "Rating") as! Double
                               
                                self.salonrating_Array.append(rating!)
                                let ratingCount = try? salon_response.value(forKey: "RatingCount") as! Int
                                let isverified = try? salon_response.value(forKey: "IsVerified") as! Bool
                                self.verified_array.append(isverified!)
                                var myData = Dictionary<String, Any>()
                                myData["Logo"] = Logo
                                myData["Location"] = Location
                                myData["Id"] = id
                                myData["Verified"] = isverified
                                myData["Gender"] = genderType
                                marker.userData = myData
                                

                            }
                            catch
                            {
                                print(error)
                            }
                            
                        }
                        self.tableview.reloadData()
                        
                        print("clean")
                        self.activityindicator.stopAnimating()
                        self.activityindicator.isHidden = true
                    }


                }
                else
                {
                    let alert = UIAlertController(title: "Alert", message: "Network Error, Try Again!", preferredStyle: .alert)
                    let okaction = alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
                    self.present(alert,animated: true,completion: nil)
                }
                
            }
        


    }
    
    @objc func hidekeyboard()
    {
        self.view.endEditing(true)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        btn_salonLocation?.titleLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping
       
        
        if UserDefaults.standard.string(forKey: "clicked_city") != nil  
        {
            var name = UserDefaults.standard.string(forKey: "clicked_city")
            if name == ""
            {
                if UserDefaults.standard.string(forKey: "city_name") != nil
                {
                    name = UserDefaults.standard.string(forKey: "city_name")
                }else
                {
                                    }
                
            }else
            {
                name = UserDefaults.standard.string(forKey: "clicked_city")
                name = name?.replacingOccurrences(of: " ", with: "")
            }
            var buttonText: NSString = "   Salons at \n \(name!) " as NSString
            
            //getting the range to separate the button title strings
            var newlineRange: NSRange = buttonText.range(of: "\n")
            
            //getting both substrings
            var substring1: NSString = ""
            var substring2: NSString = ""
            
            if(newlineRange.location != NSNotFound) {
                substring1 = buttonText.substring(to: newlineRange.location) as NSString
                substring2 = buttonText.substring(from: newlineRange.location) as NSString
            }
            let image1Attachment = NSTextAttachment()
            image1Attachment.image = UIImage(named: "downarrow.png")
            image1Attachment.bounds = CGRect(x: 0 , y: -2, width: 13, height: 13)
            
            // wrap the attachment in its own attributed string so we can append it
            let image1String = NSAttributedString(attachment: image1Attachment)
            
            //assigning diffrent fonts to both substrings
            // let font:UIFont? = UIFont(name: "Arial", size: 8.0)
            let myString = substring1
            let myAttribute = [ NSAttributedStringKey.foregroundColor: UIColor.white ,  NSAttributedStringKey.font: UIFont(name: "Arial", size: 10.0)! ]
            let myAttrString = NSAttributedString(string: myString as String, attributes: myAttribute)
            
            let font1:UIFont? = UIFont(name: "Arial", size: 17.0)
            let myString1 = substring2
            let myAttribute1 = [ NSAttributedStringKey.foregroundColor: UIColor.white,  NSAttributedStringKey.font: UIFont(name: "Arial", size: 17.0)!]
            let myAttrString1 = NSAttributedString(string: myString1 as String, attributes: myAttribute1)
            
            //appending both attributed strings
            let combination = NSMutableAttributedString()
            
            combination.append(myAttrString)
            combination.append(myAttrString1)
             combination.append(image1String)
            //assigning the resultant attributed strings to the button
            btn_salonLocation?.setAttributedTitle(combination, for: UIControlState.normal)
        }else{
            var cityname = UserDefaults.standard.string(forKey: "city_name")
            var buttonText: NSString = "   Salons at \n \(cityname!)" as NSString
            //getting the range to separate the button title strings
            var newlineRange: NSRange = buttonText.range(of: "\n")
            //getting both substrings
            var substring1: NSString = ""
            var substring2: NSString = ""
            if(newlineRange.location != NSNotFound) {
                substring1 = buttonText.substring(to: newlineRange.location) as NSString
                substring2 = buttonText.substring(from: newlineRange.location) as NSString
            }
            //assigning diffrent fonts to both substrings
            // let font:UIFont? = UIFont(name: "Arial", size: 8.0)
            let myString = substring1
            let myAttribute = [ NSAttributedStringKey.foregroundColor: UIColor.white ,  NSAttributedStringKey.font: UIFont(name: "Arial", size: 10.0)! ]
            let myAttrString = NSAttributedString(string: myString as String, attributes: myAttribute)
            let font1:UIFont? = UIFont(name: "Arial", size: 17.0)
            let myString1 = substring2
            let myAttribute1 = [ NSAttributedStringKey.foregroundColor: UIColor.white,  NSAttributedStringKey.font: UIFont(name: "Arial", size: 15.0)!]
            let myAttrString1 = NSAttributedString(string: myString1 as String, attributes: myAttribute1)
            //appending both attributed strings
            let combination = NSMutableAttributedString()
            combination.append(myAttrString)
            combination.append(myAttrString1)
            
            //assigning the resultant attributed strings to the button
           
            btn_salonLocation?.setAttributedTitle(combination, for: UIControlState.normal)

        }
       
    }
    @IBOutlet weak var btn_salonLocation: UIButton!
    @IBOutlet weak var HeaderView: UIView!
    @IBOutlet weak var tableview: UITableView!
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        //Dispose of any resources that can be recreated.
    }
    @objc func dismissKeyboard()
    {
        view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        view.endEditing(true)
        //textField.clearsOnBeginEditing = true;
        return true
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar)
    {
       searchbar.resignFirstResponder()
        
      }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension UISearchBar {
    public func setSerchTextcolor(color: UIColor) {
        let clrChange = subviews.flatMap { $0.subviews }
        guard let sc = (clrChange.filter { $0 is UITextField }).first as? UITextField else { return }
        sc.backgroundColor = .white
    }
}
