//
//  SalonTableViewCell.swift
//  BMHS
//
//  Created by TechBT on 09/08/18.
//  Copyright © 2018 TechBT. All rights reserved.
//

import UIKit

class SalonTableViewCell: UITableViewCell {

    @IBOutlet weak var view: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    @IBOutlet weak var salon_Location: UILabel!
    
    @IBOutlet weak var salon_distance: UILabel!
    @IBOutlet weak var salon_checkins: UILabel!
    @IBOutlet weak var salon_rating: UILabel!
    @IBOutlet weak var salon_Name: UILabel!
    @IBOutlet weak var salon_img: UIImageView!
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
