//
//  MoreViewController.swift
//  BMHS
//
//  Created by TechBT on 08/10/18.
//  Copyright © 2018 TechBT. All rights reserved.
//

import UIKit
import Alamofire
class MoreViewController: UIViewController , UITextFieldDelegate {
    @IBOutlet weak var activityindicator: UIActivityIndicatorView!

    @IBOutlet weak var rateus_view: UIView!
    @IBOutlet weak var txt_feedback: UITextField!
    @IBOutlet weak var feedback_view: UIView!
    
    @IBOutlet weak var help_us: UILabel!
    @IBOutlet weak var missing_salon_text: UILabel!
    @IBOutlet weak var btn: UIButton!
    override func viewDidAppear(_ animated: Bool) {
        self.txt_missingsalon.frame = CGRect(x: 15, y: missing_salon_text.frame.origin.y + missing_salon_text.frame.height + 5, width: txt_missingsalon.frame.width
            , height: 40)
          txt_missingsalon.underlined()
        
        self.txt_feedback.frame = CGRect(x: 15, y: help_us.frame.origin.y + help_us.frame.height + 5, width: txt_feedback.frame.width
            , height: 40)
        txt_feedback.underlined()
        
        
        
        
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txt_feedback.delegate = self
        txt_missingsalon.delegate =  self
        let layer = self.missing_salonpopup.layer
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        layer.shadowRadius = 6.0
        layer.shadowOpacity = 0.4
        layer.masksToBounds = false
        self.missing_salonpopup.layer.cornerRadius = 5
        let layer1 = self.feedback_view.layer
        layer1.shadowColor = UIColor.black.cgColor
        layer1.shadowOffset = CGSize(width: 0.0, height: 2.0)
        layer1.shadowRadius = 6.0
        layer1.shadowOpacity = 0.4
        layer1.masksToBounds = false
        self.feedback_view.layer.cornerRadius = 5
        let layer2 = self.rateus_view.layer
        layer2.shadowColor = UIColor.black.cgColor
        layer2.shadowOffset = CGSize(width: 0.0, height: 2.0)
        layer2.shadowRadius = 6.0
        layer2.shadowOpacity = 0.4
        layer2.masksToBounds = false
        self.rateus_view.layer.cornerRadius = 5
        let layer3 = self.rateus_improveview.layer
        layer3.shadowColor = UIColor.black.cgColor
        layer3.shadowOffset = CGSize(width: 0.0, height: 2.0)
        layer3.shadowRadius = 6.0
        layer3.shadowOpacity = 0.4
        layer3.masksToBounds = false
        self.rateus_improveview.layer.cornerRadius = 5
        self.missing_salonpopup.isHidden = true
//       missing_label.titleLabel?.adjustsFontSizeToFitWidth = true;
//        share_label.titleLabel?.adjustsFontSizeToFitWidth = true;
        txt_feedback.underlined()
        if UserDefaults.standard.string(forKey: "FullName") != nil && UserDefaults.standard.string(forKey: "FullName") != "" {
            let username = UserDefaults.standard.string(forKey: "FullName")
            self.welcome_lbl.text = " Welcome \(username!)!"
        }else
        {
             self.welcome_lbl.text = " Welcome Guest!"
        }
        
      
        // Do any additional setup after loading the view.
    }
    @IBOutlet weak var welcome_lbl: UILabel!
    @IBOutlet weak var rateus_improveview: UIView!
    @IBAction func btn_ratenotnow(_ sender: Any) {
        self.rateus_view.isHidden = true
        self.rateus_improveview.isHidden = false
    }
    
    @IBOutlet weak var missing_label: UIButton!
    @IBOutlet weak var share_label: UIButton!
    @IBAction func btn_rateimprovenotnow(_ sender: Any) {
        self.rateus_improveview.isHidden = true
    }
    
    @IBAction func btn_rateimprovenow(_ sender: Any) {
      print("Rate us came")
        
        //https://apps.apple.com/us/app/bookmyhairstylist/id1456390652?ls=1
        let appId = "bookmyhairstylist/id1456390652?ls=1&mt=8"
        let APPID = "1456390652"
//        guard let url = URL(string: "itms-apps://itunes.apple.com/us/app/" + \(appId)) else {
//            return
//        }
        let url = URL(string: "itms-apps://itunes.apple.com/us/app/apple-store/id\(APPID)?mt=8&action=write-review")!

        print("rateus URL : \(url)")
        if #available(iOS 10, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
            
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    @IBAction func btn_aboutus(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let subclass = storyboard.instantiateViewController(withIdentifier: "Aboutus_ViewController") as! Aboutus_ViewController
        self.present(subclass,animated: true,completion: nil)
    }
    
    @IBAction func btn_bookings(_ sender: Any) {
        if UserDefaults.standard.bool(forKey: "isLogged") != nil
        {
        var usertype = UserDefaults.standard.bool(forKey: "isLogged")
        if usertype
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let subclass = storyboard.instantiateViewController(withIdentifier: "BookingsTabbar") as! UITabBarController
            self.present(subclass,animated: true,completion: nil)
        }else{
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let subclass = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! UITabBarController
            self.present(subclass,animated: true,completion: nil)
        }
        }
    }
    
    @IBAction func btn_rateus(_ sender: Any) {
        
        if UserDefaults.standard.bool(forKey: "isLogged") != nil
        {
            var usertype = UserDefaults.standard.bool(forKey: "isLogged")
            if usertype
            {
                self.rateus_view.isHidden = false
            }else{
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let subclass = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! UITabBarController
                self.present(subclass,animated: true,completion: nil)
            }
        }
        
        
    }
    @IBAction func btn_feedsend(_ sender: Any) {
        var feedname = txt_feedback.text
        if feedname?.count == 0
        {
            let alert = UIAlertController(title: "Message", message: "Enter Valid Feedback Name", preferredStyle: .alert)
            let okaction = alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
            self.present(alert,animated: true,completion: nil)
        }else
        {
            getSalons(feedback: txt_feedback.text ?? "")
           
        }
        
    }
    
    @IBAction func btn_missingsalonsend(_ sender: Any) {
        view.endEditing(true)
        
        var missingsalon_name = txt_missingsalon.text
        if missingsalon_name?.count == 0
        {
            activityindicator.stopAnimating()
            let alert = UIAlertController(title: "Message", message: "Enter Valid Missing Salon Name", preferredStyle: .alert)
            let okaction = alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
            self.present(alert,animated: true,completion: nil)
        }else{
            getSalons(feedback: txt_missingsalon.text ?? "")
            
        }
        
    }
    @IBAction func btn_feedback(_ sender: Any) {
      self.rateus_improveview.isHidden = true
        if UserDefaults.standard.bool(forKey: "isLogged") != nil
        {
        var usertype = UserDefaults.standard.bool(forKey: "isLogged")
        if usertype
        {
             self.feedback_view.isHidden = false
        }else{
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let subclass = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! UITabBarController
            self.present(subclass,animated: true,completion: nil)
        }
        }
    }
    @IBAction func btn_feedcancel(_ sender: Any) {
        self.feedback_view.isHidden = true
    }
    @IBOutlet weak var txt_missingsalon: UITextField!
    @IBOutlet weak var missing_salonpopup: UIView!
    
    @IBAction func btn_missingcancel(_ sender: Any) {
        self.missing_salonpopup.isHidden = true
    }
    @IBAction func btn_profile(_ sender: Any) {
        if UserDefaults.standard.bool(forKey: "isLogged") != nil{
            var usertype = UserDefaults.standard.bool(forKey: "isLogged")
            if usertype
            {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let subclass = storyboard.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
                self.present(subclass,animated: true,completion: nil)
            }else{
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let subclass = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! UITabBarController
                self.present(subclass,animated: true,completion: nil)
            }
        }
      
       
    }
    @IBAction func share_invite(_ sender: Any)
    {
        
        if UserDefaults.standard.bool(forKey: "isLogged") != nil
        {
            var usertype = UserDefaults.standard.bool(forKey: "isLogged")
            if usertype
            {
                
                
//                let someText:String = "Hello want to share text also"
//                let objectsToShare:URL = URL(string: "https://apps.apple.com/us/app/bookmyhairstylist/id1456390652?ls=1")!
//                let sharedObjects:[AnyObject] = [objectsToShare as AnyObject,someText as AnyObject]
//                let activityViewController = UIActivityViewController(activityItems : sharedObjects, applicationActivities: nil)
//                activityViewController.popoverPresentationController?.sourceView = self.view
//
//                activityViewController.excludedActivityTypes = [ UIActivityType.airDrop]
//
//                self.present(activityViewController, animated: true, completion: nil)
                if let name = NSURL(string: "https://itunes.apple.com/us/app/myapp/id1456390652?ls=1&mt=8") {
                    let objectsToShare = [name]
                    let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
                    
                    self.present(activityVC, animated: true, completion: nil)
                }
                else
                {
                    // show alert for not available
                }
                print("registered user")
                
            }else{
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let subclass = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! UITabBarController
                self.present(subclass,animated: true,completion: nil)
            }
        }
    
}
    
    @IBAction func btn_missingsalon(_ sender: Any) {
        if UserDefaults.standard.bool(forKey: "isLogged") != nil{
            var usertype = UserDefaults.standard.bool(forKey: "isLogged")
            if usertype
            {
            self.missing_salonpopup.isHidden = false
        }else{
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let subclass = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! UITabBarController
            self.present(subclass,animated: true,completion: nil)
        }
        }
    }
    func getSalons(feedback:String)
    {
       activityindicator.stopAnimating()
        var str_feedback = feedback
        str_feedback = str_feedback.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) ?? ""
        let url = "\(CardName.sharedInstance.feedback_url)?feedBack=\(str_feedback)"
        print(url)
        var type = UserDefaults.standard.string(forKey: "token")!
        var token = UserDefaults.standard.string(forKey: "Authorizent_token")!
        let header = [
            "Authorization": "\(type) \(token)",
            ]
        print("headers\(header)")
        Alamofire.request(url, method: .get,encoding: JSONEncoding.default,headers : header)
            .responseJSON { response in
                print("response\(response)")
              
                let responce_value = response.result.value as! NSDictionary
                let status = responce_value.value(forKey: "Status") as! Int
                if status == 1
                {
                    self.missing_salonpopup.isHidden = true
                    self.feedback_view.isHidden = true
                    self.txt_feedback.text = ""
                    self.txt_missingsalon.text = ""
                }else{
                    let alert = UIAlertController(title: "Message", message: "Something went wrong! Please Try Again", preferredStyle: .alert)
                    let okaction = alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
                    self.present(alert,animated: true,completion: nil)
                    self.missing_salonpopup.isHidden = true
                }
        }
        
     
        
        
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return true
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


