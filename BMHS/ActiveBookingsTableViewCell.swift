//
//  ActiveBookingsTableViewCell.swift
//  BMHS
//
//  Created by TechBT on 24/09/18.
//  Copyright © 2018 TechBT. All rights reserved.
//

import UIKit

class ActiveBookingsTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    @IBOutlet weak var status_label: UILabel!
    @IBOutlet weak var saloonservice_Label: UILabel!
    @IBOutlet weak var saloonName_Label: UILabel!
    @IBOutlet weak var time_label: UILabel!
    @IBOutlet weak var Daylabel: UILabel!
    @IBOutlet weak var date_label: UILabel!
    @IBOutlet weak var month_label: UILabel!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
