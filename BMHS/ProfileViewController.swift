//
//  ProfileViewController.swift
//  BMHS
//
//  Created by TechBT on 04/10/18.
//  Copyright © 2018 TechBT. All rights reserved.
//

import UIKit
import Alamofire

class ProfileViewController: UIViewController {
    @IBOutlet weak var back_img: UIImageView!
    
    @IBOutlet weak var txt_gender: UILabel!
    @IBOutlet weak var txt_emailid: UILabel!
    @IBOutlet weak var txt_mobile: UILabel!
    @IBOutlet weak var txt_username: UILabel!
    @IBOutlet weak var membershipview: UIView!
    var id = ""
    var token_type = ""
    var user_toke = ""
    override func viewDidLoad() {
        super.viewDidLoad()
         getSalons()
        let tap = UITapGestureRecognizer(target: self, action: #selector(ProfileViewController.tapfunction))
        back_img.isUserInteractionEnabled = true
        
        back_img.addGestureRecognizer(tap)
        let layer = self.background_view.layer
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        layer.shadowRadius = 2.0
        layer.shadowOpacity = 0.2
        layer.masksToBounds = false
        self.background_view.layer.cornerRadius = 5
      
        //getSalons()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func btn_logout(_ sender: Any) {
        let alert = UIAlertController(title: "Alert", message: "Are you sure you want to logout?", preferredStyle: .alert)
        let okaction = alert.addAction(UIAlertAction(title: "Yes", style: .default)
        { (action) in
            UserDefaults.standard.set("", forKey: "FullName")
            self.registerdevice()
        })
        let cancel = alert.addAction(UIAlertAction(title: "No", style: .default, handler: nil))
        
        self.present(alert,animated: true,completion: nil)
        
    }
    func registerdevice()
    {
        let url = CardName.sharedInstance.device_register_url
        print("info_url\(url)")
        let uuid = UIDevice.current.identifierForVendor?.uuidString
        print("device_id\(uuid)")
        
        
        var systemVersion = UIDevice.current.systemVersion
        
        print("system\(systemVersion)")
        let parameters: [String: Any] = [
            "DeviceUniqueID" : "\(uuid!)VER\(systemVersion)",
        ]
        print("param\(parameters)")
        Alamofire.request(url, method: .post,parameters: parameters, encoding: JSONEncoding.default)
            .responseJSON { response in
                print("responce\(response)")
                let responce_value = response.result.value as! NSDictionary
                
                let status = responce_value.value(forKey: "Status") as! Bool
                if status
                {
                    self.id = responce_value.value(forKey: "ID") as! String
                   
                    self.guesttoken()
                }else
                {
                    let alert = UIAlertController(title: "Alert", message: "Plaease Try Again!", preferredStyle: .alert)
                    let okaction = alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
                    self.present(alert,animated: true,completion: nil)
                }
        }
    }
    func guesttoken()
    {
        let url = CardName.sharedInstance.guest_token_url
        print("url\(url)")
        let headers = [
            "Content-Type": "application/x-www-form-urlencoded"
        ]
       
        print("header\(headers)")
        let parameters: [String: Any] = [
            "username" : self.id,
            "password" : self.id,
            "grant_type" : "password"
        ]
        print("parameter\(parameters)")
        Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding()).responseJSON { (response:DataResponse<Any>) in
            print(response)
            print(response.error)
            if response.error == nil{
            let responce_value = response.result.value as! NSDictionary
            let token = responce_value.value(forKey: "access_token") as! String
            let token_sy = responce_value.value(forKey: "token_type") as! String
            self.user_toke = token
            self.token_type = token_sy
            self.validatetoken()
            }
            else{
                let alert = UIAlertController(title: "Message", message: "Network Error ! Try Again", preferredStyle: .alert)
                let okaction = alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
                self.present(alert,animated: true,completion: nil)
            }
        }
    }
    
    func validatetoken()
    {
        let url = CardName.sharedInstance.validate_token_url
        print("url\(url)")
        let headers = [
            "Authorization": "\(self.token_type) \(self.user_toke)"
        ]
        print("header\(headers)")
        
        Alamofire.request(url, method: .post, encoding: JSONEncoding.default,headers : headers)
            .responseJSON { response in
                print(response)
                print(response.error)
                if response.error == nil
                {
                    let responce_value = response.result.value as! NSDictionary
                    let status = responce_value.value(forKey: "Status") as! Bool
                    if status
                    {
                        UserDefaults.standard.set(self.token_type, forKey: "token")
                        UserDefaults.standard.set(self.user_toke, forKey: "Authorizent_token")
                        UserDefaults.standard.set(false, forKey: "isLogged")
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let subclass = storyboard.instantiateViewController(withIdentifier: "DashboardViewController") as! UITabBarController
                        self.present(subclass,animated: true,completion: nil)
                    }else
                    {
                       self.guesttoken()
                    }
                }else
                {
                    
                }
        }
    }
    @objc func tapfunction(sender: UIGestureRecognizer) {
       let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let subclass = storyboard.instantiateViewController(withIdentifier: "DashboardViewController") as! UITabBarController
        subclass.selectedIndex = 2
        self.present(subclass,animated: true,completion: nil)
    }
    
    func getSalons()
    {
        let url = CardName.sharedInstance.profile_url
        print(url)
        var type = UserDefaults.standard.string(forKey: "token")!
        var token = UserDefaults.standard.string(forKey: "Authorizent_token")!
        let header = [
            "Authorization": "\(type) \(token)",
            ]
        Alamofire.request(url, method: .post, encoding: JSONEncoding.default,headers : header)
            .responseJSON { response in
                print("header\(header)")
                print("response\(response)")
                let responce_value = response.result.value as! NSDictionary
                let email = responce_value.value(forKey: "EmailID") as! String
                
                self.txt_emailid.text = email
                let fullname = responce_value.value(forKey: "FullName") as! String
                UserDefaults.standard.set(fullname, forKey: "FullName")
                self.txt_username.text = fullname
                let gender = responce_value.value(forKey: "Gender") as! Int
                if gender == 1
                {
                    self.txt_gender.text = "MALE"
                }else if gender == 2{
                       self.txt_gender.text = "FEMALE"
                }
                
                let username = responce_value.value(forKey: "UserName") as! String
                self.txt_mobile.text = username
                
                
                
        }
        
        
        
    }
    @IBOutlet weak var background_view: UIView!
  
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
