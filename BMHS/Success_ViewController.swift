//
//  Success_ViewController.swift
//  BMHS
//
//  Created by TechBT on 02/11/18.
//  Copyright © 2018 TechBT. All rights reserved.
//

import UIKit

class Success_ViewController: UIViewController {

    @IBOutlet weak var back_img: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.bmhsid_lbl.text = "ORDER ID : \(CardName.sharedInstance.order_ref)"
        let tap = UITapGestureRecognizer(target: self, action: #selector(Success_ViewController.tapFunction))
        back_img.isUserInteractionEnabled = true
        
        back_img.addGestureRecognizer(tap)
        let username = UserDefaults.standard.string(forKey: "FullName")
        if (UserDefaults.standard.string(forKey: "FullName") != nil)
        {
        self.username_lbl.text = "Hey \(username!),We Will notify once Your Booking has been confirmed"
        }
        // Do any additional setup after loading the view.
    }
    
    @IBOutlet weak var bmhsid_lbl: UILabel!
    @IBOutlet weak var username_lbl: UILabel!
     @objc func tapFunction(sender:UITapGestureRecognizer) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let subclass = storyboard.instantiateViewController(withIdentifier: "DashboardViewController") as! UITabBarController
        subclass.selectedIndex = 1
        self.present(subclass,animated: true,completion: nil)
        
    }

    @IBAction func viewbooking(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let subclass = storyboard.instantiateViewController(withIdentifier: "BookingsTabbar") as! UITabBarController
        self.present(subclass,animated: true,completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
