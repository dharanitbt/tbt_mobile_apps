//
//  OrderDetailsViewController.swift
//  BMHS
//
//  Created by TechBT on 03/10/18.
//  Copyright © 2018 TechBT. All rights reserved.
//

import UIKit

class OrderDetailsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet weak var back_img: UIImageView!
    
    var contact = ""
    var servicetype_Array:[String] = []
    var serviceName_Array:[String] = []
    var serviceamount_Array:[Int] = []
    @IBOutlet weak var Bookingdate_view: UIView!

    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var back_image: UIImageView!
    @IBOutlet weak var Service_view: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        service_tableview.delegate = self
        service_tableview.dataSource = self
        service_tableview.register(UINib(nibName: "checkoutservice_TableViewCell", bundle: nil), forCellReuseIdentifier: "checkoutservice_TableViewCell")
        let layer = self.SalonDetails_view.layer
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        layer.shadowRadius = 2.0
        layer.shadowOpacity = 0.2
        layer.masksToBounds = false
        self.SalonDetails_view.layer.cornerRadius = 5
        
        
        
        let layer1 = self.Booking_details_view.layer
        layer1.shadowColor = UIColor.black.cgColor
        layer1.shadowOffset = CGSize(width: 0.0, height: 2.0)
        layer1.shadowRadius = 2.0
        layer1.shadowOpacity = 0.2
        layer1.masksToBounds = false
        self.Booking_details_view.layer.cornerRadius = 5
        
        
        let layer2 = self.Bookingdate_view.layer
        layer2.shadowColor = UIColor.black.cgColor
        layer2.shadowOffset = CGSize(width: 0.0, height: 2.0)
        layer2.shadowRadius = 2.0
        layer2.shadowOpacity = 0.2
        layer2.masksToBounds = false
        self.Bookingdate_view.layer.cornerRadius = 5
        print("result booking\(CardName.sharedInstance.booking_dictionary)")
        let tap = UITapGestureRecognizer(target: self, action: #selector(OrderDetailsViewController.tapFunction))
        back_img.isUserInteractionEnabled = true
        
        back_img.addGestureRecognizer(tap)
        
        getdetails()
        
        // Do any additional setup after loading the view.
    }
    @IBOutlet weak var Line_view: UIView!
    @IBOutlet weak var Amount_view: UIView!
    override func viewDidAppear(_ animated: Bool) {
//      service_tableview.frame = CGRect(x:  service_tableview.frame.origin.x, y:  Items_label.frame.origin.y + Items_label.frame.height + 10, width:  service_tableview.frame.size.width, height:  service_tableview.contentSize.height)
       
        
    }
    override func viewDidLayoutSubviews(){
        
        self.Items_label.frame = CGRect(x: Items_label.frame.origin.x, y: Items_label.frame.origin.y, width: Items_label.frame.width, height: Items_label.frame.height)
        service_tableview.frame = CGRect(x:  service_tableview.frame.origin.x, y:  Items_label.frame.origin.y + Items_label.frame.height + 10, width:  service_tableview.frame.size.width, height:  service_tableview.contentSize.height + 15)
        
       
        Service_view.frame = CGRect(x:  Service_view.frame.origin.x, y:  Service_view.frame.origin.y  , width:  Service_view.frame.width, height:  service_tableview.frame.height + 20)
        self.Line_view.frame = CGRect(x:  Line_view.frame.origin.x, y:  Service_view.frame.origin.y + Service_view.frame.height + 3 , width:  Line_view.frame.width, height: Line_view.frame.height)
        self.Amount_view.frame = CGRect(x:  Amount_view.frame.origin.x, y:  Service_view.frame.origin.y + Service_view.frame.height + 10 , width:  Amount_view.frame.width, height:  Amount_view.frame.height)
        
        self.scrollview.contentSize = CGSize(width: self.scrollview.contentSize.width, height: self.Amount_view.frame.origin.y + self.totalamount_view.frame.origin.y + self.totalamount_view.frame.height)
       service_tableview.reloadData()
        print("service view height : \(Service_view.frame.height)")
        print("table_view ht : \(service_tableview.frame.height)")
        print("Service_view.y : \(Service_view.frame.origin.y)")
        print("Amount_view.y : \(Amount_view.frame.origin.y)")
        
    }
    @IBOutlet weak var scrollview: UIScrollView!
    var totalservicecount = 0
    func getdetails()
    {
        
        let response_value = CardName.sharedInstance.booking_dictionary
        contact = response_value.value(forKey: "ContactNumber1") as! String
        let salonname = response_value.value(forKey: "SaloonName") as! String
        self.salonname_Lbl.text = salonname
        let salonaddress = response_value.value(forKey: "SaloonAddress") as! String
        self.salonaddress_lbl.text = salonaddress
        let status = response_value.value(forKey: "BookingStatus") as! String
        self.bookingstatus_Lbl.text = status
        let referencenumber = response_value.value(forKey: "OrderReferenceNumber") as! String
        self.bookingreference_Lbl.text = referencenumber
        let paymentmode = response_value.value(forKey: "IsOnlinePayment") as! Bool
        let provider = response_value.value(forKey: "PaymentProvider") as! String
        if paymentmode
        {
            self.salonpayment_Lbl.text = "Online Payment -\(provider)"
        }
        else
        {
            self.salonpayment_Lbl.text = "Pay @ store"
        }
        let salontime = response_value.value(forKey: "StartTimeSlot") as! String
        self.salontime_Lbl.text = salontime
        let salondate = response_value.value(forKey: "BookingDate") as! String
        
        func UTCToLocal(date:String) -> String {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
            dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
            
            let dt = dateFormatter.date(from: date)
            print("Date\(dt)")
            dateFormatter.timeZone = TimeZone.current
            dateFormatter.dateFormat = "EEE, MMM d"
            if(dt != nil){
                return dateFormatter.string(from: dt!)}
            return ""
        }
        let salonservicedate   = UTCToLocal(date: salondate)
        self.salondate_Lbl.text = salonservicedate
        
        
        
        
        let order_response = response_value.value(forKey: "SaloonService") as! NSArray
        
        for i in 0..<order_response.count
        {
            do{
                
                let service_response = order_response[i] as! NSDictionary
                let count = try? service_response.value(forKey: "SaloonServiceTime") as! Int
                totalservicecount += count!
            }
        }
        
        func secondsToHoursMinutesSeconds (seconds : Int) -> (Int, Int)
        {
            return (seconds / 3600, (seconds % 3600) / 60)
        }
        let sec = totalservicecount * 60
        let (h, m) = secondsToHoursMinutesSeconds(seconds: sec)
        print("total_time\(h) \(m)")
        if h == 0
        {
            
            self.servicetime_Lbl.text = "\(m)Min"
        }
        else
        {
            let time = h * 60
            self.servicetime_Lbl.text = "\(h)hr \(m)Min"
        }
        
        let subtotal_response = response_value.value(forKey: "SubTotal")
        let subtotal_dictionary = subtotal_response as! NSDictionary
        let subtotal = subtotal_dictionary.value(forKey: "Price") as! String
        subtotal_Lbl.text = "₹" + subtotal
        
        let loyaltyl_response = response_value.value(forKey: "Loyalty")
        let loyalty_dictionary = loyaltyl_response as! NSDictionary
        let loyalty = loyalty_dictionary.value(forKey: "Price") as! String
        loyalty_Lbl.text = "₹" + loyalty
        
        let bmhsoffer_respone = response_value.value(forKey: "BMHSOffer")
        let bmhsoffer_dictionary = bmhsoffer_respone as! NSDictionary
        let bmhsoffer = bmhsoffer_dictionary.value(forKey: "Price") as! String
        bmhsoffer_Lbl.text = "₹" + bmhsoffer
        
        let salonoffer_response = response_value.value(forKey: "SalonOffer")
        let salonoffer_dictionary = salonoffer_response as! NSDictionary
        let salonoffer = salonoffer_dictionary.value(forKey: "Price") as! String
        salonoffer_Lbl.text = "₹" + salonoffer
        
        let amountbeforetax_response = response_value.value(forKey: "AmountBeforeTax")
        let amountbeforetax_dictionary = amountbeforetax_response as! NSDictionary
        let amountbeforetax = amountbeforetax_dictionary.value(forKey: "Price") as! String
        amountbrforetax_Lbl.text = "₹" + amountbeforetax
        
        let amountaferdiscount = response_value.value(forKey:"AmountAfterDiscount") as! Int
        discount_lbl.text = "₹" + String(amountaferdiscount)
        let totalamount = response_value.value(forKey: "TotalAmount") as! String
        totalamount_Lbl.text = "₹" + totalamount
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == service_tableview
        {
            let response_value = CardName.sharedInstance.booking_dictionary
            let order_response = response_value.value(forKey: "SaloonService") as! NSArray
            
            for i in 0..<order_response.count
            {
                do{
                    
                    let service_response = order_response[i] as! NSDictionary
                    
                    
                    let count = try? service_response.value(forKey: "SaloonServiceType") as! String
                    self.servicetype_Array.append(count!)
                    let count2 = try? service_response.value(forKey: "SaloonServiceName") as! String
                    self.serviceName_Array.append(count2!)
                    let count3 = try? service_response.value(forKey: "SaloonServicePrice") as! Int
                    self.serviceamount_Array.append(count3!)
                }
                
            }
            return order_response.count
        }
        //        if tableView == serviceamount_tableview
        //        {
        //
        //        }
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = service_tableview.dequeueReusableCell(withIdentifier: "checkoutservice_TableViewCell", for: indexPath) as! checkoutservice_TableViewCell
        let screenSize = UIScreen.main.bounds
        let screenWidth = screenSize.width
       if screenWidth == 320.0
       {
        
        cell.tickimage.frame = CGRect(x: 8 , y: 4, width: 10, height: 10)
        cell.service_name.frame = CGRect(x: 25 , y: 2, width: 190, height: 15)
        cell.service_amount.frame = CGRect(x:210, y: 2, width:62, height : 15 )
        
        }
        cell.service_name.text = serviceName_Array[indexPath.row] as! String + " (\(servicetype_Array[indexPath.row]))"
        cell.service_amount.text = "₹" + String(serviceamount_Array[indexPath.row] as! Int)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 28
    }
    @IBOutlet weak var Booking_details_view: UIView!
    
    @IBOutlet weak var SalonDetails_view: UIView!
    @IBOutlet weak var salonname_Lbl: UILabel!
    
  @objc func tapFunction(sender:UITapGestureRecognizer) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let subclass = storyboard.instantiateViewController(withIdentifier: "BookingsTabbar") as! UITabBarController
        self.present(subclass,animated: true,completion: nil)
        
    }
    
    
    @IBAction func btn_requestcall(_ sender: Any) {
        
        if let url = URL(string: "tel://\(contact)"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    @IBOutlet weak var salonaddress_lbl: UILabel!
    @IBOutlet weak var bookingstatus_Lbl: UILabel!
    
    @IBOutlet weak var bookingreference_Lbl: UILabel!
     @IBOutlet weak var Items_label: UILabel!
    
    @IBOutlet weak var serviceamount_tableview: UITableView!
    
    @IBOutlet weak var salonpayment_Lbl: UILabel!
    
    @IBOutlet weak var salonoffer_Lbl: UILabel!
    
    @IBOutlet weak var bmhsoffer_Lbl: UILabel!
    @IBOutlet weak var service_tableview: UITableView!
    @IBOutlet weak var salondate_Lbl: UILabel!
    @IBOutlet weak var salontime_Lbl: UILabel!
    
    @IBOutlet weak var subtotal_Lbl: UILabel!
    @IBOutlet weak var totalamount_Lbl: UILabel!
    @IBOutlet weak var amountbrforetax_Lbl: UILabel!
    @IBOutlet weak var loyalty_Lbl: UILabel!
    @IBOutlet weak var discount_lbl: UILabel!
    @IBOutlet weak var servicetime_Lbl: UILabel!
    
    
    @IBOutlet weak var totalamount_view: UIView!
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
