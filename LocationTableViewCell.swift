//
//  LocationTableViewCell.swift
//  BMHS
//
//  Created by TechBT on 28/08/18.
//  Copyright © 2018 TechBT. All rights reserved.
//

import UIKit

class LocationTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    @IBOutlet weak var Location_Name: UILabel!
    @IBOutlet weak var Location_img: UIImageView!
    @IBOutlet weak var Location_View: UIView!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
