//
//  LocationCollectionViewCell.swift
//  BMHS
//
//  Created by TechBT on 27/08/18.
//  Copyright © 2018 TechBT. All rights reserved.
//

import UIKit

class LocationCollectionViewCell: UICollectionViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    @IBOutlet weak var LocationView: UIView!
    @IBOutlet weak var Location_img: UIImageView!
    @IBOutlet weak var Location_Name: UILabel!
    
}
