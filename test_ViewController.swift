//
//  test_ViewController.swift
//  BMHS
//
//  Created by TechBT on 18/02/19.
//  Copyright © 2019 TechBT. All rights reserved.
//

import UIKit

class test_ViewController: UIViewController {
  
     var cells: [Service_TableViewCell] = []
   

    override func viewDidLoad() {
        super.viewDidLoad()
 //tableview.register(UINib(nibName: "Service_TableViewCell", bundle: nil), forCellReuseIdentifier: "Cell")
        cellsSetup()
        tableViewSetup()
        // Do any additional setup after loading the view.
    }


    @IBOutlet weak var tableview: UITableView!
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension test_ViewController {
    
    // NOTE: - Here we pre-create all the cells we need and add them to the cells array. Not good!
    func cellsSetup() {
        for _ in 0...1000 {
            let cell = Bundle.main.loadNibNamed("Service_TableViewCell", owner: self, options: nil)?[0] as! Service_TableViewCell
            //cell.textLabel?.text = "Wow so bad"
          //  cell.textLabel?.text = "Karna"
            cells.append(cell)
        }
    }
    
    func tableViewSetup() {
        tableview.rowHeight = UITableViewAutomaticDimension
        tableview.estimatedRowHeight = 60
    }
    
}
extension test_ViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cells.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // NOTE: - Now we only need to specify a cell from the array based on index, but not worth it
        let cell = cells[indexPath.row]
        return cell
    }
    
}
